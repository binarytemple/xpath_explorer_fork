package net.sourceforge.xpe.xmlassert;

import java.io.*;
import java.util.*;

import org.apache.commons.lang.*;
import org.jaxen.*;
import org.jaxen.jdom.*;
import org.jdom.*;
import org.jdom.input.*;
import junit.framework.*;
import org.jaxen.function.StringFunction;

public class XmlAssert
        extends Assert
{

    public static void assertEquals(String expected, String actual)
    {
        assertStringEquals(expected, actual);
    }

    public static void assertEquals(String message, String expected,
            String actual)
    {
        assertStringEquals(message, expected, actual);
    }

    public static void assertStringEquals(String expected, String actual)
    {
        assertStringEquals(null, expected, actual);
    }

    public static void assertStringEquals(String message, String expected,
            String actual)
    {
        if (!equalsOrBothNull(expected, actual)) {
            int at = StringUtils.indexOfDifference(expected, actual);
            String expectedWindow = StringUtils.abbreviate
                    (expected, at - 10, 59);
            String actualWindow = StringUtils.abbreviate(actual,
                    at - 10, 59);
            fail( (message == null ? "" : (message + ":")) +
                    "Expected <" + expected + "> but was <" + actual + ">" +
                    "\nExpected: " + StringEscapeUtils.escapeJava(expectedWindow) +
                    "\n  Actual: " + StringEscapeUtils.escapeJava(actualWindow));
        }
    }

    private static boolean equalsOrBothNull(String expected, String actual)
    {
        if (expected == null) {
            return actual == null;
        }
        return expected.equals(actual);
    }

    public static void assertStringContains(String expectedSubstring,
            String actual)
    {
        assertContains(expectedSubstring, actual);
    }

    public static void assertContains(String expectedSubstring, String actual)
    {
        assertTrue("did not find expected '" + expectedSubstring + "' in '" +
                actual + "'",
                actual.indexOf(expectedSubstring) != -1);
    }

    public static Element asJdomElement(String content)
    {
        Document doc = asJdomDocument(content);
        return doc.getRootElement();
    }

    public static Document asJdomDocument(String content)
    {
        return asJdomDocument(content, false);
    }

    public static Document asJdomDocument(String content, boolean validate)
    {
        return asJdomDocument(new StringReader(content), validate);
    }

    private static Document asJdomDocument(Reader reader, boolean validate)
    {
        return XmlFixture.fromReader(reader, validate).getDocument();
    }

    public static Document asJdomDocument(org.w3c.dom.Document document)
    {
        return XmlFixture.fromW3cDom(document).getDocument();
    }

    // XPath

    public static void assertXpath(Document document, String xpath,
            String expectedValue)
    {
        assertXpathEquals(document, xpath, expectedValue);
    }

    public static void assertXpath(Document document, String xpath,
            int expected)
    {
        assertXpathEquals(document, xpath, String.valueOf(expected));
    }

    public static void assertXpathEquals(Document document,
            String xpathExpression,
            String expectedValue)
    {
        assertXpathEquals("matching xpath '" + xpathExpression + "'", document,
                xpathExpression, expectedValue);
    }

    public static void assertXpathEquals(String message, Document document,
            String xpathExpression,
            String expectedValue)
    {
        assertStringEquals(message, expectedValue,
                xpathValue(document, xpathExpression));
    }

    public static void assertXpathExists(Document document,
            String xpathExpression)
    {
        XmlFixture xml = XmlFixture.fromJdom(document);
        assertTrue("xpath '" + xpathExpression + "' not found", xml.xpathExists(xpathExpression));
    }

    public static void assertXpathStartsWith(Document document, String xpath,
            String start)
    {
        String value = XmlAssert.xpathValue(document, xpath);
        assertTrue(value.startsWith(start));
    }

    public static void assertXpath(Document document, String xpath,
            boolean value)
    {
        assertXpathEquals(document, xpath, String.valueOf(value));
    }

    public static void assertXpath(Document document, String xpath, long value)
    {
        assertXpathEquals(document, xpath, String.valueOf(value));
    }

    public static void assertXpathCount(Document document, String xpath,
            int expectedCount)
    {
        int count = xpathCount(document, xpath);
        assertEquals(expectedCount, count);
    }

    public static void assertXpathIsEmptyElement(Document document,
            String xpath)
    {
        assertXpathExists(document, xpath);
        assertXpath(document, xpath, "");
    }

    public static void assertXpathDoesNotHaveChildElement(Document document,
            String parentXpath, String childName) throws JaxenException
    {
        // TODO: can this be done with straight XPath code, bypassing any need to use JDOM?
        XPath jaxen = new JDOMXPath(parentXpath);
        Element parentElement = (Element) jaxen.selectSingleNode(document);
        List children = parentElement.getChildren();
        for (int i = 0; i < children.size(); i++) {
            Element child = (Element) children.get(i);
            if (child.getName().equals(childName)) {
                fail("Should not have found: " + childName + " in " +
                        parentXpath);
            }
        }
    }

    protected static void assertXpathDoesNotHaveAttribute(Document document,
            String elementXpath, String attributeName) throws JaxenException
    {
        // TODO: can this be done with straight XPath code, bypassing any need to use Jdom?
        XPath jaxen = new JDOMXPath(elementXpath);
        Element parentElement = (Element) jaxen.selectSingleNode(document);
        assertNull("attribute '" + attributeName +
                "' was found for element xpath: " + elementXpath,
                parentElement.getAttribute(attributeName));
    }

    protected void assertXpathDoesNotExist(Document document, String xPath) throws
            Exception
    {
        int count = xpathCount(document, xPath);
        assertTrue("Element should not exist: " + xPath, count == 0);
    }

    public static String xpathValue(Document document, String xpath)
    {
        XpathMatch match = XmlFixture.fromJdom(document).xpathMatch(xpath);
        if (match.count() == 0) {
            return null;
        } else if (match.count() == 1) {
            return match.stringValue();
        } else {
            return StringFunction.evaluate(match.list().get(0), match.getXpath().getNavigator());
        }
    }

    public static int xpathCount(Document document, String xpath)
    {
        return XmlFixture.fromJdom(document).xpathCount(xpath);
    }

    public static void assertXmlEquals(String expectedXml, String actualXml)
    {
        assertXmlEquals(null, expectedXml, actualXml);
    }

    public static void assertXmlEquals(String message, String expectedXml,
            String actualXml)
    {
        try {
            XmlDiff diff = new XmlDiff(expectedXml, actualXml);
            assertXmlEquals(diff, message);
        }
        catch (Exception ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static void assertXmlEquals(String message, Element expectedXml,
            Element actualXml)
    {
        XmlDiff diff = new XmlDiff(expectedXml, actualXml);
        assertXmlEquals(diff, message);
    }

    public static void assertXmlEquals(XmlDiff diff, String message)
    {
        if (!diff.same()) {
            String result = diff.print();
            if (message != null) {
                result = message + Difference.NEWLINE + result;
            }
            Assert.fail(result);
        }
    }
}
