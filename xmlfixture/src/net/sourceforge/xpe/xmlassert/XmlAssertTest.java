package net.sourceforge.xpe.xmlassert;

import java.io.*;
import javax.xml.parsers.*;

import org.jdom.*;
import org.xml.sax.*;
import junit.framework.*;

public class XmlAssertTest
        extends TestCase
{
    //TODO: test more stuff

    public void testXpathAssert()
    {
        Document doc = XmlAssert.asJdomDocument("<a><b>c</b></a>", false);

        XmlAssert.assertXpathExists(doc, "/a/b");
        XmlAssert.assertXpathEquals(doc, "/a/b", "c");
        XmlAssert.assertXpathEquals(doc, "/a/e", null);
    }

    public void testAsJomDocumentFromW3cDocument() throws Exception
    {
        Document doc = XmlAssert.asJdomDocument(createW3cDocument("<a><b>c</b></a>"));
        assertNotNull(doc);
        XmlAssert.assertXpath(doc, "/a/b", "c");
    }

    private org.w3c.dom.Document createW3cDocument(String content) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(content)));
    }

    public void testBackwardCompatibility() {
        Document document = XmlFixture.fromText("<root><item>one</item><item>two</item></root>").getDocument();
        XmlAssert.assertXpath(document, "/root/item", "one");
    }

}
