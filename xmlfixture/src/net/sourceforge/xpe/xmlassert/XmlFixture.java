package net.sourceforge.xpe.xmlassert;

import java.io.*;

import org.jdom.*;
import org.jdom.input.*;
import org.jdom.output.*;

public class XmlFixture
{
    private Document document;

    public XmlFixture(Document document) {
        this.document = document;
    }

    public Document getDocument() {
        return document;
    }

    public String xpath(String xpath)
    {
        return xpathMatch(xpath).stringValue();
    }

    public XpathMatch xpathMatch(String xpath)
    {
        return XpathMatch.match(document, xpath);
    }

    public int xpathCount(String xpath)
    {
        return xpathMatch(xpath).count();
    }


    public static XmlFixture fromJdom(Document document)
    {
        return new XmlFixture(document);
    }

    public String asText()
    {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        try {
            new XMLOutputter("  ", true).output(document, output);
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
        return output.toString();
    }

    public static XmlFixture fromText(String content)
    {
        return fromReader(new StringReader(content));
    }

    private static XmlFixture fromReader(Reader reader)
    {
        return fromReader(reader, false);
    }

    public static XmlFixture fromReader(Reader reader, boolean validate)
    {
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder(validate);
        try {
            return new XmlFixture(builder.build(reader));
        }
        catch (IOException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
        catch (JDOMException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    public static XmlFixture fromW3cDom(org.w3c.dom.Document document)
    {
        DOMBuilder builder = new DOMBuilder();
        return new XmlFixture(builder.build(document));
    }

    public boolean xpathExists(String xpath)
    {
        return xpathMatch(xpath).hasMatch();
    }

}
