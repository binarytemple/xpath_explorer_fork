package net.sourceforge.xpe.xmlassert;

import java.io.*;

import org.jdom.*;
import org.jdom.input.*;
import junit.framework.*;

public class XmlDiffTest
        extends TestCase
{
    public void testEmpty() throws Exception
    {
        try {
            assertXmlSame("", "");
            fail("Should fail since empty string is not well-formed");
        }
        catch (JDOMParseException weExpectToGetThisException) {
        }
    }

    private void assertXmlSame(String xmlExpected, String xmlActual) throws JDOMException, IOException
    {
        XmlDiff diff = new XmlDiff(xmlExpected, xmlActual);
        assertTrue(diff.print(), diff.same());
    }

    private void assertXmlDifferent(String xmlExpected, String xmlActual,
            String message,
            String elementExpected, String elementActual, String parent) throws JDOMException, IOException
    {
        assertXmlDifferent(xmlExpected, xmlActual, message, elementExpected, elementActual, parent, parent);
    }

    private void assertXmlDifferent(String xmlExpected, String xmlActual, String message, String elementExpected, String elementActual, String parentExpected,
            String parentActual) throws JDOMException, IOException
    {
        XmlDiff diff = new XmlDiff(xmlExpected, xmlActual);
        assertFalse("Should be marked as different", diff.same());
        Difference difference = diff.firstDifference();
        assertEquals(message, difference.getMessage());

        assertEquals(elementExpected, difference.getExpectedString());
        assertEquals(elementActual, difference.getActualString());
        assertEquals(parentExpected, difference.getExpectedParentString());
        assertEquals(parentActual, difference.getActualParentString());
    }

    private void assertDifferences(String xmlExpected, String xmlActual, int count) throws JDOMException, IOException
    {
        XmlDiff diff = new XmlDiff(xmlExpected, xmlActual);
        assertEquals(count, diff.differences().size());
    }

    public void testSingleElement() throws Exception
    {
        assertXmlSame("<foo/>", "<foo/>");
        assertXmlDifferent("<foo/>", "<bar/>",
                "Expected element named \"foo\" but found element named \"bar\"", "<foo />", "<bar />", "");
    }

    public void testSelfClosing() throws Exception
    {
        assertXmlSame("<foo/>", "<foo></foo>");
        assertXmlSame("<foo></foo>", "<foo/>");
    }

    public void testWithAttributes() throws Exception
    {
        assertXmlSame("<foo bar=\"baz\"/>", "<foo bar=\"baz\"/>");
        assertXmlSame("<foo bar=\"baz\" baf=\"bam\"/>", "<foo baf=\"bam\" bar=\"baz\"/>");
    }

    public void testAttributeMissingAtEnd() throws Exception
    {
        String xmlExpected = "<foo aaa=\"baz\" zzz=\"bam\" />";
        String xmlActual = "<foo aaa=\"baz\" />";
        assertXmlDifferent(xmlExpected, xmlActual, "Expected attribute zzz='bam' was missing", xmlExpected, xmlActual, "");
    }

    public void testAttributeMissingAtBeginning() throws Exception
    {
        String xmlExpected = "<foo aaa=\"baz\" zzz=\"bam\" />";
        String xmlActual = "<foo zzz=\"bam\" />";
        assertXmlDifferent(xmlExpected, xmlActual, "Expected attribute aaa='baz' was missing", xmlExpected, xmlActual, "");
    }

    public void testExtraAttribute() throws Exception
    {
        String xmlExpected = "<foo bbb=\"zzz\" />";
        String xmlActual = "<foo aaa=\"xxx\" bbb=\"zzz\" />";
        assertXmlDifferent(xmlExpected, xmlActual, "Additional attribute aaa='xxx'", xmlExpected, xmlActual, "");
    }

    public void testAttributeValueDifferent() throws Exception
    {
        String xmlExpected = "<foo aaa=\"bar\" />";
        String xmlActual = "<foo aaa=\"baz\" />";
        assertXmlDifferent(xmlExpected, xmlActual,
                "Expected attribute aaa='bar' but found attribute aaa='baz'", xmlExpected, xmlActual, "");
    }

    public void testTextContentDifferent() throws Exception
    {
        String xmlExpected = "<foo>bar</foo>";
        String xmlActual = "<foo>baz</foo>";
        assertXmlDifferent(xmlExpected, xmlActual,
                "Expected text \"bar\" but found text \"baz\"", "bar", "baz", "<foo />");
    }

    public void testExpectingElementGotText() throws Exception
    {
        assertXmlDifferent("<foo><bar/></foo>", "<foo>baz</foo>",
                "Expected element named \"bar\" but found text \"baz\"",
                "<bar />", "baz", "<foo />");
    }

    public void testExpectingTextGotElement() throws Exception
    {
        assertXmlDifferent("<foo>baz</foo>", "<foo><bar/></foo>",
                "Expected text \"baz\" but found element named \"bar\"",
                "baz", "<bar />", "<foo />");
    }

    public void testExpectingElementButFoundText() throws Exception
    {
        assertXmlDifferent("<foo><bar/></foo>", "<foo>baz</foo>",
                "Expected element named \"bar\" but found text \"baz\"",
                "<bar />", "baz", "<foo />");
    }

    public void testChild() throws Exception
    {
        assertXmlDifferent("<foo><bar/></foo>", "<foo><baz/></foo>",
                "Expected element named \"bar\" but found element named \"baz\"", //todo: don't print final / for parent nodes
                "<bar />", "<baz />", "<foo />");
    }

    public void testChildOfParentWithAttributes() throws Exception
    {
        assertXmlDifferent("<foo aaa='zzz'><bar/></foo>", "<foo aaa='zzz'><baz/></foo>",
                "Expected element named \"bar\" but found element named \"baz\"", //todo: don't print final / for parent nodes
                "<bar />", "<baz />", "<foo aaa=\"zzz\" />");
    }

    public void testChildMissing() throws Exception
    {
        assertXmlDifferent("<foo><bar/><baz/></foo>", "<foo><bar/></foo>",
                "Expected child element named \"baz\" missing from inside element named \"foo\"", //todo: make this not print that final /
                "<baz />", "", "<foo />", "");
    }

    public void testExtraChild() throws Exception
    {
        assertXmlDifferent("<foo><bar/></foo>", "<foo><bar/><baz/></foo>",
                "Unexpected child element named \"baz\" found inside element named \"foo\"", //todo: don't print final / for parent nodes
                "", "<baz />", "", "<foo />");
    }

    //todo: intra-element whitespace trimming

    public void testIgnoreWhitespaceBetweenElements() throws Exception
    {
        checkIgnoreWhitespaceBetweenElements("<data> <more>foo</more> </data>", "<data><more>foo</more></data>");
    }

    private void checkIgnoreWhitespaceBetweenElements(String withSpace, String noSpace) throws JDOMException, IOException
    {
        checkIgnoreWhitespaceBetweenElements(new XmlDiff(noSpace, withSpace));
        checkIgnoreWhitespaceBetweenElements(new XmlDiff(withSpace, noSpace));
    }

    private void checkIgnoreWhitespaceBetweenElements(XmlDiff diff)
    {
        diff.setIgnoreWhitespaceBetweenElements(true);
        assertTrue("Differences:" + diff.print(), diff.same());
        diff.setIgnoreWhitespaceBetweenElements(false);
        assertFalse("Differences:" + diff.print(), diff.same());
    }

    public void testTrimWhitespaceInsideElements() throws Exception
    {
        checkTrimWhitespaceInsideElements("<data><more> foo </more></data>", "<data><more>foo</more></data>");
    }

    private void checkTrimWhitespaceInsideElements(String withSpace, String noSpace) throws JDOMException, IOException
    {
        checkTrimWhitespaceInsideElements(new XmlDiff(noSpace, withSpace));
        checkTrimWhitespaceInsideElements(new XmlDiff(withSpace, noSpace));
    }

    private void checkTrimWhitespaceInsideElements(XmlDiff diff)
    {
        diff.setTrimWhitespaceInsideElements(true);
        assertTrue("Differences:" + diff.print(), diff.same());
        diff.setTrimWhitespaceInsideElements(false);
        assertFalse("Differences:" + diff.print(), diff.same());
    }

    public void testRenamedElementCausesOnlyOneDifference() throws Exception
    {
        String expected = "<foo><one/><two/></foo>";
        String actual = "<foo><uno/><two/></foo>";
        assertDifferences(expected, actual, 1);
    }

    public void testBugClassCast() throws Exception
    {
        String expected = "<table><p>1</p><p>2</p><p>3</p></table>";
        String actual = "<table><p/><p/><p/></table>";
        assertXmlDifferent(expected, actual,
                "Expected child text \"1\" missing from inside element named \"p\"",
                "1", "", "<p />", "");
    }

    public void testCdata() throws Exception
    {
        assertXmlSame("<foo>bar</foo>", "<foo><![CDATA[bar]]></foo>");
    }

    public void testIgnoreElementOrder() throws Exception
    {
        checkIgnoringElementOrder("<foo><bar/><baz/></foo>", "<foo><baz/><bar/></foo>", true);
        checkIgnoringElementOrder("<foo><bar/><baz/></foo>", "<foo><baz/><bar/><bar/></foo>", false);
        checkIgnoringElementOrder("<foo><bar/><baz/><xxx/></foo>", "<foo><baz/><bar/><yyy/></foo>", false);
    }

    private void checkIgnoringElementOrder(String xmlExpected, String xmlActual, boolean expect) throws JDOMException, IOException
    {
        XmlDiff diff = new XmlDiff(xmlExpected, xmlActual);
        diff.setIgnoreElementOrder(true);
        assertEquals(diff.print(), expect, diff.same());
    }

}
