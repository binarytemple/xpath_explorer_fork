package net.sourceforge.xpe.xmlassert;

import java.lang.reflect.*;

import junit.framework.*;

public class XmlFixtureTest extends TestCase {
    public void testXpath() {
        XmlFixture xml = XmlFixture.fromText("<person><address type=\"test\">address value</address></person>");
        assertEquals("address value", xml.xpath("/person/address[@type='test']"));
    }

    public void testAsText() {
        String content = "<person/>";
        XmlFixture xml = XmlFixture.fromText(content);
        XmlFixture actual = XmlFixture.fromText(xml.asText());
        assertEquals(1, actual.xpathCount("/person"));
    }

    public void testXpathCount() {
        XmlFixture xml = XmlFixture.fromText("<person/>");
        assertEquals(0, xml.xpathCount("/person/test"));
    }

    public void testStaticFactoryMethodReturnTypes() {
        Class type = XmlFixture.class;
        Method[] method = type.getDeclaredMethods();
        for (int i = 0; i < method.length; i++) {
            if (matchFactoryMethodPattern(method[i])) {
                Class returnType = method[i].getReturnType();
                assertEquals(XmlFixture.class, returnType);
            }
        }
    }

    private boolean matchFactoryMethodPattern(Method method)
    {
        return Modifier.isStatic(method.getModifiers()) &&
                Modifier.isPublic(method.getModifiers()) &&
                method.getName().startsWith("from");
    }
}
