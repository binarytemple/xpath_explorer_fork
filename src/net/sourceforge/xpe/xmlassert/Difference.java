package net.sourceforge.xpe.xmlassert;

import org.jdom.*;

public class Difference
{
    private String message;
    private Object expectedNode; //todo: use JDOM b10 Node
    private Object actualNode; //todo: use JDOM b10 Node
    public static String NEWLINE = System.getProperty("line.separator");

    public Difference(String message, Object expectedNode, Object actualNode)
    {
        this.message = message;
        this.expectedNode = expectedNode;
        this.actualNode = actualNode;
    }

    public String getExpectedParentString()
    {
        return XmlDiff.asString(parent(expectedNode));
    }

    public String getActualParentString()
    {
        return XmlDiff.asString(parent(actualNode));
    }

    public Element getExpectedParent()
    {
        return parent(expectedNode);
    }

    public Element getActualParent()
    {
        return parent(actualNode);
    }

    private Element parent(Object node)
    {
        if (node == null) {
            return null;
        }
        if (node instanceof Element) {
            return ( (Element) node).getParent();
        }
        if (node instanceof Text) {
            return ( (Text) node).getParent();
        }
        System.err.println("Unknown node type " + node.getClass().getName());
        return null;
    }

    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (! (o instanceof Difference)) {
            return false;
        }

        final Difference difference = (Difference) o;

        if (actualNode != null ? !actualNode.equals(difference.actualNode) : difference.actualNode != null) {
            return false;
        }
        if (expectedNode != null ? !expectedNode.equals(difference.expectedNode) : difference.expectedNode != null) {
            return false;
        }
        if (message != null ? !message.equals(difference.message) : difference.message != null) {
            return false;
        }

        return true;
    }

    public int hashCode()
    {
        int result;
        result = (message != null ? message.hashCode() : 0);
        result = 29 * result + (expectedNode != null ? expectedNode.hashCode() : 0);
        result = 29 * result + (actualNode != null ? actualNode.hashCode() : 0);
        return result;
    }

    public String getMessage()
    {
        return message;
    }

    public String getExpectedString()
    {
        return XmlDiff.asString(expectedNode);
    }

    public String getActualString()
    {
        return XmlDiff.asString(actualNode);
    }

    public String print()
    {
        return getMessage() + NEWLINE +
                "  Parent: " + XmlDiff.asString(getExpectedParent()) + NEWLINE +
                "Expected: " + XmlDiff.asString(getExpectedString()) + NEWLINE +
                "  Actual: " + XmlDiff.asString(getActualString()) + NEWLINE;
    }

    public String toString()
    {
        return "Difference[message=" + message + ",expected=" + expectedNode + ",actual=" + actualNode + "]";
    }
}
