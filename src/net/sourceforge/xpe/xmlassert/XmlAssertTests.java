package net.sourceforge.xpe.xmlassert;

import junit.framework.*;

public class XmlAssertTests
        extends TestSuite
{
    public static Test suite()
    {
        TestSuite suite = new TestSuite("All XmlAssert Tests");
        suite.addTestSuite(XmlDiffTest.class);
        suite.addTestSuite(XmlAssertTest.class);
        return suite;
    }
}
