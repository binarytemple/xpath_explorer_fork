package net.sourceforge.xpe.xmlassert;

import junit.framework.*;
import org.jdom.Element;

public class XpathMatchTest extends TestCase {
    public void testMatch() {
        XmlFixture xml = XmlFixture.fromText("<person>pcdata</person>");
        XpathMatch match = XpathMatch.match(xml.getDocument(), "/person");
        assertEquals(1, match.count());
        assertEquals("pcdata", match.stringValue());
        assertEquals(Element.class, match.node().getClass());
        Element element = (Element) match.node();
        assertEquals("person", element.getName());
    }

    public void testNodeRequiresSingleMatch() {
        XmlFixture xml = XmlFixture.fromText("<person><item/><item>match</item></person>");
        XpathMatch match = XpathMatch.match(xml.getDocument(), "/person/item");
        try {
            match.node();
            fail("more than one match should cause exception");
        } catch(IllegalStateException e) {
            assertEquals(e.getMessage() + " is not good enough", true, e.getMessage().indexOf("/person/item") > -1);
        }
    }

    public void testNoMatch() {
        XmlFixture xml = XmlFixture.fromText("<person/>");
        XpathMatch match = XpathMatch.match(xml.getDocument(), "/person/fail");
        assertEquals(0, match.count());
        assertEquals(true, match.list().isEmpty());
        assertEquals(null, match.stringValue());
    }

}
