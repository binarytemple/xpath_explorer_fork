/*
 * ====================================================================
 * Copyright (c) 1995-2003 Purple Technology, Inc. All rights
 * reserved.
 *
 * PLAIN LANGUAGE LICENSE: Do whatever you like with this code, free
 * of charge, just give credit where credit is due. If you improve it,
 * please send your improvements to code@purpletech.com. Check
 * http://www.purpletech.com/code/ for the latest version and news.
 *
 * LEGAL LANGUAGE LICENSE: Redistribution and use in source and binary
 * forms, with or without modification, are permitted provided that
 * the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * 3. The names of the authors and the names "Purple Technology,"
 * "Purple Server" and "Purple Chat" must not be used to endorse or
 * promote products derived from this software without prior written
 * permission. For written permission, please contact
 * server@purpletech.com.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND PURPLE TECHNOLOGY ``AS
 * IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * AUTHORS OR PURPLE TECHNOLOGY BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * ====================================================================
 *
 **/


package net.sourceforge.xpe.xmlassert;

import java.io.*;
import java.text.*;
import java.util.*;

import org.apache.commons.lang.*;
import org.jdom.*;
import org.jdom.Attribute;
import org.jdom.filter.*;
import org.jdom.output.*;

public class XmlDiff
{
    public static void main(String[] args) throws Exception
    {
        XmlDiff xmlDiff = new XmlDiff(args);
        if (xmlDiff.same()) {
            System.exit(0);
        }
        else {
            System.out.println(xmlDiff.print());
            System.exit(1);
        }
    }

    public static String usage()
    {
        return "xmldiff expected.xml actual.xml"; //todo: add -validate
    }

    //todo: test me
    public XmlDiff(String[] args) throws Exception
    {
        if (args.length != 2) {
            throw new RuntimeException("Bad argument count " + usage());
        }

        set(load(args[0]), load(args[1]));
    }

    private Element rootElementExpected;
    private Element rootElementActual;
    private List differences;
    private boolean ignoreWhitespaceBetweenElements = true;
    private boolean trimWhitespaceInsideElements = false;
    private boolean ignoreElementOrder = false;

    public XmlDiff(String xmlExpected, String xmlActual) throws JDOMException, IOException
    {
        set(xmlExpected, xmlActual);
    }

    public XmlDiff(Document documentExpected, Document documentActual)
    {
        set(documentExpected, documentActual);
    }

    public XmlDiff(Element elementExpected, Element elementActual)
    {
        set(elementExpected, elementActual);
    }

    private void set(String xmlExpected, String xmlActual) throws JDOMException, IOException
    {
        set(parse(xmlExpected, false), parse(xmlActual, false));
    }

    private void set(Document documentExpected, Document documentActual)
    {
        this.rootElementExpected = documentExpected.getRootElement();
        this.rootElementActual = documentActual.getRootElement();
        reset();
    }

    private void set(Element elementExpected, Element elementActual)
    {
        this.rootElementExpected = elementExpected;
        this.rootElementActual = elementActual;
        reset();
    }

    public void setIgnoreWhitespaceBetweenElements(boolean ignoreWhitespaceBetweenElements)
    {
        this.ignoreWhitespaceBetweenElements = ignoreWhitespaceBetweenElements;
        reset();
    }

    public void setTrimWhitespaceInsideElements(boolean trimWhitespace)
    {
        this.trimWhitespaceInsideElements = trimWhitespace;
        reset();
    }

    public void setIgnoreElementOrder(boolean ignoreElementOrder)
    {
        this.ignoreElementOrder = ignoreElementOrder;
        reset();
    }

    private void reset()
    {
        differences = null;
    }

    private void scan()
    {
        differences = new ArrayList();
        different(rootElementExpected, rootElementActual);
        return;
    }

    public List differences()
    {
        if (differences == null) {
            scan();
        }
        return differences;
    }

    public boolean same()
    {
        return differences().isEmpty();
    }

    public Difference firstDifference()
    {
        if (differences().isEmpty()) {
            return null;
        }
        return (Difference) differences().get(0);
    }

    public String print()
    {
        StringBuffer buf = new StringBuffer();
        for (Iterator iterator = differences().iterator(); iterator.hasNext(); ) {
            Difference difference = (Difference) iterator.next();
            buf.append(difference.print());
            if (iterator.hasNext()) {
                buf.append(Difference.NEWLINE);
            }
        }
        return buf.toString();
    }

    private boolean different(Element elementExpected, Element elementActual)
    {
        //for different elements, should we abort, or scan children?
        //for now, abort, but should probably make it an option
        if (differentNames(elementExpected, elementActual)) {
            return true;
        }
        if (differentAttributes(elementExpected, elementActual)) {
            return true;
        }

        return differentChildren(elementExpected, elementActual);

    }

    private boolean differentChildren(Element elementExpected, Element elementActual)
    {
        List contentExpected = elementExpected.getContent(filter());
        List contentActual = elementActual.getContent(filter());

        if (ignoreElementOrder) {
            contentExpected = sort(copy(contentExpected));
            contentActual = sort(copy(contentActual));
        }

        ListIterator childrenExpected = contentExpected.listIterator();
        ListIterator childrenActual = contentActual.listIterator();

        boolean different = false;
        while (childrenExpected.hasNext() &&
                childrenActual.hasNext() && !different) {
            Object childExpected = childrenExpected.next();
            Object childActual = childrenActual.next();

            if (!sameNodeType(childExpected, childActual)) {
                addDifference("Expected " + descriptionOf(childExpected) +
                        " but found " + descriptionOf(childActual),
                        childExpected, childActual);
                different = true;

            }
            else {
                if (isElement(childExpected)) {
                    different = different( (Element) childExpected, (Element) childActual);
                }
                else if (isText(childExpected)) {
                    different = different( (Text) childExpected, (Text) childActual);
                }
                else {
                    // unknown node type -- ignore for now
                }
                // todo: look for the next match and sync up there
            }
            // workaround to make test pass
            if (different) {
                return different;
            }
        }
        if (childrenExpected.hasNext()) {
            Object childExpected = childrenExpected.next();
            addDifference("Expected child " + descriptionOf(childExpected) +
                    " missing from inside " + descriptionOf(elementActual),
                    childExpected, null);
            different = true;
        }
        if (childrenActual.hasNext()) {
            Object childActual = childrenActual.next();
            addDifference("Unexpected child " + descriptionOf(childActual) +
                    " found inside " + descriptionOf(elementActual),
                    null, childActual);
            different = true;
        }

        return different;
    }

    private List copy(List list)
    {
        List temp = new ArrayList();
        for (int i = 0; i < list.size(); ++i) {
            temp.add(list.get(i));
        }
        return temp;
    }

    private List sort(List content)
    {
        Collections.sort(content, new ContentComparator());
        return content;
    }

    class ContentComparator
            implements Comparator
    {

        public int compare(Object arg0, Object arg1)
        {
            if (arg0 instanceof Element) {
                if (arg1 instanceof Element) {
                    return ( (Element) arg0).getName().compareTo( ( (Element) arg1).getName());
                }
                else {
                    return -1;
                }
            }
            else if (arg0 instanceof Text) {
                if (arg1 instanceof Text) {
                    return ( (Text) arg0).getText().compareTo( ( (Text) arg1).getText());
                }
                else {
                    return 1;
                }
            }
            //TODO: comments
            return 0;
        }
    }

    private boolean isText(Object childExpected)
    {
        return childExpected instanceof Text || childExpected instanceof CDATA;
    }

    private boolean isElement(Object childExpected)
    {
        return childExpected instanceof Element;
    }

    private Filter filter()
    {
        Filter contentFilter = new Filter()
        {
            public boolean matches(Object o)
            {
                if (isText(o) && ignoreWhitespaceBetweenElements) {
                    String text = ( (Text) o).getText();
                    boolean isAllWhitespace = StringUtils.isWhitespace(text);
                    return!isAllWhitespace;
                }
                return true;
            }
        };
        return contentFilter;
    }

    private boolean sameNodeType(Object node1, Object node2)
    {
        return (isElement(node1) && isElement(node2)) ||
                (isText(node1) && isText(node2));
    }

    private boolean different(Text nodeExpected, Text nodeActual)
    {
        boolean differentText = false;
        String textExpected = text(nodeExpected);
        String textActual = text(nodeActual);
        if (!textExpected.equals(textActual)) {
            addDifference("Expected " + descriptionOf(nodeExpected) + " but found " + descriptionOf(nodeActual),
                    nodeExpected, nodeActual);
            differentText = true;
        }
        return differentText;
    }

    private String text(Text node)
    {
        String text = node.getText();
        if (trimWhitespaceInsideElements) {
            text = text.trim();
        }
        return text;
    }

    private boolean differentNames(Element elementExpected, Element elementActual)
    {
        String nameExpected = elementExpected.getName();
        String nameActual = elementActual.getName();
        if (!nameExpected.equals(nameActual)) {
            addDifference("Expected element named \"{0}\" but found element named \"{1}\"",
                    nameExpected, nameActual,
                    elementExpected, elementActual);
            return true;
        }
        return false;
    }

    private boolean differentAttributes(Element elementExpected, Element elementActual)
    {
        boolean different = false;
        List attributesExpected = sortedAttributes(elementExpected);
        for (Iterator iterator = attributesExpected.iterator(); iterator.hasNext(); ) {
            Attribute attributeExpected = (Attribute) iterator.next();
            Attribute attributeFound = elementActual.getAttribute(attributeExpected.getName());
            if (attributeFound == null) {
                addDifference("Expected " + descriptionOf(attributeExpected) +
                        " was missing",
                        elementExpected, elementActual);
                different = true;
            }
            else if (!attributeFound.getValue().equals(attributeExpected.getValue())) {
                addDifference("Expected " + descriptionOf(attributeExpected) +
                        " but found " + descriptionOf(attributeFound),
                        elementExpected, elementActual);
                different = true;
            }
        }

        List attributesActual = sortedAttributes(elementActual);
        for (Iterator iterator = attributesActual.iterator(); iterator.hasNext(); ) {
            Attribute attributeActual = (Attribute) iterator.next();
            Attribute found = elementExpected.getAttribute(attributeActual.getName());
            if (found == null) {
                addDifference("Additional attribute " + asString(attributeActual),
                        elementExpected, elementActual);
                different = true;
            }
        }

        return different;
    }

    //todo: use JDOM b10 Node
    //todo: extract formatter object?
    public static String asString(Object node)
    {
        if (node == null) {
            return "";
        }
        else if (node instanceof Element) {
            return asString( (Element) node);
        }
        else if (node instanceof Attribute) {
            return asString( (Attribute) node);
        }
        else if (node instanceof Text) {
            return asString( (Text) node);
        }
        else {
            return node.toString();
        }
    }

    public static String asString(Element element)
    {
        if (element == null) {
            return "";
        }
        Element clone = (Element) element.clone();
        clone.getContent().clear();
        XMLOutputter outputter = new XMLOutputter();
        outputter.setNewlines(false);
        outputter.setTrimAllWhite(true);
        String string = outputter.outputString(clone);
        return string;
    }

    private static String asString(Attribute attribute)
    {
        return attribute.getName() + "='" + attribute.getValue() + "'";
    }

    private static String asString(Text text)
    {
        return StringEscapeUtils.escapeJava(text.getText()); // should we trim this?
    }

    //todo: test me?
    String descriptionOf(Object o)
    {
        if (o instanceof Element) {
            return "element named \"" + ( (Element) o).getName() + "\"";
        }
        else if (o instanceof Attribute) {
            return "attribute " + asString( (Attribute) o);
        }
        else if (o instanceof Text) {
            return "text \"" + StringEscapeUtils.escapeJava( ( (Text) o).getText()) + "\""; // should we trim this?
        }
        else {
            return "object " + o.toString();
        }
    }

    private List sortedAttributes(Element element)
    {
        List attributes = new ArrayList(element.getAttributes());
        Collections.sort(attributes, new Comparator()
        {
            public int compare(Object o1, Object o2)
            {
                Attribute attribute1 = (Attribute) o1;
                Attribute attribute2 = (Attribute) o2;
                return attribute1.getName().compareTo(attribute2.getName());
            }
        });
        return attributes;
    }

    private void addDifference(String format, String expectedArg, String actualArg, Element expectedElement, Element actualElement)
    {
        String message = new MessageFormat(format).format(new String[] {
                expectedArg, actualArg});
        differences().add(new Difference(message, expectedElement, actualElement));
    }

    private void addDifference(String message, Object expectedNode, Object actualNode)
    {
        Difference difference = new Difference(message, expectedNode, actualNode);
        differences().add(difference);
    }

    public static Document parse(String xml, boolean validate) throws JDOMException, IOException
    {
        if (xml == null) { //todo: test me
            throw new RuntimeException("Must set 'xml' parameter");
        }
        CharArrayReader charArrayReader = new CharArrayReader(xml.toCharArray());
        return parse(charArrayReader, validate);
    }

    public static Document parse(Reader xmlReader, boolean validate) throws JDOMException, IOException
    {
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder(validate);
        return builder.build(xmlReader);
    }

    public static Document load(String filename) throws JDOMException, IOException
    {
        return load(new FileReader(filename));
    }

    public static Document load(File file) throws JDOMException, IOException
    {
        return load(new FileReader(file));
    }

    private static Document load(FileReader fileReader) throws JDOMException, IOException
    {
        org.jdom.input.SAXBuilder builder = new org.jdom.input.SAXBuilder(false);
        Document document = builder.build(fileReader);
        return document;
    }

}
