package net.sourceforge.xpe.xmlassert;

import java.util.*;

import org.jaxen.*;
import org.jaxen.function.*;
import org.jaxen.jdom.*;
import org.jdom.*;

public class XpathMatch
{
    private Document document;
    private JDOMXPath xpath;
    private List matched;
    private String xpathString;

    public XpathMatch(Document document, JDOMXPath xpath) {
        this.document = document;
        this.xpath = xpath;
        this.xpathString = xpath.getRootExpr().getText();
    }

    public static XpathMatch match(Document document, String xpath)
    {
        try {
            XpathMatch match = new XpathMatch(document, new JDOMXPath(xpath));
            match.xpathString = xpath;
            return match;
        }
        catch (JaxenException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }

    JDOMXPath getXpath() {
        return xpath;
    }

    public String stringValue()
    {
        Object node = node();
        return node == null ? null : StringFunction.evaluate(node, xpath.getNavigator());
    }

    public Object node()
    {
        if (count() == 0) {
            return null;
        }
        else if (count() > 1) {
            throw new IllegalStateException("more than one match found for xpath:"
                    + xpathString);
        }
        List list = list();
        return list.get(0);
    }

    public int count()
    {
        return list().size();
    }

    public List list() {
        if (matched == null) {
            try {
                matched = xpath.selectNodes(document);
            }
            catch (JaxenException ex) {
                throw new RuntimeException(ex.getMessage(), ex);
            }
        }
        return matched;
    }

    public boolean hasMatch()
    {
        return count() > 0;
    }
}
