package xpe;

/**
 * @author Alex Chaffee
 **/

import java.io.*;

public class FilePrintWriter extends PrintWriter {
    public FilePrintWriter(String filename) throws IOException {
        super(new BufferedWriter(new FileWriter(filename)));
    }

    public FilePrintWriter(File file) throws IOException {
        super(new BufferedWriter(new FileWriter(file)));
    }
}

