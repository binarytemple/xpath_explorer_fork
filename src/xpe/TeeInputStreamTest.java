package xpe;

import com.purpletech.util.*;

import java.io.*;
import java.util.*;

public class TeeInputStreamTest extends BaseTest
{
    public void testShort() throws Exception
    {
        String message = "test";
        check(message);
    }

    public void testLong() throws Exception
    {
        Random r= new Random();
        StringBuffer buf = new StringBuffer();
        for (int i=0; i<500000; ++i) {
            buf.append(((char)(r.nextInt(26) + 'a')));
        }
        check(buf.toString());
    }

    private void check(String message) throws IOException
    {
        InputStream in = new ByteArrayInputStream(message.getBytes());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        TeeInputStream tee = new TeeInputStream(in, out);
        String readFromTee = IOUtils.readStream(tee);
        assertEquals(message, readFromTee);
        assertEquals(message, out.toString());
    }

}
