package xpe;

import java.io.*;
import java.io.InputStream;
import java.net.*;
import java.net.URLConnection;

import javax.xml.parsers.*;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.tidy.Tidy;
import org.xml.sax.InputSource;

import com.sun.resolver.tools.CatalogResolver;

//todo: write tests for this class
public class DocumentLoaderImplPlain implements DocumentLoader
{
    CatalogResolver catalogResolver = new CatalogResolver();

    private void log(String s)
    {
        System.out.println(s);
    }
	/**
	 * @see xpe.DocumentLoader#loadDocument(String)
	 */
    public LoadedDocument loadDocument(String url) throws Exception
    {
        Document document;
        URLConnection connection = connectToUrl(url);
        connection.connect();  // is this OK to do twice?
        InputStream inputStream = connection.getInputStream();
        ByteArrayOutputStream arrayOut = new ByteArrayOutputStream();
        TeeInputStream tee = new TeeInputStream(inputStream, arrayOut);

        if (isHtml(connection, url))
        {
            document = loadDocumentTidy(tee);
        }
        else
        {
            document = loadDocumentJAXP(tee);
        }
        return new LoadedDocument(url, document, arrayOut.toString());
    }
    
	/**
	 * @see xpe.DocumentLoader#supportsNamespaces()
	 */
    public boolean supportsNamespaces(){
    	return true;
    }

    private boolean isHtml(URLConnection connection, String url)
    {
        boolean isHtml = false;
        String contentType = connection.getContentType();
        if (contentType.startsWith("text/html")
                || url.endsWith(".html")
                || url.endsWith(".htm")
                || url.endsWith(".HTML")
                || url.endsWith(".HTM"))
        isHtml = true;
        return isHtml;
    }

    private URLConnection connectToUrl(String url) throws IOException
    {
        URL urlObject = new URL(url);
        URLConnection connection = urlObject.openConnection();
        if (connection instanceof HttpURLConnection)
        {
            log("Connecting to " + url);
            HttpURLConnection httpConnection = (HttpURLConnection) connection;
            httpConnection.setRequestProperty("host", urlObject.getHost());
            httpConnection.setInstanceFollowRedirects(false);
            httpConnection.connect();
            if (httpConnection.getResponseCode() == 302) {
                String newUrl = httpConnection.getHeaderField("Location");
                log("Redirecting to " + newUrl);
                // todo: deal with relative URLs inside Location
                connection = connectToUrl(newUrl);
            }
        }
        log("Connected to " + url + ": type=" + connection.getContentType() +
                ", length=" + connection.getContentLength());
        return connection;
    }

    private Document loadDocumentJAXP(InputStream in) throws Exception
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setNamespaceAware(true);
        factory.setValidating(false);
        DocumentBuilder builder = factory.newDocumentBuilder();
        builder.setEntityResolver(catalogResolver);
        Document document = builder.parse(new InputSource(in));
        document.normalize();
        return document;
    }

    private Document loadDocumentXerces(String url) throws Exception
    {
        org.apache.xerces.parsers.DOMParser parser =
            new org.apache.xerces.parsers.DOMParser();
        parser.setEntityResolver( catalogResolver );
        parser.parse(new InputSource(url));
        Document document = parser.getDocument();
        document.normalize();
        return document;
    }

    private Document loadDocumentTidy(InputStream in)
    {
        Tidy tidy = new Tidy();
        tidy.setCharEncoding(org.w3c.tidy.Configuration.UTF8);
        tidy.setQuiet(true);
        tidy.setShowWarnings(false);
        tidy.setOnlyErrors(true);
        StringPrintWriter err = new StringPrintWriter();
        tidy.setErrout(err);
        Document document = tidy.parseDOM(in, null);
        String errors = err.getString();
        if (!errors.equals(""))
        {
            //todo: log this
//            log("Tidy complaints: " + errors);
        }
        document.normalize();

        return document;
    }

}
