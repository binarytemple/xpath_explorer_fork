package xpe.gui;

import java.awt.*;
import java.io.*;
import java.io.File;
import java.util.*;
import java.util.List;

import javax.swing.*;
import javax.swing.text.JTextComponent;
import javax.swing.tree.*;

import org.w3c.dom.*;

import com.purpletech.util.Utils;
import xpe.*;

public class XPEPanel extends JPanel
{
    // these are all package-protected since the test needs to access them directly

    JComboBox comboURL;
    JComboBox comboXPath;

    JList listResults;
    DefaultListModel modelResultsList;

    DOMTree treeContents;

    JEditorPane textMessage;
    JTextArea textSource;

    JTextField textStringValue;
    JTextField textNumberValue;
    JTextField textBooleanValue;
    JTextField textExpanded;
    JTextField textGenerated;

    JScrollPane scrollResults;
    JScrollPane scrollContents;
    JScrollPane scrollMessage;
    JScrollPane scrollSource;

    JTabbedPane tab;

    public Actions actions;

    JButton buttonLoad;
    JButton buttonEvaluate;
    JButton buttonUse;

    private CompatibleProgressBar progress;

    // model
    XPE xpe;

    public XPEPanel()
    {
        init("", "");
    }

    public XPEPanel(String url, String xpath)
    {
        init(url, xpath);
        update();
    }

    private void init(String url, String xpath)
    {
        new XPEPanelInit(this).init(url, xpath);
    }

    private static String getComboValue(JComboBox combo)
    {
        return (String) combo.getSelectedItem();
    }

    private void addComboUnique(JComboBox combo, String item)
    {
        MutableComboBoxModel model = (MutableComboBoxModel) combo.getModel();

        if (!getComboList(combo).contains(item))
            model.addElement(item);
        // silly combo box gets confused if we insert at the top -- be
        // warned if you reimplement to do most-recent-first ordering
    }

    private List getComboList(JComboBox combo)
    {
        ComboBoxModel model = combo.getModel();
        List list = new ArrayList();
        for (int i = 0; i < model.getSize(); ++i)
        {
            list.add(model.getElementAt(i));
        }
        return Collections.unmodifiableList(list);
    }

    public String getURL()
    {
        return getComboValue(comboURL);
    }

    public void setURL(String url)
    {
        comboURL.setSelectedItem(url); //note: this sends an action event which calls update()
        updateHistory();
    }

    private void updateSource() throws Exception
    {
        textSource.setText(xpe.getSource());
    }

    private void updateHistory()
    {
        addComboUnique(comboURL, getURL());
        addComboUnique(comboXPath, getXPath());
    }

    public List getURLHistory()
    {
        return getComboList(comboURL);
    }

    public String getXPath()
    {
        return getComboValue(comboXPath);
    }

    public void setXPath(String xpath)
    {
        comboXPath.setSelectedItem(xpath);
        updateHistory();
    }

    public List getXPathHistory()
    {
        return getComboList(comboXPath);
    }

    public String getMessage()
    {
        return textMessage.getText();
    }

    public String getExpanded()
    {
        return textExpanded.getText();
    }

    public String getGenerated()
    {
        return textGenerated.getText();
    }

    public String getStringValue()
    {
        return textStringValue.getText();
    }

    public String getNumberValue()
    {
        return textNumberValue.getText();
    }

    public String getBooleanValue()
    {
        return textBooleanValue.getText();
    }

    void showMessage(String message)
    {
        setMessageText(message);
        tab.setSelectedComponent(scrollMessage);
        scrollMessage.getViewport().scrollRectToVisible(new Rectangle(0,0,1,1));
    }

    private void showContents()
    {
        tab.setSelectedComponent(scrollContents);
    }

    private void setMessageText(String message) {
        textMessage.setText(message);
    }

    private void appendMessage(String message) {
        String existing = textMessage.getText();
        if (!existing.equals("")) {
            existing += "\n";
        }
        textMessage.setText(existing + message);
    }

    private void clearMessage()
    {
        textMessage.setText("");
    }

    private void setResultsList() throws Exception
    {
        // todo: preserve selection (get selection before, set selection afterwards)
        Collection results = xpe.getMatches();
        modelResultsList.clear();
        Iterator i = results.iterator();
        while (i.hasNext())
        {
            Object obj = i.next();
            if (obj instanceof Node)
            {
                modelResultsList.addElement(
                    new DOMTreeNode((Node) obj, results, false, false, false));
            } else
            {
                modelResultsList.addElement(obj);
            }
        }
    }

    private void startWaiting()
    {
        this.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
    }

    private void stopWaiting()
    {
        this.setCursor(Cursor.getDefaultCursor());
    }

    public void load()
    {
        // easiest way
        xpe = null;
        update();
    }

    public void update()
    {
        try
        {
            clearMessage();
            showContents();
            startWaiting();
            boolean differentUrl = updateXPE();
            updateSource();
            updateHistory();
            updateContents(differentUrl);
            setResultsList();
            setStringValue(xpe.getStringValue());
            Number num = xpe.getNumberValue();
            textNumberValue.setText(num == null ? "null" : num.toString());
            textBooleanValue.setText(xpe.getBooleanValue() ? "true" : "false");
            textExpanded.setText(xpe.getExpandedXPath());
            selectNodeFromContents();
        }
        catch (Throwable t)
        {
            displayError(t);
        }
        finally
        {
            hideProgressBar();
            stopWaiting();
        }
    }

    private void showProgressBar(String message)
    {
        JViewport viewport = scrollContents.getViewport();

        viewport.remove(treeContents);
        progress = new CompatibleProgressBar();
        progress.setStringPainted(true);
        progress.setString(message);
        progress.setIndeterminate(true);

        treeContents.setProgressBar(progress);
        viewport.setView(progress);
    }

    private void hideProgressBar() {
        treeContents.setProgressBar(null);
        JViewport viewport = scrollContents.getViewport();
        viewport.setView(treeContents);
    }

    void updateContents(boolean newUrl) throws Exception
    {
        if (newUrl) showProgressBar("Updating contents tree...");
        treeContents.setDocument(xpe.getDocument(), xpe.getMatches());
        if (newUrl) maybeExpandAll();
    }

    private void maybeExpandAll() throws Exception
    {
        Document document = xpe.getDocument();
        int nodeCount = countDescendants(document);
        if (nodeCount > 500) {
            appendMessage("Skipping node expansion (size " + nodeCount + ")");
        } else {
            treeContents.expandAll();
        }
    }

    private static int countDescendants(Node node)
    {
        NodeList childNodes = node.getChildNodes();
        int nodeCount = childNodes.getLength();
        for (int i = 0; i < childNodes.getLength(); ++i)
        {
            Node child = childNodes.item(i);
            nodeCount += countDescendants(child);
        }
        return nodeCount;
    }

    public void expandAll() {
        showProgressBar("Expanding all nodes...");
        treeContents.expandAll();
        hideProgressBar();
        tab.setSelectedComponent(scrollContents);
    }

    public void collapseAll() {
        showProgressBar("Collapsing all nodes...");
        treeContents.collapseAll();
        hideProgressBar();
        tab.setSelectedComponent(scrollContents);
    }

    private boolean updateXPE() throws Exception
    {
        if (xpe == null)
        {
            xpe = new XPE(getURL(), getXPath());
            return true;
        }
        else
        {
            boolean differentUrl = !xpe.getURL().equals(getURL());
            boolean differentXpath = !xpe.getXPath().equals(getXPath());

            if (differentUrl) {
                showProgressBar("Loading " + getURL());
            }

            if (differentUrl && differentXpath) {
                xpe.setDocumentAndXpath(getURL(), getXPath());
            }
            else if (differentUrl) {
                xpe.setDocument(getURL());
            }
            else if (differentXpath) {
                xpe.setXPath(getXPath());
            }
            return differentUrl;
        }
    }

    private void setStringValue(String s)
    {
        textStringValue.setText(Utils.javaEscape(s));
    }

    private void displayError(Throwable t)
    {
        displayError(null, t);
    }

    void displayError(String message, Throwable t)
    {
        StringPrintWriter s = new StringPrintWriter();
        if (message != null)
            s.println(message);
        t.printStackTrace(s);
        showMessage(s.getString());
    }

    public void useGenerated()
    {
        setXPath(getGenerated());
    }

    private Node selectedNode;

    private Node getSelectedNode()
    {
        return selectedNode;
    }

    private Node setSelectedNode(Node selectedNode)
    {
        this.selectedNode = selectedNode;
        return selectedNode;
    }

    public void selectNodeFromContents()
    {
        selectNode(getSelectedNodeFromContentsTree());
    }

    public void selectNodeFromResults()
    {
        selectNode(getSelectedNodeFromResults());
    }

    /*test*/
    Node getSelectedNodeFromContentsTree()
    {
        TreePath selection = treeContents.getSelectionPath();
        if (selection == null) {
            return null;
        }
        DOMTreeNode treenode = (DOMTreeNode) selection.getLastPathComponent();
        if (treenode == null) {
            return null;
        }
        return treenode.getNode();
    }

    /*test*/
    Node getSelectedNodeFromResults()
    {
        Object selected = listResults.getSelectedValue();
        if (selected instanceof DOMTreeNode)
        {
            return ((DOMTreeNode) selected).getNode();
        }
        return null;
    }

    public void selectNode(Node node)
    {
        setSelectedNode(node);
        if (node != getSelectedNodeFromContentsTree())
            selectNodeInContentsTree(node);
        if (node != getSelectedNodeFromResults())
            selectNodeInResultsList(node);
        generate();
    }

    void selectNodeInContentsTree(Node target)
    {
        treeContents.selectNode(target);
    }

    void selectNodeInResultsList(Node target)
    {
        if (target == null)
        {
            listResults.setSelectedIndices(new int[] {
            });
            return;
        }
        for (int i = 0; i < listResults.getModel().getSize(); ++i)
        {
            Object obj = listResults.getModel().getElementAt(i);
            if (obj instanceof DOMTreeNode)
            {
                if (((DOMTreeNode) obj).getNode() == target)
                {
                    listResults.setSelectedIndex(i);
                    return;
                }
            }
        }
        // if we didn't find it, then clear the selection
        listResults.setSelectedIndices(new int[] {
        });
    }

    private void generate()
    {
        String text;
        if (getSelectedNode() == null)
            text = "select a node to generate its xpath";
        else
            text = XPE.generateXPath(getSelectedNode());

        textGenerated.setText(text);
    }

    File currentDirectory;

    public void openFileDialog()
    {
        JFileChooser chooser = new JFileChooser();
        if (currentDirectory != null) {
            chooser.setCurrentDirectory(currentDirectory);
        }
        int returnVal = chooser.showOpenDialog(this);
        currentDirectory = chooser.getCurrentDirectory();
        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            xpe = null; // make it so that the file will definitely get loaded
            File file = chooser.getSelectedFile();
            String url = toFileUrl(file);
            this.setURL(url); // ultimately sends an update()
        }
    }

    public String toFileUrl(File file)
    {
        try
        {
            String path = file.getCanonicalPath();
            if (File.separatorChar == '\\') {
                path = "/" + path.replace('\\', '/');
            }
            return "file://" + path;
        } catch (IOException e)
        {
            displayError(e);
            return "";
        }
    }

    public void openLocation() {
        showMessage("To open a document, type its URL into the 'URL' field above.");
        getComboUrlTextComponent().selectAll();
        getComboUrlTextComponent().requestFocus();
    }

    public JTextComponent getComboUrlTextComponent() {
        return (JTextComponent) this.comboURL.getEditor().getEditorComponent();
    }

    public void popupMenuAt(Point point) {
        JPopupMenu menu = new JPopupMenu();
        TreeNode node = treeContents.getSelectedNode();
        if (node != null && !node.isLeaf()) {
            menu.add(actions.expandCurrent);
//            For some reason this doesn't work; hide it for now
//            menu.add(actions.collapseCurrent);
        }

        Point locationOnScreen = treeContents.getLocationOnScreen();
        point.translate(locationOnScreen.x, locationOnScreen.y);
        menu.setInvoker(treeContents);
        menu.setLocation(point);
        menu.setVisible(true);
    }

    public void expandCurrent() {
        treeContents.expandCurrent();
    }

    public void collapseCurrent() {
        treeContents.collapseCurrent();
    }

}
