package xpe.gui;

import javax.swing.*;
import java.awt.event.*;

import xpe.DocumentLoaderFactory;

class Actions
{
    public Action load;
    public ThreadedAction evaluate;
    public Action use;  // should this be threaded?
    public Action openFile;
    public Action openLocation;
    public Action expandAll;
    public Action collapseAll;
    public Action expandCurrent;
	public Action collapseCurrent;
	public Action chooseDefaultLoader;
	public Action chooseHttpUnitLoader;
	public Action preserveTags;

    // todo: move this into each anonymous inner class; make an XPEAction superclass
    private XPEPanel xpepanel;

    public Actions(XPEPanel xpepanel)
    {
        this.xpepanel = xpepanel;
        init();
    }

    private void init()
    {
        ImageIcon icon;

        icon = loadIcon("evaluate.gif");
        evaluate = new ThreadedAction
                (new AbstractAction("Evaluate", icon)
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.update();
                    }
                });

        icon = loadIcon("load.gif");
        load = new ThreadedAction
                (new AbstractAction("Load", icon)
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.load();
                    }
                });

        icon = loadIcon("use.gif");
        use = new AbstractAction("Use This Path", icon)
        {
            public void actionPerformed(ActionEvent e)
            {
                xpepanel.useGenerated();
            }
        };

        icon = loadIcon("open.gif");
        openFile = new ThreadedAction
                (new AbstractAction("Open File...", icon)
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.openFileDialog();
                    }
                });

        openLocation = new ThreadedAction
                (new AbstractAction("Open Location")
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.openLocation();
                    }
                });

        expandAll = new ThreadedAction
                (new AbstractAction("Expand All")
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.expandAll();
                    }
                });

        collapseAll = new ThreadedAction
                (new AbstractAction("Collapse All")
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.collapseAll();
                    }
                });

        expandCurrent = new ThreadedAction
                (new AbstractAction("Expand This Node")
                {
                    public void actionPerformed(ActionEvent e)
                    {
                        xpepanel.expandCurrent();
                    }
                });

		collapseCurrent = new ThreadedAction
				(new AbstractAction("Collapse This Node")
				{
					public void actionPerformed(ActionEvent e)
					{
						xpepanel.collapseCurrent();
					}
				});

		chooseDefaultLoader = new ThreadedAction
				(new AbstractAction("Choose Default Loader")
				{
					public void actionPerformed(ActionEvent e)
					{
						DocumentLoaderFactory.instance().setDocumentLoaderClassName("xpe.DocumentLoaderImplPlain");
					}
				});

		chooseHttpUnitLoader = new ThreadedAction
				(new AbstractAction("Choose HttpUnit Loader")
				{
					public void actionPerformed(ActionEvent e)
					{
						try {
							DocumentLoaderFactory.instance().setDocumentLoaderClassName("xpe.DocumentLoaderImplHttpUnit");
						} catch (RuntimeException re){
							setEnabled(false);
							xpepanel.displayError("Setting Loader Failed:", re);
						}
					}
				});

    }

    private ImageIcon loadIcon(String filename)
    {
        return XPEPanelInit.loadIcon(filename);
    }

}
