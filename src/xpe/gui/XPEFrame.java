package xpe.gui;

import xpe.*;

import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.*;

public class XPEFrame extends JFrame {
    public static void main(String[] args) throws IOException {
        XPEFrame frame;

        String url = getDefaultUrlString();
        String xpath = "//";

        installLookAndFeel();

        if (args.length > 0) {
            url = args[0];
            if (args.length > 1) {
                xpath = args[1];
            }
        }

        frame = displayFrame(url, xpath);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private static void installLookAndFeel()
    {
        // If we're a Mac, stick with the platform L&F
        // See http://developer.apple.com/technotes/tn/tn2042.html
        if (System.getProperty("mrj.version") != null)
        {
            return;
        }
        
        try {
            // Kunststoff L&F courtesy of http://www.incors.org/
            UIManager.setLookAndFeel(new com.incors.plaf.kunststoff.KunststoffLookAndFeel());
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
    }

    public static String getDefaultUrlString() {
        String url;
        URL urlDefault = XPEFrame.class.getResource("/xml/id.xml");
        if (urlDefault == null)
            urlDefault = XPEFrame.class.getResource("xml/id.xml");
        if (urlDefault == null)
            url = "";
        else
            url = urlDefault.toString();
        return url;
    }

    public static XPEFrame displayFrame(String url, String xpath)
        throws IOException {
        XPEFrame frame;
        frame = new XPEFrame(url, xpath);
        frame.setSize(620, 440);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        return frame;
    }

    XPEPanel panel;
    JMenuBar menubar;
    JMenu menuSampleFiles;
    JMenu menuFile;
    JMenu menuNodes;
    JMenu menuParsers;

    public XPEFrame(String url, String xpath) throws IOException {
        super(makeTitle());

        this.setIconImage(XPEPanelInit.loadIcon("x.gif").getImage());
        this.getContentPane().setLayout(new BorderLayout());
        panel = new XPEPanel(url, xpath);
        this.getContentPane().add(panel);

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        menubar = new JMenuBar();
        this.setJMenuBar(menubar);

        menuFile = new JMenu("File");
        Action quit = new AbstractAction("Quit") {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        };
        menuFile.add(panel.actions.openFile);
        menuFile.add(panel.actions.openLocation);
        menuFile.add(quit);
        menubar.add(menuFile);

        menuNodes = new JMenu("Nodes");
        menuNodes.add(panel.actions.expandAll);
        menuNodes.add(panel.actions.collapseAll);
        menubar.add(menuNodes);

        menuSampleFiles = new JMenu("Sample Files");
        menuSampleFiles.setIcon(XPEPanelInit.loadIcon("file.gif"));
        for (Iterator i = URLSetter.getSamples(panel).iterator();
            i.hasNext();
            ) {
            menuSampleFiles.add(new ThreadedAction((Action) i.next()));
        }
        menubar.add(menuSampleFiles);
        
        menuParsers = new JMenu("Doc Loaders");
		menuParsers.add(panel.actions.chooseDefaultLoader);
		menuParsers.add(panel.actions.chooseHttpUnitLoader);
		menubar.add(menuParsers);

    }

    protected static String makeTitle() {
        String title = "XPath Explorer by Purple Technology";
        if (Version.getVersion() != null) {
            title += " - Build " + Version.getVersion();
        }
        return title;
    }

}
