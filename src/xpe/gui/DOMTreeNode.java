

package xpe.gui;

import java.util.*;

import javax.swing.tree.DefaultMutableTreeNode;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import xpe.*;

public class DOMTreeNode extends DefaultMutableTreeNode
{
    private static final String HTML_NULL = "<html></html>";
    static final String HTML_START = "<html><font face='sans'>";
    static final String HTML_END = "</font></html>";

    Node node;
    Collection selectedNodes;
    String text;
    private boolean isCloseTag;
    private boolean expandAttributes;
    

    /**
     * creates an entire tree of treenodes based on the document
     * (creates and adds its children too)
     **/
    public DOMTreeNode(Document document, Collection selectedNodes, boolean expandAttributes) {
        this(document, selectedNodes, false, true, expandAttributes);
    }

    /**
     * creates a single treenode
     * */
    public DOMTreeNode(Node node, Collection selectedNodes, boolean isCloseTag, boolean addChildren, boolean expandAttributes) {
        this.node = node;
        this.selectedNodes = selectedNodes;
        this.isCloseTag = isCloseTag;
        this.expandAttributes = expandAttributes;
        if (addChildren)
            addChildren();
    }

    public void setMatches(Collection selectedNodes) {
        this.selectedNodes = selectedNodes;
        for (int i=0; i<getChildCount(); ++i)
        {
            DOMTreeNode child = (DOMTreeNode)getChildAt(i);
            child.setMatches(selectedNodes);
        }
    }

    private void addElementNode(Node child) {
        DOMTreeNode treenodeChild = new DOMTreeNode(child, selectedNodes, false, false, expandAttributes);
        this.add(treenodeChild);
        if (child.getChildNodes().getLength() > 0) {
            treenodeChild.addChildren();
            this.add(new DOMTreeNode(child, selectedNodes, true, false, expandAttributes));
        }
    }

    private void addTextNode(Node child) {
        String text = child.getNodeValue();
        if (text != null)
        {
            text = text.trim();
            if (!"".equals(text))
            {
                DOMTreeNode treenodeText = new DOMTreeNode(child, selectedNodes, false, false, expandAttributes);
                this.add(treenodeText);
            }
        }
    }

    private void addChildren()
    {
        if (node == null)
            return;

        int c = this.node.getChildNodes().getLength();
        for (int i=0; i<c; ++i)
        {
            Node child = this.node.getChildNodes().item(i);
            switch (child.getNodeType()) {
            case Node.ELEMENT_NODE :        // The node is an Element.
                addElementNode(child);
                break;

            case Node.TEXT_NODE :            // The node is a Text node.
                addTextNode(child);
                break;

            case Node.ATTRIBUTE_NODE :        // The node is an Attr.
            case Node.DOCUMENT_NODE :        // The node is a Document.
            case Node.CDATA_SECTION_NODE :        // The node is a CDATASection.
            case Node.COMMENT_NODE :        // The node is a Comment.
            case Node.DOCUMENT_FRAGMENT_NODE :    // The node is a DocumentFragment.
            case Node.DOCUMENT_TYPE_NODE :        // The node is a DocumentType.
            case Node.ENTITY_NODE :            // The node is an Entity.
            case Node.ENTITY_REFERENCE_NODE :    // The node is an EntityReference.
            case Node.NOTATION_NODE :        // The node is a Notation.
            case Node.PROCESSING_INSTRUCTION_NODE : // The node is a ProcessingInstruction.
            default:
                break;
            }
        }
    }

    public String toString()
    {
        return toString(selectedNodes);
    }

    public String toString(Collection selectedNodes)
    {
        if (node == null)
            return HTML_NULL;

        StringPrintWriter out = new StringPrintWriter();
        out.print(HTML_START);
        if (node.getNodeType() == Node.TEXT_NODE)
        {
            String value = node.getNodeValue();
            if (value != null)
                out.print(value.trim());
        }
        else
        {
            NodePrinter printer = new NodePrinter(out, selectedNodes, true, expandAttributes);
            if (isCloseTag) {
                printer.printCloseTag(node, 0);
            }
            else {
                printer.printSingleNode(node,0);
            }
        }
        out.print(HTML_END);
        return out.getString();
    }

    public Node getNode() {
        return node;
    }
    
    public Set getAllNodes() {
        return getAllNodes(new HashSet(), false);
    }

    public Set getAllLeafNodes() {
        return getAllNodes(new HashSet(), true);
    }

    private Set getAllNodes(Set set, boolean onlyLeaves) {
        if (!onlyLeaves || this.isLeaf()) {
            set.add(this);
        }
        int i=0;
        while (i < this.getChildCount()) {
            DOMTreeNode child = (DOMTreeNode)getChildAt(i);
            child.getAllNodes(set, onlyLeaves);
            ++i;
        }
        return set;
    }
    
}
