package xpe.gui;

import java.io.*;
import java.util.*;

import org.w3c.dom.*;

import com.purpletech.util.*;

/**
 *
 **/
public class NodePrinter
{

    static List EMPTY_LIST = new ArrayList();

    PrintWriter out;
    Collection selectedNodes;

    String indentSpace = "  ";
    String openTagStart = "<";
    String openTagEnd = ">";
    String selfClosingTagEnd = "/>";
    String closeTagStart = "</";
    String closeTagEnd = ">";
    String attributeQuoteStart = "'";
    String attributeQuoteEnd = "'";
    String matchStart = "*";
    String matchEnd = "*";

    String eol;

    boolean htmlMode;
    private boolean expandAttributes;

    public NodePrinter(PrintWriter out)
    {
        this(out, EMPTY_LIST, false, false);
    }

    public NodePrinter(PrintWriter out, Collection selectedNodes)
    {
        this(out, selectedNodes, false, false);
    }

    public NodePrinter(
        PrintWriter out,
        Collection selectedNodes,
        boolean htmlMode,
        boolean expandAttributes)
    {
        if (selectedNodes == null)
            selectedNodes = EMPTY_LIST;

        this.out = out;
        this.selectedNodes = selectedNodes;
        this.htmlMode = htmlMode;
        this.expandAttributes = expandAttributes;

        if (htmlMode)
        {
            initHTML();
        }
        else
        {
            initText();
        }
    }

    private void initText()
    {
        indentSpace = "  ";
        openTagStart = "<";
        openTagEnd = ">";
        selfClosingTagEnd = "/>";
        closeTagStart = "</";
        closeTagEnd = ">";
        attributeQuoteStart = "'";
        attributeQuoteEnd = "'";

        matchStart = "*";
        matchEnd = "*";

        eol = System.getProperty("line.separator");
    }

    private void initHTML()
    {
        indentSpace = "&nbsp;&nbsp;";
        openTagStart = "&lt;";
        openTagEnd = "&gt;";
        selfClosingTagEnd = "/&gt;";
        closeTagStart = "&lt;/";
        closeTagEnd = "&gt;";
        attributeQuoteStart = "'";
        attributeQuoteEnd = "'";
        matchStart = "<font color='blue'><b class='match'>";
        matchEnd = "</b></font>";

        eol = "<br>" + System.getProperty("line.separator");
    }

    private void indent(int indent)
    {
        for (int i = 0; i < indent; ++i)
        {
            out.print(indentSpace);
        }
    }

    private void println()
    {
        out.print(eol);
    }

    public void printFullNode(Node node) throws Exception
    {
        printFullNode(node, 0);
    }

    public void printFullNode(Node node, int indent) throws Exception
    {
        if (printSingleNode(node, indent))
            println();

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i)
        {
            printFullNode(children.item(i), indent + 1);
        }

        if (children.getLength() > 0)
        {
            printCloseTag(node, indent);
            println();
        }
    }

    public void printCloseTag(Node node, int indent)
    {
        indent(indent);
        out.print(closeTagStart + node.getNodeName() + closeTagEnd);
    }

    public boolean printSingleNode(Node node, int indent)
    {

        switch (node.getNodeType())
        {

            case Node.TEXT_NODE : // The node is a Text node.
                return printTextNode(node, indent);

            case Node.ATTRIBUTE_NODE : // The node is an Attr.
                printAttributeNode(node);
                return true;

            case Node.DOCUMENT_NODE : // The node is a Document.
            case Node.CDATA_SECTION_NODE : // The node is a CDATASection.
            case Node.COMMENT_NODE : // The node is a Comment.
            case Node.DOCUMENT_FRAGMENT_NODE :
            // The node is a DocumentFragment.
            case Node.DOCUMENT_TYPE_NODE : // The node is a DocumentType.
            case Node.ELEMENT_NODE : // The node is an Element.
            case Node.ENTITY_NODE : // The node is an Entity.
            case Node.ENTITY_REFERENCE_NODE :
            // The node is an EntityReference.
            case Node.NOTATION_NODE : // The node is a Notation.
            case Node.PROCESSING_INSTRUCTION_NODE :
            // The node is a ProcessingInstruction.
            default :
                printElementNode(node, indent);
                return true;

        }
    }

    private boolean printTextNode(Node node, int indent)
    {
        String data = ((Text) node).getData();
        if (data == null)
            return false;
        data = data.trim();
        if ("".equals(data))
            return false;
        indent(indent);
        printText(data);
        return true;
    }

    private void printText(String data)
    {
        if (data == null)
            return;
        if (htmlMode)
            out.print(Utils.htmlescape(data));
        else
            out.print(data);
    }

    private void printAttributeNode(Node node)
    {
        if (selectedNodes.contains(node))
        {
            out.print(matchStart + node.getNodeName() + matchEnd);
        }
        else
        {
            out.print(node.getNodeName());
        }
        out.print("=");
        out.print(attributeQuoteStart);
        printText(node.getNodeValue());
        out.print(attributeQuoteEnd);
    }

    /** prints the open tag of an element (not the contents or close tag) **/
    private void printElementNode(Node node, int indent)
    {
        printElementStart(node, indent);
        if (expandAttributes)
        {
            if (!hasAttributes(node))
                printElementEnd(node);
        }
        else
        {
            printAttributes(node);
            printElementEnd(node);
        }
    }

    private void printElementStart(Node node, int indent)
    {
        indent(indent);

        if (selectedNodes.contains(node))
        {
            out.print(
                openTagStart + matchStart + node.getNodeName() + matchEnd);
        }
        else
        {
            out.print(openTagStart + node.getNodeName());
        }
    }

    private void printAttributes(Node node)
    {
        if (hasAttributes(node))
        {
            NamedNodeMap attributes = node.getAttributes();
            for (int i = 0; i < attributes.getLength(); ++i)
            {
                Node attribute = attributes.item(i);
                out.print(" ");
                printAttributeNode(attribute);
            }
        }
    }

    private boolean hasAttributes(Node node)
    {
        return node.getAttributes() != null
            && node.getAttributes().getLength() != 0;
    }

    public void printElementEnd(Node node)
    {
        NodeList children = node.getChildNodes();
        if (children.getLength() == 0)
            out.print(selfClosingTagEnd);
        else
            out.print(openTagEnd);
    }

}
