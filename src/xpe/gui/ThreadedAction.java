package xpe.gui;

import javax.swing.*;
import java.awt.event.ActionEvent;

public class ThreadedAction extends AbstractAction {
    private Action action;
    private Thread thread;
    private int count = 0;

    public ThreadedAction(Action action) {
//todo: try super((String) action.getValue(Action.NAME), (Icon)action.getValue(Action.SMALL_ICON));
        super((String) action.getValue(Action.NAME));
        Icon icon = (Icon) action.getValue(Action.SMALL_ICON);
        if (icon != null) {
            putValue(Action.SMALL_ICON, icon);
        }
        this.action = action;
    }

    synchronized public void actionPerformed(final ActionEvent e) {
        thread = new Thread() {
            public void run() {
                action.actionPerformed(e);
                increment();
                removeThread();
            }
        };
        thread.start();
    }

    synchronized private void removeThread() {
        thread = null;
    }

    synchronized private void increment() {
        count++;
        this.notifyAll();
    }

    synchronized public int getCount() {
        return count;
    }

    synchronized public void waitUntilFinished(int targetCount) {
        while (count < (targetCount+1)) {
            try {
                this.wait();
            } catch (InterruptedException e) {
            }
        }
    }
}
