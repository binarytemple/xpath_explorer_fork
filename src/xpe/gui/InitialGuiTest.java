package xpe.gui;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.netbeans.jemmy.JemmyProperties;
import org.netbeans.jemmy.TestOut;
import org.netbeans.jemmy.operators.JFrameOperator;
import org.netbeans.jemmy.operators.JTextFieldOperator;

public class InitialGuiTest extends TestCase
{
	private JFrameOperator xpeFrame;
	private JTextFieldOperator urlEntryField;
	private JTextFieldOperator xpathEntryField;

	public InitialGuiTest(String name)
	{
		super(name);
	}

	public void startApp() throws Exception
	{
//      dk: uncomment to use the robot. more stable, but may interfere with other processes.
//		JemmyProperties.setCurrentDispatchingModel(JemmyProperties.ROBOT_MODEL_MASK);
		JemmyProperties.setCurrentOutput(new TestOut(System.in, null, System.err, null));
		XPEFrame.main(new String[]{});
	}

	private void init()
	{
		if (xpeFrame == null)
		{
			xpeFrame = new JFrameOperator("XPath Explorer");
			urlEntryField = new JTextFieldOperator(xpeFrame, 0);
			xpathEntryField = new JTextFieldOperator(xpeFrame, 1);
		}
	}
	public void testInitialAppearance() throws Exception
	{
		init();
		assertTrue("default url", urlEntryField.getText().endsWith("xml/id.xml"));
		assertTrue("default protocol", urlEntryField.getText().startsWith("file:"));
		assertEquals("default xpath", "//", xpathEntryField.getText());
	}

	public void stopApp() throws Exception
	{
		// todo dk: why do we whe have NPE on this?
		// xpeFrame.close();
	}

	// dk: do this in a self-made suite, as we possibly don't want to start/stop the app for every test (performance)
	public static junit.framework.Test suite()
	{
		TestSuite result = new TestSuite();
		result.addTest(new InitialGuiTest("startApp"));
		result.addTest(new InitialGuiTest("testInitialAppearance"));
		result.addTest(new InitialGuiTest("stopApp"));
		return result;
	}

}