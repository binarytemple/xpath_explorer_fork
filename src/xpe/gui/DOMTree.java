
package xpe.gui;

import javax.swing.*;
import javax.swing.tree.*;
import org.w3c.dom.*;
import java.util.*;
import java.util.List;
import javax.swing.tree.DefaultTreeModel;

public class DOMTree extends JTree
{
    Document document;
    Collection matches;
    DOMTreeNode root;
    boolean nodesExpanded = false;
    private CompatibleProgressBar progress;

    public DOMTree()
    {
        super(new DOMTreeNode(null, null, false));
        init();
    }

    public DOMTree(Document document, Collection matches)
    {
        super(new DOMTreeNode(document, matches, false));
        init();
        setDocument(document, matches);
    }

    private void init()
    {
        // remove folder/doc icons
        DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
        renderer.setLeafIcon(null);
        renderer.setOpenIcon(null);
        renderer.setClosedIcon(null);
        this.setCellRenderer(renderer);
        // remove root node
        this.setRootVisible(false);
        // make sure +/- icons are still there
        this.setShowsRootHandles(true);
        // double-clicking should not expand/contract
        this.setToggleClickCount(0);
        // don't bother scrolling
        this.setScrollsOnExpand(false);
    }

    synchronized public void expandAll() {
        if (progress != null) {
            progress.setString("Scanning nodes...");
            progress.setIndeterminate(true);
        }
        expandAllBeneath(root);
    }

    synchronized private void expandAllBeneath(DOMTreeNode baseNode) {
        Set nodes = baseNode.getAllLeafNodes();
        if (progress != null) {
            progress.setString("Expanding nodes...");
            progress.setIndeterminate(false);
            progress.setMinimum(0);
            progress.setMaximum(nodes.size());
        }
        int i=0;
        for (Iterator iter = nodes.iterator(); iter.hasNext();)
        {
            DOMTreeNode node = (DOMTreeNode)iter.next();
            TreePath path = new TreePath(node.getPath());
            makeVisible(path);
            if (progress != null)
                progress.setValue(++i);
        }
    }

    synchronized public void collapseAll() {
        if (progress != null) {
            progress.setString("Scanning nodes...");
            progress.setIndeterminate(true);
        }
        TreeNode rootNode = getRootNode();
        collapseAllBeneath(rootNode);
    }

    synchronized private void collapseAllBeneath(TreeNode baseNode)
    {
        List nodes = getAllTreePathsBeneath(baseNode);

        if (progress != null) {
            progress.setString("Collapsing nodes...");
            progress.setIndeterminate(false);
            progress.setMinimum(0);
            progress.setMaximum(nodes.size());
        }
        int i=0;
        for (Iterator iterator = nodes.iterator(); iterator.hasNext();) {
            TreePath path = (TreePath) iterator.next();
            if (!isRootPath(path)) {
                collapsePath(path);
            }
            if (progress != null)
                progress.setValue(++i);
        }
    }

    private boolean isRootPath(TreePath path) {
        return new TreePath(getRootNode()).equals(path);
    }

    List getAllTreePaths()
    {
        return getAllTreePathsBeneath(getRootNode());
    }

    private List getAllTreePathsBeneath(TreeNode root) {
        TreePath path = new TreePath(root);
        List paths = new LinkedList();
        paths.add(path);
        addChildPaths(path, paths);
        return paths;
    }

    public TreeNode getRootNode() {
        TreeNode root = (TreeNode)getModel().getRoot();
        return root;
    }

    private void addChildPaths(TreePath path, List paths)
    {
        TreeNode node = (TreeNode)path.getLastPathComponent();
        for (int i=0; i<node.getChildCount(); ++i)
        {
            TreeNode child = node.getChildAt(i);
            TreePath childPath = path.pathByAddingChild(child);
            paths.add(0,childPath);
            addChildPaths(childPath, paths);
        }
    }

    public static Set getAllLeafNodes(TreeNode root) {
        return getAllTreeNodes(root, new HashSet(), true);
    }

    private static Set getAllTreeNodes(TreeNode node, Set set, boolean onlyLeaves) {
        if (!onlyLeaves || node.isLeaf()) {
            set.add(node);
        }
        int i = 0;
        while (i < node.getChildCount()) {
            TreeNode child = node.getChildAt(i);
            getAllTreeNodes(child, set, onlyLeaves);
            ++i;
        }
        return set;
    }

    public void setDocument(Document document, Collection matches)
    {
        if (this.document == document) {
//            System.out.println("same document; setting matches only");
            setMatches(matches);
            return;
        }

        this.document = document;
        this.matches = matches;

        if (progress != null) {
            progress.setString("Building node hierarchy...");
            progress.setIndeterminate(true);
        }
        root = new DOMTreeNode(document, matches, false);

        DefaultTreeModel model = ((DefaultTreeModel)getModel());
        model.setRoot(root);

//        expandAll();
    }

    private void refreshChangedNodes(Collection matchesOld, Collection matchesNew) {
        DefaultTreeModel model = ((DefaultTreeModel)getModel());
        Iterator i = getAllTreePaths().iterator();
        while (i.hasNext()) {
            TreePath path = (TreePath)i.next();
            DOMTreeNode treenode = (DOMTreeNode)path.getLastPathComponent();

            String was = treenode.toString(matchesOld);
            String will = treenode.toString(matchesNew);
            if (!was.equals(will)) {
                model.valueForPathChanged(path, treenode);
            }
        }
    }


    public void setMatches(Collection newMatches)
    {
        Collection oldMatches = this.matches;
        this.matches = newMatches;

        // tell the DOMTreeNode graph to change its idea of what matches (is blue)
        root.setMatches(newMatches);

        // refresh changed nodes
        refreshChangedNodes( oldMatches, newMatches);
    }

    public void selectNode(Node target)
    {
        if (target == null) {
            this.setSelectionRows( new int[] {} );
            return;
        }
        Iterator i = this.getAllTreePaths().iterator();
        while (i.hasNext())
        {
            TreePath path = (TreePath)i.next();
            Node current = ((DOMTreeNode)(path.getLastPathComponent())).getNode();
            if (current == target) {
                this.setSelectionPath(path);
            }
        }
    }

    synchronized public void setProgressBar(CompatibleProgressBar progress) {
       this.progress = progress;
    }

    //todo:test me
    public void expandCurrent() {
        expandAllBeneath(getSelectedNode());
    }

    //todo:test me
    public DOMTreeNode getSelectedNode() {
        TreePath selectionPath = getSelectionPath();
        if (selectionPath == null) return null;
        return (DOMTreeNode)(selectionPath.getLastPathComponent());
    }

    //todo:test me
    public void selectPath(TreePath path) {
        this.setSelectionPath(path);
    }

    //todo:test me
    public void collapseCurrent() {
        System.out.println("collapsing " + getSelectedNode());
        collapseAllBeneath(getSelectedNode());
    }

}
