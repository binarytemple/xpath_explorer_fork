package xpe.gui;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.List;

import org.w3c.dom.*;

import com.purpletech.util.*;
import xpe.gui.XPEPanel;
import xpe.*;

import javax.swing.tree.*;
import javax.swing.text.JTextComponent;

/**
 * TestSuite that runs all the tests
 *
 */
public class XPEPanelTest extends BaseTest {

    public XPEPanelTest(String s)
    {
        super(s);
    }

    XPEPanel panel;


    public void setUp() {
    }

    public void testDefaults() {
        panel = new XPEPanel();
        assertEquals( "", panel.getURL());
        assertEquals( "", panel.getXPath());
        assertEquals(0, panel.listResults.getModel().getSize());
        // todo: test empty contents tree
        // assertEquals(0, panel.treeContents.getModel().getRoot().???
        assertEquals( "", panel.getMessage());
        assertEquals( "", panel.getStringValue());
        assertEquals( "", panel.getExpanded());
        assertEquals( "", panel.getGenerated());
    }

    private String fitSwingHTML(String html) {
        //html = com.purpletech.util.Utils.replace( html, newline, newline + "    " );
        html = com.purpletech.util.Utils.replace( html, "&nbsp;", "&#160;" );
        html = com.purpletech.util.Utils.replace( html, "&gt;<br>" + newline, "&gt;<br>" );
        html = com.purpletech.util.Utils.replace( html, "<br>" + newline, "<br>" );  // sometimes it does, sometimes it doesn't!
        html = com.purpletech.util.Utils.replace( html, " class='match'", "" );
        return swingTop + html + swingBottom;
    }

    private void assertEqualsIgnoreWhitespace(String expected, String actual)
    {
        expected = Utils.replace(expected, newline, "");
        expected = Utils.replace(expected, " ", "");

        actual = Utils.replace(actual, newline, "");
        actual = Utils.replace(actual, " ", "");

        assertEquals(expected, actual);
    }

    String removeClassAttribute(String string) {
        // i don't know why she swallowed the fly
        // for some reason, Swing strips class, but only sometimes
        string = Utils.replace(string, "class=\"match\"", "");
        string = Utils.replace(string, "class='match'", "");
        return(string);
    }

    public void testResultsList() throws Exception
    {
        String url = urlForXmlFile("basic.xml");
        String xpath = "//cheese";
        panel = new XPEPanel(url, xpath);

        String htmlCheese = "<html><font face='sans'>&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + "/&gt;</font></html>";
        assertEquals(htmlCheese,
                     panel.listResults.getModel().getElementAt(0).toString());
        assertEquals(htmlCheese,
                     panel.listResults.getModel().getElementAt(1).toString());

        ThreadedAction action = (ThreadedAction)panel.actions.evaluate;
        int actionCount = action.getCount();
        panel.setXPath("//bar");
        action.waitUntilFinished(actionCount);

        String htmlBar = "<html><font face='sans'>&lt;" + htmlMatchStart + "bar" + htmlMatchEnd + "&gt;</font></html>";
        assertEquals(htmlBar,
                     panel.listResults.getModel().getElementAt(0).toString());
        assertEquals(1, panel.listResults.getModel().getSize());
    }

    public void testDefaultsUsingId() throws Exception
    {
        String url = urlForXmlFile("id.xml");
        String xpath = "//cheese";
        panel = new XPEPanel(url, xpath);

        assertEquals( url, panel.getURL() );
        assertEquals( xpath, panel.getXPath() );
        assertEquals( panel.xpe.getDocument(), panel.treeContents.document );

        assertEquals
            ("getStringValue",
             "gouda",
             panel.getStringValue());

        assertEquals
            ("getNumberValue",
             "NaN",
             panel.getNumberValue());

        assertEquals("booleanValue", "true", panel.getBooleanValue());

        assertEquals("Expanded XPath", "/descendant-or-self::node()/child::cheese", panel.getExpanded());

    }

    public void testError() {
        String url = "bad url";
        String xpath = "//ok";
        panel = new XPEPanel(url, xpath);

        String err1 = "java.lang.RuntimeException: No protocol specified in URL 'bad url'";

        String err2 = "java.net.MalformedURLException: no protocol: bad url";

        String message = panel.getMessage();

        if (!( message.startsWith(err1) || message.startsWith(err2) ))
        {
            assertEquals(err2, message);
        }

    }

    /*
      public void testFunctionCallException()
      {
      String url = urlForXmlFile("id.xml");
      String xpath = "id()='foobar'";
      panel = new XPEPanel(url, xpath);
      String err = "org.jaxen.FunctionCallException: id() requires one argument";
      assertTrue(stripWhitespace(panel.getResults()).
      indexOf(stripWhitespace(err)) != -1);

      }
    */

    public void testChangeXPath() throws Exception
    {
        String url = urlForXmlFile("id.xml");
        String xpath = "//cheese";
        panel = new XPEPanel(url, xpath);

        assertEquals
            ("getStringValue",
             "gouda",
             panel.getStringValue());

        assertEquals( panel.xpe.getDocument(), panel.treeContents.document );

        assertEquals( "/descendant-or-self::node()/child::cheese", panel.getExpanded()) ;

        panel.setXPath("//cheese/@kind");
        panel.update();

        assertEquals
            ("getStringValue",
             "edam",
             panel.getStringValue());

        assertEquals( panel.xpe.getDocument(), panel.treeContents.document );

        assertEquals( "/descendant-or-self::node()/child::cheese/attribute::kind", panel.getExpanded()) ;
    }

    // doesn't work
    /*
      public void testScrollPosition() throws Exception {
      XPEFrame frame = new XPEFrame(urlForXmlFile("id.xml"), "//fibonacci");
      frame.setSize(400,420);
      frame.setVisible(true);

      frame.panel.setURL(urlForXmlFile("fibo.xml"));
      frame.panel.update();

      assertEquals(0, frame.panel.
      scrollContents.
      getVerticalScrollBar().getValue());
      assertEquals(0, frame.panel.scrollResults.getVerticalScrollBar().getValue());

      frame.dispose();
      }
    */

    public void testEvaluateDoesntReloadDocument() throws Exception
    {
        String url = urlForXmlFile("id.xml");
        String xpath = "//cheese";
        panel = new XPEPanel(url, xpath);
        panel.update();
        Document doc1 = panel.xpe.getDocument();
        panel.setXPath("//cheese/@kind");
        panel.update();
        Document doc2 = panel.xpe.getDocument();
        assertTrue( doc1 == doc2 );
        panel.setURL(urlForXmlFile("basic.xml"));
        panel.update();
        Document doc3 = panel.xpe.getDocument();
        assertTrue( doc3 != doc2 );
    }

    public void testLoad() throws Exception
    {
        String url = urlForXmlFile("id.xml");
        String xpath = "//cheese";
        panel = new XPEPanel(url, xpath);
        Document doc1 = panel.xpe.getDocument();
        panel.load();
        Document doc2 = panel.xpe.getDocument();
        assertTrue( doc1 != doc2 );
    }

    private void assertNoDuplicates(List history) {
        Set dups = new HashSet();
        for (Iterator i = history.iterator(); i.hasNext(); ) {
            String item = (String)i.next();
            if (dups.contains(item)) {
                fail("Duplicate history item " + item);
            }
            dups.add(item);
        }
    }

    public void testURLHistory() throws Exception
    {
        String url = urlForXmlFile("id.xml");
        String url2 = urlForXmlFile("basic.xml");
        String xpath = "//cheese";
        panel = new XPEPanel(url, xpath);
        panel.setURL(url2);
        assertTrue( panel.getURLHistory().contains(url2) );
        assertTrue( panel.getURLHistory().contains(url) );
        // test duplicates
        panel.setURL(url);
        List history = panel.getURLHistory();

        assertNoDuplicates(history);
    }

    private void assertListImmutable(List list)
    {
        try {
            list.add("Foo");
            fail("Should not be able to add to URL history list");
        }
        catch (Exception e) {
            // yes, it should throw an exception
        }
    }

    public void testURLHistoryImmutable() throws Exception {
        panel = new XPEPanel();
        List h = panel.getURLHistory();
        assertListImmutable(h);
    }


    public void testXPathHistory() throws Exception
    {
        String xpath = "//";
        String xpath2 = "//foo";
        String url = urlForXmlFile("basic.xml");
        panel = new XPEPanel(url, xpath);
        panel.setXPath(xpath2);
        assertTrue( panel.getXPathHistory().contains(xpath2) );
        assertTrue( panel.getXPathHistory().contains(xpath) );
        // test duplicates
        panel.setXPath(xpath);
        List history = panel.getXPathHistory();
        assertNoDuplicates(history);
    }

    public void testXPathHistoryImmutable() throws Exception
    {
        panel = new XPEPanel();
        assertListImmutable(panel.getXPathHistory());
    }

    public void testScrollPanesContainCorrectComponents() {
        panel = new XPEPanel();
        assertTrue( panel.scrollResults.getViewport().getView() == panel.listResults );
        assertTrue( panel.scrollContents.getViewport().getView() == panel.treeContents );
        panel.setURL(urlForXmlFile("id.xml"));
        panel.update();
        assertTrue( panel.scrollResults.getViewport().getView() == panel.listResults );
        assertTrue( panel.scrollContents.getViewport().getView() == panel.treeContents );
    }

    public void testStinky() {
        panel = new XPEPanel(urlForXmlFile("www.stinky.com.html"), "//");
        assertTrue(panel.getMessage().indexOf("NullPointerException") == -1);
    }

    public void testGenerated() throws InterruptedException {
        panel = new XPEPanel(urlForXmlFile("id.xml"), "//");
        panel.treeContents.setSelectionRow(3);

        String gpath = "//bar[@id='fb1']/cheese[1]";
        assertEquals(gpath, panel.getGenerated());

        panel.useGenerated();
        assertEquals(gpath, panel.getXPath());
    }

    public void testSynchronizeResultsAndContents()
    {
        panel = new XPEPanel(urlForXmlFile("id.xml"), "//");

        // test tree -> results
        panel.treeContents.setSelectionRow(0);  // foo element
        assertEquals("changing selection in contents tree should change results list",
                     "foo", panel.getSelectedNodeFromResults().getNodeName());

        // test results -> tree
        panel.listResults.setSelectedIndex(3); // bar element
        assertEquals("changing selection in results list should change contents tree",
                     "bar", panel.getSelectedNodeFromContentsTree().getNodeName());

        // select tree, but not in results -> no selection in results
        Node bar = (Node)(panel.xpe.getMatches().get(3));
        int actionCount = panel.actions.evaluate.getCount();
        panel.setXPath("//cheese");
        panel.actions.evaluate.waitUntilFinished(actionCount);
        panel.listResults.setSelectedIndex(0); // first cheese
        panel.selectNodeInContentsTree(bar);
        assertNull("setting tree to node absent from list should deselect list",
                   panel.getSelectedNodeFromResults());

    }

    public void testSelectNode()
    {
        panel = new XPEPanel(urlForXmlFile("id.xml"), "//");

        Node bar = (Node)(panel.xpe.getMatches().get(3));
        assertEquals("bar", bar.getNodeName());

        panel.selectNode(bar);
        assertEquals(bar, panel.getSelectedNodeFromContentsTree());
        assertEquals(bar, panel.getSelectedNodeFromResults());

        panel.selectNode(null);
        assertNull( "contents selection should be null", panel.getSelectedNodeFromContentsTree());
        assertNull( "results selection should be null", panel.getSelectedNodeFromResults());

    }

    public void testJavaEscape()
    {
        panel = new XPEPanel(urlForXmlFile("fibo.xml"), "/Fibonacci_Numbers");
        String expected = "\\n  0\\n  1\\n  1\\n  2\\n  3\\n  5\\n  8\\n  13\\n  21\\n  34\\n  55\\n  89\\n  144\\n  233\\n  377\\n  610\\n  987\\n  1597\\n  2584\\n  4181\\n  6765\\n  10946\\n  17711\\n  28657\\n  46368\\n  75025\\n";
//        if (System.getProperty("line.separator").equals("\r\n"))
//        {
//            expected = Utils.replace(expected, "\\n", "\\r\\n");
//        }
        assertEquals(expected, panel.getStringValue());
    }

    public void testToFileUrl() throws IOException {
        panel = new XPEPanel(urlForXmlFile("fibo.xml"), "/Fibonacci_Numbers");
        File file = new File(filePathForXmlFile("id.xml"));
        String urlString = panel.toFileUrl(file);
        assertTrue(urlString.startsWith("file:"));
        URL url = new URL(urlString);
        Object content = IOUtils.readStream((InputStream)url.getContent());
        String idContent = IOUtils.readFile(file);
        assertEquals(idContent, content);
    }

    public void testReallyLargeFileDoesntExpandNodes() throws Exception
    {
        String filename = "build/reallybig.xml";
        PrintWriter out = new FilePrintWriter(filename);
        buildALot(out, 5, 0);
        out.close();

        panel = new XPEPanel("file:" + filename, "/");

        assertNoTopLevelChildExpanded();
    }

    private void assertNoTopLevelChildExpanded() {
        TreeNode root = panel.treeContents.getRootNode();
        for (int i=0; i<root.getChildCount(); ++i) {
            TreeNode child = root.getChildAt(i);
            TreePath path = new TreePath(new Object[] { root, child });
            boolean expanded = panel.treeContents.isExpanded(path);
            assertFalse("Path " + path + " should not be expanded", expanded);
        }
    }

    // this takes 16 seconds; remove the "DONT" to add it to the test suite anyway
    public void DONTtestReallyReallyLargeFile() throws Exception
    {
        String filename = "build/reallyreallybig.xml";
        PrintWriter out = new FilePrintWriter(filename);
        buildALot(out, 8, 0);
        out.close();
        panel = new XPEPanel("file:" + filename, "/");
        assertEquals("Skipping node expansion (size 178883)", panel.getMessage());
    }

    private int buildALot(PrintWriter out, int depth, int total) {
        if (depth == 0) {
            out.print(total++);
            return total;
        }

        out.println("<depth" + depth + ">");
        for (int i = 0; i < depth; ++i) {
            total += buildALot(out, depth - 1, total);
        }
        out.println("</depth" + depth + ">");
        return total;
    }

    public void testContentsTreeVisible() {
        panel = new XPEPanel(urlForXmlFile("id.xml"), "//");
        assertEquals(panel.treeContents, panel.scrollContents.getViewport().getView());
    }

    public void testOpenLocation() {
        String url = urlForXmlFile("id.xml");
        panel = new XPEPanel(url, "//");
        panel.openLocation();

        JTextComponent editorComponent = panel.getComboUrlTextComponent();
        assertEquals(url, editorComponent.getSelectedText());
        //        assertTrue(panel.getComboUrlTextComponent().hasFocus());
    }

    public void testExpandAll() {
        String url = urlForXmlFile("id.xml");
        panel = new XPEPanel(url, "//");
        panel.showMessage("foo");
        panel.expandAll();
        assertEquals(panel.scrollContents, panel.tab.getSelectedComponent());
    }

    public void testSource() throws Exception
    {
        panel = new XPEPanel(urlForXmlFile("id.xml"), "/");
        String sourceId = IOUtils.readFile(new File(filePathForXmlFile("id.xml")));
        assertEquals(sourceId, panel.textSource.getText());

        panel.setURL(urlForXmlFile("basic.xml"));
        panel.update();
        String sourceBasic = IOUtils.readFile(new File(filePathForXmlFile("basic.xml")));
        assertEquals(sourceBasic, panel.textSource.getText());
    }
}
