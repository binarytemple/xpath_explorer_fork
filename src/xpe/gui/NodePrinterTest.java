package xpe.gui;

import java.util.*;

import org.w3c.dom.*;

import xpe.gui.NodePrinter;
import xpe.*;

public class NodePrinterTest extends BaseTest
{

    private String getText(Document doc) throws Exception
    {
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter np = new NodePrinter(out, new ArrayList());
        np.printFullNode(doc.getDocumentElement());
        String text = out.getString();
        return text;
    }

    private String getHTML(Document doc) throws Exception
    {
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter np = new NodePrinter(out, new ArrayList(), true, false);
        np.printFullNode(doc.getDocumentElement());
        String text = out.getString();
        return text;
    }

    private String getHTML(Document doc, List results) throws Exception
    {
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter np = new NodePrinter(out, results, true, false);
        np.printFullNode(doc.getDocumentElement());
        String text = out.getString();
        return text;
    }

    public void testText() throws Exception
    {
        Document doc = getDocument(urlForXmlFile("id.xml"));
        String text = getText(doc);
        assertEquals(textId, text);

    }

    /*
      This test fails for different parsers -- it needs to be rewritten to not care about attribute order
    Expected: <JavaXML:Book ora:category='Java' xmlns:JavaXML='http://w...
    Actual: <JavaXML:Book xmlns:JavaXML='http://www.oreilly.com/catal...

    public void testNamespaces() throws Exception {

        Document doc = getDocument("file:xml/contents.xml");
        String text = getText(doc);
        assertEquals(xmlContents, text);
    }
    */

    public void testBasicHTML() throws Exception
    {
        Document doc = getDocument(urlForXmlFile("id.xml"));
        String text = getHTML(doc);
        assertEquals(htmlId, text);
    }

    public void testMatchingHTML() throws Exception
    {
        Document doc = getDocument(urlForXmlFile("id.xml"));
        Node foo = doc.getDocumentElement();
        List results = new ArrayList();
        results.add(foo);
        String text = getHTML(doc, results);
        assertEquals(htmlIdMatching, text);
    }

    public void testHTMLWithSpecialCharacters() throws Exception
    {
        Document doc = getDocument(urlForXmlFile("funky.html"));
        String text = getHTML(doc);
        assertEquals(htmlFunky, text);
    }

    public void testExpandAttributes() throws Exception
    {
        Document doc = getDocument(urlForXmlFile("attributes.xml"));
        Node root = doc.getDocumentElement();

        checkNode(root, "<foo");

        checkAttribute(root, "name", "name='macaroni'");
        checkAttribute(root, "value", "value='cheese'");

        NodeList children = root.getChildNodes();
        Map elements = new HashMap();
        for (int i = 0; i < children.getLength(); ++i)
        {
            if (children.item(i).getNodeType() == Node.ELEMENT_NODE)
            {
                elements.put(children.item(i).getNodeName(), children.item(i));
            }
        }

        Node child = (Node) elements.get("bar");
        checkNode(child, "<bar/>");

        child = (Node) elements.get("baz");
        checkNode(child, "<baz");
        checkAttribute(child, "name", "name='phone'");

        child = (Node) elements.get("baf");
        checkNode(child, "<baf");
        // attributes are unordered
        checkAttribute(child, "hero", "hero='dragon'");
        checkAttribute(child, "type", "type='savage'");
    }

    private void checkNode(Node node, String text)
    {
//        System.out.println("checking node " + node);
        List list = new ArrayList();
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter printer = new NodePrinter(out, list, false, true);
        printer.printSingleNode(node, 0);
        assertEquals(text, out.getString());
    }

    private void checkAttribute(Node root, String attributeName, String text)
    {
        StringPrintWriter out;
        List list;
        NodePrinter printer;
        out = new StringPrintWriter();
        list = new ArrayList();
        printer = new NodePrinter(out, list, false, true);
        Node attr = root.getAttributes().getNamedItem(attributeName);
        printer.printSingleNode(attr, 0);
        assertEquals(text, out.getString());
    }
}
