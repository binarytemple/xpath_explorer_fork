package xpe.gui;

import com.purpletech.util.Utils;
import org.w3c.dom.*;
import xpe.BaseTest;

import javax.swing.tree.*;
import java.util.*;

public class DOMTreeTest extends BaseTest
{
    DOMTree tree;
    Document document;

    public void setUp() throws Exception
    {
        String url = urlForXmlFile("id.xml");
        document = getDocument(url);
    }

    public void testRoot() throws Exception
    {
        DOMTreeNode root = new DOMTreeNode(document, null, false);
        assertEquals(document, root.node);
    }

    private void assertEqualsHTML(String expected, String actual)
    {
        assertEquals(
            DOMTreeNode.HTML_START
                + Utils.htmlescape(expected)
                + DOMTreeNode.HTML_END,
            actual);
    }

    public void testStructure() throws Exception
    {
        DOMTreeNode root = new DOMTreeNode(document, null, false);
        assertEquals(document, root.node);

        DOMTreeNode foo = (DOMTreeNode) (root.getChildAt(0));
        assertEquals("foo", foo.node.getNodeName());
        assertEqualsHTML("<foo id='foobar'>", foo.toString());

        DOMTreeNode bar = (DOMTreeNode) (foo.getChildAt(0));
        assertEquals("bar", bar.node.getNodeName());
        assertEqualsHTML("<bar id='fb1'>", bar.toString());

        DOMTreeNode baz = (DOMTreeNode) (bar.getChildAt(0));
        assertEqualsHTML("baz", baz.toString());

        DOMTreeNode cheese = (DOMTreeNode) (bar.getChildAt(1));
        assertEquals("cheese", cheese.node.getNodeName());

        DOMTreeNode gouda = (DOMTreeNode) (cheese.getChildAt(0));
        assertEquals("gouda", gouda.node.getNodeValue());

        DOMTreeNode cheeseClose = (DOMTreeNode) (bar.getChildAt(2));
        assertEquals(cheese.node, cheeseClose.node);
        assertEqualsHTML("</cheese>", cheeseClose.toString());

    }

    /*
    // test turning a root DOMTreeNode (and all children) into an HTML string
    public void testToHTML() throws Exception
    {
    Document document = XPE.loadDOM(urlForXmlFile("id.xml"));
    DOMTreeNode root = new DOMTreeNode(document, null);
    String html = DOMTree.toHTML(root);
    assertEquals(stripWhitespace(swingTop + htmlId + swingBottom), stripWhitespace(html));
    }

    public void testHTMLSelected() throws Exception
    {
    Document document = XPE.loadDOM(urlForXmlFile("id.xml"));
    Node foo = document.getDocumentElement();
    List results = Arrays.asList( new Object[] { foo } );
    DOMTreeNode root = new DOMTreeNode(document, results);
    String html = DOMTree.toHTML(root);
    assertEquals(stripWhitespace(swingTop + htmlIdFoo + swingBottom),
    stripWhitespace(html));

    // next, test attribute selection?
    }
    */

    public void testExpandAll() throws Exception
    {
        Document document;
        DOMTree tree;

        document = getDocument(urlForXmlFile("id.xml"));
        tree = new DOMTree(document, null);
        tree.expandAll();
        assertAllVisible(tree);

        document = getDocument(urlForXmlFile("basic.xml"));
        tree.setDocument(document, null);
        tree.expandAll();
        assertAllVisible(tree);
    }

    public void testCollapseAll() throws Exception {
        document = getDocument(urlForXmlFile("id.xml"));
        DOMTree tree = new DOMTree(document, null);
        tree.expandAll();
        tree.collapseAll();

        List allPaths = tree.getAllTreePaths();
        assertCollapsed(tree, allPaths);
    }

//    public void testCollapseSelected() throws Exception {
//        document = getDocument(urlForXmlFile("id.xml"));
//        DOMTree tree = new DOMTree(document, null);
//        tree.expandAll();
//
//        TreeNode node = getTopTreeNode(tree).getChildAt(0);
//
//        tree.collapseCurrent();
//        assertCollapsed(tree, null);
//    }

    private void assertCollapsed(DOMTree tree, List collapsedPaths)
    {
        TreePath rootPath = new TreePath(tree.getRootNode());
        for (Iterator iterator = collapsedPaths.iterator(); iterator.hasNext();) {
            TreePath path = (TreePath) iterator.next();
            if (path.equals(rootPath))
                assertTrue("node should be expanded: " + path, tree.isExpanded(path));
            else
                assertFalse("node should not be expanded: " + path, tree.isExpanded(path));
        }
    }

    private void assertAllVisible(DOMTree tree)
    {

        java.awt.Toolkit.getDefaultToolkit(); // start AWT thread

        for (Iterator i = tree.getAllTreePaths().iterator(); i.hasNext();)
        {
            TreePath path = (TreePath) i.next();
            assertTrue(tree.isVisible(path));
        }
    }

    TreeNode getTopTreeNode(DOMTree tree)
    {
        TreeNode treenode = (TreeNode) tree.getModel().getRoot();
        treenode = treenode.getChildAt(0);
        return (treenode);
    }

    public void testSetMatches() throws Exception
    {
        document = getDocument(urlForXmlFile("id.xml"));
        tree = new DOMTree(document, null);

        TreeNode treenode;
        treenode = getTopTreeNode(tree);
        assertEquals(
            "top element should not be bold",
            "<html><font face='sans'>&lt;foo id='foobar'&gt;</font></html>",
            treenode.toString());

        Node root = document.getDocumentElement();
        List matches = new ArrayList();
        matches.add(root);
        tree.setMatches(matches);

        // how to test?
        treenode = getTopTreeNode(tree);
        assertEquals(
            "top element should be bold",
            "<html><font face='sans'>&lt;"
                + htmlMatchStart
                + "foo"
                + htmlMatchEnd
                + " id='foobar'&gt;</font></html>",
            treenode.toString());

    }

//  this passes, but i remove it so it doesn't stall the test run

    public void DONTtestOpenLargeFileQuickly() throws Exception
    {
        document = getDocument(urlForXmlFile("moreover.xml"));
        long start = System.currentTimeMillis();
        tree = new DOMTree(document, null);
        tree.expandAll();
        long finish = System.currentTimeMillis();
        long msec = finish - start;
        long secsExpected = 15;
        assertTrue(
            "expanding all nodes should take less than "
                + secsExpected
                + " seconds, "
                + "but took "
                + msec
                + " msec",
            msec < secsExpected * 1000);
    }


/*
    public void testAttributes() throws Exception
    {
        String filename = "build/test/attributes.xml";
        String[] nodes =
            {
                "<foo",
                "name='macaroni'",
                "value='cheese'>",
                "<bar/>",
                "<baz ",
                "name='phone'/>",
                "<baf",
                "hero='dragon'",
                "type='savage'/>",
                "<bar>",
                "<baz",
                "name='phone'>",
                "<baf",
                "hero='dragon'",
                "type='savage'>",
                "<cheese/>",
                "</baf>",
                "</baz>",
                "</bar>",
                "</foo>",
                };
        StringBuffer doc = new StringBuffer();
        for (int i = 0; i < nodes.length; ++i)
        {
            doc.append(nodes[i]);
            doc.append(" ");
        }
        File file = new File(filename);
        file.getParentFile().mkdirs();
        IOUtils.writeString(file, doc.toString());
        String string = "file:" + filename;

        document = getDocument(string);
        DOMTreeNode root = new DOMTreeNode(document, null, true);

        for (int i = 0; i < nodes.length; i++)
        {
            DOMTreeNode treenode = (DOMTreeNode) (root.getChildAt(i));
            if (nodes[i].startsWith("<"))
            {
                assertTrue(treenode.getNode() instanceof Element);
            }
            else
            {
                assertTrue(treenode.getNode() instanceof Attr);
            }
            assertEquals(
                DOMTreeNode.HTML_START
                    + Utils.htmlescape(nodes[i])
                    + DOMTreeNode.HTML_END,
                treenode.toString());
        }
    }
    */


}
