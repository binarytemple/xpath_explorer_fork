package xpe.gui;

import xpe.*;

import javax.swing.*;
import javax.swing.text.*;
import javax.swing.tree.TreePath;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.event.TreeSelectionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.*;
import java.net.URL;

class XPEPanelInit
{
    private XPEPanel xpepanel;

    public XPEPanelInit(XPEPanel panel)
    {
        this.xpepanel = panel;
    }

    public void init(String url, String xpath)
    {
        initTextComponents(url, xpath);
        initActions();
        addActionListeners();
        addMouseListeners();
        initButtons();
        makeScrollPanes();
        layoutPanels();
    }

    private void initTextComponents(String url, String xpath)
    {
        xpepanel.comboURL = makeComboBox(url);
        xpepanel.comboXPath = makeComboBox(xpath);
        xpepanel.textSource = makeJTextArea();
        xpepanel.textMessage = makeJEditorPane();
        xpepanel.listResults = makeResultsList();
        xpepanel.treeContents = makeContentsTree();
        xpepanel.textStringValue = makeStaticTextField();
        xpepanel.textStringValue.setToolTipText("The results of xpath.valueOf, escaped using Java string conventions");
        xpepanel.textNumberValue = makeStaticTextField();
        xpepanel.textBooleanValue = makeStaticTextField();
        xpepanel.textExpanded = makeStaticTextField();
        xpepanel.textGenerated = makeStaticTextField();
    }

    private void initActions()
    {
        xpepanel.actions = new Actions(xpepanel);
    }

    private void addActionListeners()
    {
        xpepanel.comboURL.addActionListener(xpepanel.actions.evaluate);
        xpepanel.comboXPath.addActionListener(xpepanel.actions.evaluate);
    }

    private void addMouseListeners()
    {
        // double-click doesn't send action for JTree; must use
        // MouseListener instead
        MouseListener ml = new MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    xpepanel.useGenerated();
                }
            }
        };
        xpepanel.treeContents.addMouseListener(ml);
        xpepanel.listResults.addMouseListener(ml);

        MouseListener popup = new MouseAdapter()
        {
            public void mousePressed(MouseEvent e) {
                fnord(e);
            }

            public void mouseReleased(MouseEvent e) {
                fnord(e);
            }

            /**
             * some L&Fs popup on press; others on release. So we have to
             * unify them in this awkward way.
             */
            private void fnord(MouseEvent e) {
                if (e.isPopupTrigger()) {
                    Point point = e.getPoint();
                    TreePath clickedPath = xpepanel.treeContents.getPathForLocation(point.x, point.y);
                    xpepanel.treeContents.selectPath(clickedPath);
                    xpepanel.popupMenuAt(point);
                }
            }
        };
        xpepanel.treeContents.addMouseListener(popup);
    }

    private void initButtons()
    {
        xpepanel.buttonLoad = new JButton(xpepanel.actions.load);
        xpepanel.buttonEvaluate = new JButton(xpepanel.actions.evaluate);
        xpepanel.buttonUse = new JButton(xpepanel.actions.use);
        xpepanel.buttonUse.setToolTipText
                ("Use the generated XPath as the target XPath and re-run the search. " +
                "(This will happen automatically when you double-click a node " +
                "in the 'All Nodes' display.");
    }

    public static ImageIcon loadIcon(String filename)
    {
        URL url;
        ImageIcon icon;
        url = XPEPanelInit.class.getResource("/icons/" + filename);
        if (url == null){
        	System.err.println("Could not load resource /icons/"+filename);
        	icon = new ImageIcon("");
        } else {
			icon = new ImageIcon(url);
        }
        return icon;
    }

    public static void unifyPreferredWidths(JComponent[] components)
    {
        int width = 0;
        for (int i=0; i<components.length; ++i)
        {
            if (width < components[i].getPreferredSize().getWidth())
            {
                width = (int)components[i].getPreferredSize().getWidth();
            }
        }
        for (int i=0; i<components.length; ++i)
        {
            Dimension preferred = components[i].getPreferredSize();
            preferred.width = width;
            components[i].setPreferredSize(preferred);
        }
    }

    private void layoutPanels()
    {
        JPanel panelTop = makeTopPanel();
        JPanel panelContents = makeContentsPanel();
        JPanel panelGenerated = makeGeneratedPanel();

        xpepanel.setLayout(new BorderLayout(0,4));
        xpepanel.add(BorderLayout.NORTH, panelTop);
        xpepanel.add(BorderLayout.CENTER, panelContents);
        xpepanel.add(BorderLayout.SOUTH, panelGenerated);
    }

    private void addRow(GridBagHelper helper, String label, Component component, Component button)
    {
        helper.addLabel(label);
        helper.add(component);
        helper.add(button);
        helper.nextRow();
    }

    private JPanel makeTopPanel()
    {
        JPanel panel = new JPanel(new BorderLayout(4,4));
        panel.add(BorderLayout.NORTH, makeEntryAndLogoPanel());
        panel.add(BorderLayout.CENTER, makeValuePanel());
        return panel;
    }

    static Color bg = new Color(240,240,240);

    private JPanel makeEntryAndLogoPanel()
    {
        JPanel panel = new JPanel(new BorderLayout(4,4));
        panel.add(BorderLayout.CENTER, makeEntryPanel());
        JLabel logo = new JLabel(loadIcon("x.gif"));
        panel.add(BorderLayout.WEST, logo);
        panel.setBorder(BorderFactory.createEtchedBorder()) ;
        panel.setBackground(bg);
        return panel;
    }

    private JPanel makeEntryPanel()
    {
        final double[] weights = { 0.0, 0.8, 0.1 };

        unifyPreferredWidths(new JButton[] { xpepanel.buttonLoad, xpepanel.buttonEvaluate });
        JPanel panel = new JPanel();
        GridBagHelper helper = new GridBagHelper(panel, weights);
        addRow(helper, "URL:", xpepanel.comboURL, panelize(xpepanel.buttonLoad, bg));
        addRow(helper, "XPath:", xpepanel.comboXPath, panelize(xpepanel.buttonEvaluate, bg));
        panel.setBackground(bg);
        return panel;
    }

    private JPanel makeValuePanel()
    {
        final double[] weights = { 0.0, 0.8, 0.0, 0.8 };

        JPanel panel = new JPanel();
        panel.setBackground(new Color(203,220,237));
        GridBagHelper helper = new GridBagHelper(panel, weights);

        helper.addLabel("Expanded:");
        helper.add(xpepanel.textExpanded, 3);
        helper.nextRow();

        helper.addLabel("String Value:");
        helper.add(xpepanel.textStringValue, 3);
        helper.nextRow();

        helper.addLabel("Number Value:");
        helper.add(xpepanel.textNumberValue);
        helper.addLabel("Boolean Value:");
        helper.add(xpepanel.textBooleanValue);
        helper.nextRow();

        panel.setBorder(BorderFactory.createEtchedBorder()) ;

        return panel;
    }

    private JPanel makeGeneratedPanel()
    {
        final double[] weights = { 0.0, 0.8, 0.1 };

        JPanel panel = new JPanel();
        GridBagHelper helper = new GridBagHelper(panel, weights);
        addRow(helper, "Generated:", xpepanel.textGenerated, panelize(xpepanel.buttonUse));
        panel.setBorder(BorderFactory.createEtchedBorder()) ;
        return panel;
    }

    private JList makeResultsList()
    {
        xpepanel.modelResultsList = new DefaultListModel();
        JList list = new JList(xpepanel.modelResultsList);
        list.addListSelectionListener(new ListSelectionListener()
        {
            public void valueChanged(ListSelectionEvent e)
            {
                xpepanel.selectNodeFromResults();
            }
        });
        return list;
    }

    private void makeScrollPanes()
    {
        xpepanel.scrollResults = makeScrollPane(xpepanel.listResults);
        xpepanel.scrollContents = makeScrollPane(xpepanel.treeContents);
        xpepanel.scrollMessage = makeScrollPane(xpepanel.textMessage);
        xpepanel.scrollSource = makeScrollPane(xpepanel.textSource);
    }

    private JPanel makeContentsPanel()
    {
        JPanel panelContents = new JPanel();
        panelContents.setLayout(new BorderLayout());

        xpepanel.tab = new JTabbedPane();
        xpepanel.tab.addTab("Source", xpepanel.scrollSource);
        xpepanel.tab.addTab("All Nodes", xpepanel.scrollContents);
        xpepanel.tab.setSelectedIndex(0);
        xpepanel.tab.addTab("Matching Nodes", xpepanel.scrollResults);
        xpepanel.tab.addTab("Messages", xpepanel.scrollMessage);

        panelContents.add(BorderLayout.CENTER, xpepanel.tab);

        //panelContents.setBorder(BorderFactory.createEtchedBorder()) ;
        return panelContents;
    }

    private DOMTree makeContentsTree()
    {
        DOMTree tree;
        tree = new DOMTree();
        tree.addTreeSelectionListener(new TreeSelectionListener()
        {
            public void valueChanged(TreeSelectionEvent event)
            {
                xpepanel.selectNodeFromContents();
            }
        });
        return tree;
    }

    private JTextComponent setupTextComponent(JTextComponent text)
    {
        text.setEditable(false);
        text.setBackground(Color.white);
        return text;
    }

    private JTextField makeStaticTextField()
    {
        JTextField field = new JTextField();
        setupTextComponent(field);
        return field;
    }

    private JEditorPane makeJEditorPane()
    {
        JEditorPane field = new JEditorPane();
        setupTextComponent(field);
        return field;
    }

    private JTextArea makeJTextArea()
    {
        JTextArea field = new JTextArea();
        setupTextComponent(field);
        return field;
    }

    private JScrollPane makeScrollPane(Component component)
    {
        JScrollPane scroll = new JScrollPane(component);
        scroll.setMinimumSize(new Dimension(300, 120));
        scroll.setPreferredSize(new Dimension(300, 120));
        return scroll;
    }


    /**
     * wraps a component in a panel using FlowLayout, in order to
     * preserve its preferred size if added to a more annoying layout
     * (say, a BorderLayout or a GridBagLayout)
     **/
    public static JPanel panelize(Component c)
    {
        return panelize(c, null);
    }

    /**
     * wraps a component in a panel using FlowLayout, in order to
     * preserve its preferred size if added to a more annoying layout
     * (say, a BorderLayout or a GridBagLayout)
     *
     * @param bg background color of the surrounding panel
     **/
    public static JPanel panelize(Component c, Color bg)
    {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
        panel.add(c);
        if (bg!=null)
            panel.setBackground(bg);
        return panel;
    }

    private static JComboBox makeComboBox(String item)
    {
        JComboBox combo = new JComboBox();
        combo.addItem(item);
        combo.setEditable(true);
        return combo;
    }
}
