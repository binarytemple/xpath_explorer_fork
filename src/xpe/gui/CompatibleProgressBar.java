package xpe.gui;

import javax.swing.*;

/**
 * A JProgressBar that provides an implementation for JDK-1.4 methods.
 * Make sure you compile this one with JDK 1.4
 */
public class CompatibleProgressBar extends JProgressBar
{
    public void setIndeterminate(boolean newValue)
    {
        try {
            //todo: add conditional compile to build.xml
//            super.setIndeterminate(newValue);
        }
        catch (NoSuchMethodError e) {
            // older JDK -- ignore
        }
    }
}
