package xpe;

/**
 * @author Bernhard Wagner <xpe.bw@xmlizer.biz>
 *
 */
public class HttpUnitXPETest extends XPETest {
	public HttpUnitXPETest(String name){
		super(name);
	}
	public void setUp(){
		DocumentLoaderFactory.instance().setDocumentLoaderClassName("xpe.DocumentLoaderImplHttpUnit");
	}
	public void tearDown(){
		DocumentLoaderFactory.instance().setDocumentLoaderClassName("xpe.DocumentLoaderImplPlain");
	}
	
	/**
	 * Namespaces are not fully supported by DocumentLoaderImplHttpUnit.
	 */
	public void testNamespaces() {

	}
	
	public void testUpperCase() {
	}
}
