package xpe;

import java.util.*;
import java.io.*;

import org.w3c.dom.*;
import xpe.gui.NodePrinter;
import com.purpletech.util.*;
import com.purpletech.util.StringPrintWriter;
import java.net.URI;
import java.net.URL;

public class XPETest extends BaseTest {

    public XPETest(String s) {
        super(s);
    }

    public void testBasic() throws Exception {

        String url="file:resources/xml/basic.xml";
        String xpath="//cheese";

        XPE xpe = new XPE(url, xpath);
        String text = xpe.getFullText();

        assertEquals(textBasicCheese,
                     text);

    }

    public void testBasicResults() throws Exception {

        String url="file:resources/xml/basic.xml";
        String xpath="//cheese";

        XPE xpe = new XPE(url, xpath);
        String text = xpe.getMatchesText();

        assertEquals
            ("<*cheese*/>" + newline +
             "<*cheese*/>" + newline +
             "", text);

    }

    public void testBasicResultsHTML() throws Exception {

        String url="file:resources/xml/basic.xml";
        String xpath="//cheese";

        XPE xpe = new XPE(url, xpath);
        String text = xpe.getMatchesHTML();

        assertEquals(resultsBasicCheeseHTML, text);
    }

    public void testSetURL() throws Exception {

        String url=urlForXmlFile("basic.xml");
        String xpath="//cheese";
        XPE xpe = new XPE(url, xpath);
        Document documentOriginal = xpe.getDocument();

        xpe.setDocument(urlForXmlFile("id.xml"));
        Document documentNew = xpe.getDocument();

        assertTrue("documentOriginal != documentNew", documentOriginal != documentNew);

        String text = xpe.getFullText();
        assertEquals
            ("old XPath matches new document",
             textIdCheese,
             text);

        xpe.setDocument(urlForXmlFile("id.xml"));
        assertNotSame("setting to same url reloads document",
                     documentNew, xpe.getDocument());
    }

  public void testSetXPath() throws Exception {
        String url=urlForXmlFile("basic.xml");
        String xpath="//cheese";
        XPE xpe = new XPE(url, xpath);
        xpe.setXPath("//baz");
        String text = xpe.getFullText();
        assertEquals(textBasicBaz, text);

        Document doc = xpe.getDocument();
        xpe.setXPath("//baz");
        assertEquals("setting to same XPath retains document",
                     doc, xpe.getDocument());

    }

    public void testBasic2() throws Exception {

        String url=urlForXmlFile("basic.xml");
        String xpath="//baz";

        XPE xpe = new XPE(url, xpath);
        String text = xpe.getFullText();

        assertEquals
            (textBasicBaz,
             text);
    }

    public void testIdCheese() throws Exception
    {
        String url=urlForXmlFile("id.xml");
        String xpath="//cheese";

        XPE xpe = new XPE(url, xpath);
        String text = xpe.getFullText();

        assertEquals
            (textIdCheese,
             text);

        assertEquals("gouda", xpe.getStringValue());
    }

    public void testStringValueDoesntCorruptFullText()
        throws Exception
    {
        String url=urlForXmlFile("id.xml");
        String xpath="//cheese";

        XPE xpe = new XPE(url, xpath);

        assertEquals("gouda", xpe.getStringValue());

        assertEquals
            ("getStringValue corrupts getFullText",
             textIdCheese,
             xpe.getFullText());

    }

    public void testIdAttributes() throws Exception
    {
        String url=urlForXmlFile("id.xml");
        String xpath="//cheese/@kind";

        XPE xpe = new XPE(url, xpath);
        String text = xpe.getFullText();

        assertEquals(textIdKind, text);
    }

    public void testNumberValue() throws Exception
    {
        String url=urlForXmlFile("fibo.xml");
        String xpath="//fibonacci[@index='12']";
        XPE xpe = new XPE(url, xpath);
        assertEquals(144, xpe.getNumberValue().intValue());
        assertEquals("144.0", xpe.getNumberValue().toString());
    }

    public void testBooleanValue() throws Exception
    {
        String url=urlForXmlFile("id.xml");
        XPE xpe = new XPE(url, "//cheese");
        assertEquals(true, xpe.getBooleanValue());
        xpe = new XPE(url, "//nocheese");
        assertEquals(false, xpe.getBooleanValue());
    }

    public void testBasicHTML() throws Exception {

        String url=urlForXmlFile("basic.xml");
        String xpath="//cheese";

        XPE xpe = new XPE(url, xpath);
        String html = xpe.getFullHTML();

        assertEquals(htmlBasicCheese, html);
    }

    public void testReturnsDouble() throws Exception {
        String url=urlForXmlFile("basic.xml");
        String xpath="count(//cheese)";

        XPE xpe = new XPE(url, xpath);

        assertEquals("2.0<br>" + newline, xpe.getMatchesHTML());
        assertEquals("2.0" + newline, xpe.getMatchesText());
    }

    public void testExpanded() throws Exception
    {
        String url=urlForXmlFile("id.xml");
        String xpath="//cheese";
        XPE xpe = new XPE(url, xpath);
        assertEquals("/descendant-or-self::node()/child::cheese", xpe.getExpandedXPath());
    }

    public void testHTMLSource() throws Exception
    {
        XPE xpe = new XPE(urlForXmlFile("www.stinky.com.html"), "//");
    }

    String getNodeString(Node node) {
        StringPrintWriter namer = new StringPrintWriter();
        NodePrinter printer = new NodePrinter(namer);
        printer.printSingleNode(node, 0);
        String name = namer.getString();
        namer.close();
        return(name);
    }


    void assertGeneratedNodeUnique(XPE xpe, Node node) throws Exception
    {
        // all we're testing here is that the generated path (a)
        // matches the passed-in node, and (b) uniquely matches it

        String name = getNodeString(node);

        String path = XPE.generateXPath(node);

        xpe.setXPath(path);
        Collection resultsGenerated = xpe.getMatches();
        String message = "generated xpath for " + name + ": " + path;
        assertEquals(message,
                     1, resultsGenerated.size());
//        assertEquals(message,
//                     node, resultsGenerated.get(0));
    }

    public void testGenerateRoot() throws Exception
    {
        XPE xpe = new XPE(urlForXmlFile("id.xml"), "/foo");
        List results = xpe.getMatches();
        assertGeneratedNodeUnique( xpe, (Node)(results.get(0)));
    }

    public void testGenerateFirstChild() throws Exception
    {
        XPE xpe = new XPE(urlForXmlFile("id.xml"), "/foo/bar");
        List results = xpe.getMatches();
        assertGeneratedNodeUnique( xpe, (Node)(results.get(0)));
    }

    public void testGenerateBarID() throws Exception
    {
        XPE xpe = new XPE(urlForXmlFile("id.xml"), "/foo/bar");
        List results = xpe.getMatches();
        assertEquals( "//bar[@id='fb1']", XPE.generateXPath( (Node)(results.get(0))));
    }

    public void testGenerateCheese() throws Exception
    {
        XPE xpe = new XPE(urlForXmlFile("id.xml"), "/foo/bar/cheese[1]");
        List results = xpe.getMatches();
        assertGeneratedNodeUnique( xpe, (Node)(results.get(0)));
    }

    public void testGenerateAttribute() throws Exception
    {
        XPE xpe = new XPE(urlForXmlFile("id.xml"), "/foo/bar/cheese[2]/@kind");
        List results = xpe.getMatches();
        assertGeneratedNodeUnique( xpe, (Node)(results.get(0)));
    }

    void assertAllNodes(String url, String xpath) throws Exception
    {
        XPE xpe = new XPE(url, xpath);
        Collection results = xpe.getMatches();
        for (Iterator i=results.iterator();
             i.hasNext(); )
        {
            Object result = i.next();
            if (result instanceof Node) {
                Node node = (Node)result;
                if (node.getNodeType() == Node.ELEMENT_NODE || node.getNodeType() == Node.ATTRIBUTE_NODE)
                {
                    assertGeneratedNodeUnique(xpe, node);
                }
            }
        }

    }

    public void testGenerateAll1() throws Exception
    {
        String url=urlForXmlFile("id.xml");
        String xpath="//";

        assertAllNodes(url, xpath);
    }

    public void testGenerateAll2() throws Exception
    {
        String url=urlForXmlFile("moreover.xml");
        String xpath="//";

        assertAllNodes(url, xpath);
    }

    public void testGenerateAll3() throws Exception
    {
        String url=urlForXmlFile("www.stinky.com.html");
        String xpath="//";

        assertAllNodes(url, xpath);
    }

    public void testToString() throws Exception
    {
        String url = urlForXmlFile("www.stinky.com.html");
        XPE xpe = new XPE(url, "//");
        assertEquals("XPE[url=" + url + ",xpath=//]", xpe.toString());
    }

    public void testEqualsAndHashCode() throws Exception
    {
        XPE xpeA = new XPE(urlForXmlFile("www.stinky.com.html"), "//");
        XPE xpeB = new XPE(urlForXmlFile("www.stinky.com.html"), "//");
        XPE xpeC = new XPE(urlForXmlFile("id.xml"), "//");
        XPE xpeD = new XPE(urlForXmlFile("www.stinky.com.html"), "/foo/bar");


        assertEquals(xpeA, xpeB);
        assertEquals(xpeA.hashCode(), xpeB.hashCode());
        assertEquals(xpeB, xpeA);
        assertFalse(xpeA.equals(xpeC));
        assertFalse(xpeA.equals(xpeC));
    }

    public void testNamespaces() throws Exception
    {
        XPE xpe;
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/b");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/b/c");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/foo:d");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/foo:d/foo:e");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/bar:f");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/bar:f/bar:g");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/alias:x");
        assertEquals(1, xpe.getMatches().size());
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/foo:a/alias:x/alias:y");
        assertEquals(1, xpe.getMatches().size());
    }

    public void testGetNamespaces() throws Exception
    {
        XPE xpe;
        xpe = new XPE(urlForXmlFile("namespaces.xml"), "/");
        Set namespaces = xpe.getNamespaces();
        assertEquals(3, namespaces.size());
        assertTrue(namespaces.contains(new XPE.Namespace("foo", "fooNamespace")));
        assertTrue(namespaces.contains(new XPE.Namespace("bar", "barNamespace")));
        assertTrue(namespaces.contains(new XPE.Namespace("alias", "fooNamespace")));
    }

    // attempt to solve bug at
    // https://sourceforge.net/tracker/?func=detail&atid=474579&aid=655886&group_id=54719
    // see also remote test inside XPERemoteTest
    public void testDocBookTrivial() throws Exception
    {
        new XPE(urlForXmlFile("docbook-trivial.xml"), "/");
    }

    public void testDocBook1() throws Exception
    {
        // this should work when disconnected from the net
        new XPE(urlForXmlFile("docbook1.xml"), "/");
    }


    public void testSource() throws Exception
    {
        String s = urlForXmlFile("id.xml");
        URL url = new URL(s);
        XPE xpe = new XPE(s, "/");
        String text = IOUtils.readFile(new File(filePathForXmlFile("id.xml")));
        assertEquals(text, xpe.getSource());
    }

}
