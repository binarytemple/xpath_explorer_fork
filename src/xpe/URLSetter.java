package xpe;

import xpe.gui.XPEPanel;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;
import java.util.*;
import java.net.*;

public class URLSetter extends AbstractAction
{
    private XPEPanel panel;
    private String url;
    private String name;

    public URLSetter(XPEPanel panel, String name, String url)
    {
        super(name);
        this.panel = panel;
        this.url = url;
        this.name = name;
    }

    public void actionPerformed(ActionEvent e) {
        panel.setURL(url);
    }

    public String getName() {
        return name;
    }

    public String getURL() {
        return url;
    }

    static String[] sampleNames = {
        "axis.xml",
        "basicupdate.xml",
        "basic.xml",
        "contents.xml",
        "defaultNamespace.xml",
        "evaluate.xml",
        "fibo.xml",
        "funky.html",
        "FUNKY2.html",
        "id.xml",
        "message.xml",
        "moreover.xml",
        "much_ado.xml",
        "namespaces.xml",
        "nitf.xml",
        "numbers.xml",
        "pi.xml",
        "testNamespaces.xml",
        "text.xml",
        "web.xml",
    };

    public static List getSamples(XPEPanel panel)
    {
        List samples = new ArrayList(sampleNames.length);
        for (int i=0; i<sampleNames.length; ++i)
        {

            URL url = URLSetter.class.getResource("/xml/" + sampleNames[i]);
            if (url == null) {
                url = URLSetter.class.getResource("xml/" + sampleNames[i]);
            }
            if (url == null) {
                System.err.println("Can't find " + sampleNames[i]);
                continue;
            }
            URLSetter setter = new URLSetter(panel, sampleNames[i], url.toString());
            samples.add(setter);
        }
        return samples;
    }
}
