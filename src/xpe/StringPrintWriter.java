package xpe;

/**
 * @author Alex Chaffee
 * @author Scott Stanchfield
 **/

import java.io.*;

public class StringPrintWriter extends PrintWriter {
    public StringPrintWriter() {
        super(new StringWriter());
    }

    public String getString() {
        flush();
        return ((StringWriter) out).toString();
    }
}

