package xpe;

/**
 *
 *  
 * @author Bernhard Wagner <xpe.bw@xmlizer.biz>
 * 
 * 
 */
public interface DocumentLoader {
	/**
	 * Retrieves the LoadedDocument for the given url.
	 * @param url The url for which to retrieve the LoadedDocument.
	 * @return The LoadedDocument retrieved for the given url.
	 * @throws Exception
	 */
	public LoadedDocument loadDocument(String url) throws Exception;
	
	/**
	 * Returns true if the DocumentLoader supports namespaces.
	 * @return True if the DocumentLoader supports namespaces.
	 */
	public boolean supportsNamespaces();
}
