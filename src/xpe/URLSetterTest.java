package xpe;

import java.awt.event.*;
import java.util.*;

import junit.framework.*;
import xpe.gui.XPEPanel;

public class URLSetterTest extends BaseTest
{
    public void testSet() {
        XPEPanel panel = new XPEPanel();
        String url;
        url = urlForXmlFile("id.xml");
        URLSetter setter = new URLSetter(panel, "id.xml", url);
        setter.actionPerformed(new ActionEvent(this, 2, "foo"));
        assertEquals(url, panel.getURL());

        url = urlForXmlFile("basic.xml");
        setter = new URLSetter(panel, "basic.xml", url);
        setter.actionPerformed(new ActionEvent(this, 2, "foo"));
        assertEquals(url, panel.getURL());
    }

    public void testGetSamples()
    {
    	XPEPanel panel = new XPEPanel();
    	List samples = URLSetter.getSamples(panel);
    	assertEquals( URLSetter.sampleNames.length, samples.size() );
    	URLSetter setter = (URLSetter)samples.get(0);
    	assertEquals("axis.xml", setter.getName());
    }

}
