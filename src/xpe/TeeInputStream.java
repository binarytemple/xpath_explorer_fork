package xpe;

import java.io.*;

public class TeeInputStream extends FilterInputStream
{
    OutputStream out;

    TeeInputStream(InputStream in, OutputStream out)
    {
        super(in);
        this.out = out;
    }

    public int read() throws IOException
    {
        int c = super.read();
        if (c == -1)
        {
            out.close();
        }
        else
        {
            out.write(c);
        }
        return c;
    }

    public int read(byte b[], int off, int len) throws IOException
    {
        int count = super.read(b, off, len);
        if (count == -1)
        {
            out.close();
        }
        else
        {
            out.write(b, off, count);
        }
        return count;
    }

}
