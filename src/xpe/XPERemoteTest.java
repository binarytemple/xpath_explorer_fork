package xpe;

public class XPERemoteTest extends BaseTest
{
    public XPERemoteTest(String s)
    {
        super(s);
    }

    public void testRemoteURL() throws Exception
    {
        String url = "http://www.google.com";
        String xpath = "/html/head/title";

        XPE xpe = new XPE(url, xpath);
        assertEquals(1, xpe.getMatches().size());
        assertEquals("Google", xpe.getStringValue());
    }

    public void testRemoteURL2() throws Exception
    {
        String url = "http://www.stinky.com/index.html";
        String xpath = "/html/head/title";

        XPE xpe = new XPE(url, xpath);
        assertEquals(1, xpe.getMatches().size());
        assertEquals("The Stinky Artists' Collective", xpe.getStringValue());
    }

    public void testRemoteURLWithRedirect() throws Exception
    {
        String url = "http://www.stinky.com/";
        String xpath = "/html/head/title";

        XPE xpe = new XPE(url, xpath);
        assertEquals(1, xpe.getMatches().size());
        assertEquals("The Stinky Artists' Collective", xpe.getStringValue());
    }

//    public void testRemoteURL3() throws Exception
//    {
//        String url = "http://www.purpletech.com/index.jsp";
//        String xpath = "/html";
//
//        XPE xpe = new XPE(url, xpath);
//        assertEquals(1, xpe.getResults().size());
//    }

    public void testDocBook() throws Exception
    {
        // attempt to solve bug at
        // https://sourceforge.net/tracker/?func=detail&atid=474579&aid=655886&group_id=54719
        new XPE(urlForXmlFile("docbook1.xml"), "/");
        new XPE(urlForXmlFile("docbook2.xml"), "/");
        // see also local test inside XPETest
    }

}
