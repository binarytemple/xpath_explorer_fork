package xpe;


import xpe.gui.XPEPanel;

public class Version
{
    static public String getVersion() {
        try {
            java.io.InputStream in = XPEPanel.class.getResourceAsStream("/version.txt");
            String version = com.purpletech.util.IOUtils.readStream(in);
            in.close();
            return version.trim();
        }
        catch (Exception e) {
            return "";
        }

    }

}

