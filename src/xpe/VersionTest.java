package xpe;

/**
 * VersionTest.java
 *
 *
 * Created: Tue Apr 16 22:55:20 2002
 *
 * @author <a href="mailto:alex@stinky.com">Alex Chaffee</a>
 * @version
 */

abstract public class VersionTest {
    public static void main(String[] args) {
        if (Version.getVersion().equals(args[0])) {
            System.exit(0);
        }
        else {
            System.err.println("Version: expected '" + args[0] +"', got '" + Version.getVersion() + "'");
            System.exit(1);
        }
    }

    public void testDummy() {}
}// VersionTest
