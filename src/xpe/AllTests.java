package xpe;

import org.xpathexplorer.xslt.*;
import junit.framework.*;
import net.sourceforge.xpe.xmlassert.*;

//todo: rename to AllLocalTests? (since it excludes the Remote tests)
public class AllTests extends TestSuite
{
    public static void main(String[] args)
    {
        junit.textui.TestRunner.run(suite());
    }

    public static Test suite()
    {
        TestSuite suite = new TestSuite("All Tests");
        suite.addTest(xpeSuite());
        suite.addTest(xsltTesterSuite());
        suite.addTest(xmldiffTesterSuite());
        return suite;
    }

    private static Test xsltTesterSuite()
    {
        TestSuite suite = new TestSuite("All XsltTester Tests");
        suite.addTestSuite(org.xpathexplorer.xslt.XsltTesterTest.class);
        suite.addTestSuite(LookupUriResolverTest.class);
        suite.addTestSuite(XsltFixtureTest.class);
        return suite;
    }

    private static Test xmldiffTesterSuite()
    {
        return XmlAssertTests.suite();
    }

    private static Test xpeSuite()
    {
        TestSuite suite = new TestSuite("All XPath Explorer Tests");
        suite.addTest(new TestSuite(xpe.HttpUnitXPETest.class));
        suite.addTest(new TestSuite(xpe.gui.DOMTreeTest.class));
        suite.addTest(new TestSuite(xpe.gui.NodePrinterTest.class));
        suite.addTest(new TestSuite(XPETest.class));
        suite.addTest(new TestSuite(StringPrintWriterTest.class));
        suite.addTest(new TestSuite(xpe.gui.XPEPanelTest.class));
        suite.addTest(new TestSuite(URLSetterTest.class));
        suite.addTest(new TestSuite(DocumentLoaderTest.class));
        suite.addTest(xpe.gui.InitialGuiTest.suite());
        return suite;
    }
}
