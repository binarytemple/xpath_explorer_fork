package xpe;

import org.w3c.dom.Document;

// todo: add Source
// todo: add parser info
public class LoadedDocument {
    private String url;
    private Document document;
    private String source;

    public LoadedDocument(String url, Document document, String source)
    {
        this.url = url;
        this.document = document;
        this.source = source;
    }

    public String getUrl()
    {
        return url;
    }

    public Document getDocument()
    {
        return document;
    }

    public String getSource()
    {
        return source;
    }
}
