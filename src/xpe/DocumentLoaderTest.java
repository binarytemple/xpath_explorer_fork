package xpe;

import com.purpletech.util.*;

import java.io.*;

//todo: write a mock HTTP server and then write these tests
public class DocumentLoaderTest extends BaseTest
{
    public DocumentLoaderTest(String s)
    {
        super(s);
    }

	private DocumentLoader loader = DocumentLoaderFactory.instance().getDocumentLoader();

    public void testSourceXml() throws Exception
    {
        LoadedDocument doc = loader.loadDocument(urlForXmlFile("id.xml"));
        String text = IOUtils.readFile(new File(filePathForXmlFile(("id.xml"))));
        assertEquals(text, doc.getSource());
    }

    public void testSourceHtml() throws Exception
    {
        LoadedDocument doc = loader.loadDocument(urlForXmlFile("funky.html"));
        String text = IOUtils.readFile(new File(filePathForXmlFile("funky.html")));
        assertEquals(text, doc.getSource());
    }

    public void testLocalXml() throws Exception
    {

    }

    public void testLocalHtml() throws Exception
    {

    }

    public void testRemoteXml() throws Exception
    {

    }

    public void testRemoteHtml() throws Exception
    {

    }

    public void testRedirectXml() throws Exception
    {

    }

    public void testRedirectHtml() throws Exception
    {

    }

    public void testString() throws Exception
    {

    }
}
