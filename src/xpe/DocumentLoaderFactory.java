package xpe;

/**
 *
 *  
 * @author Bernhard Wagner <xpe.bw@xmlizer.biz>
 * 
 * 
 */
public class DocumentLoaderFactory {
	
	/**
	 * Private constructor: Singleton Design Pattern.
	 *
	 */
	private DocumentLoaderFactory(){
		setDocumentLoaderClassName(this.documentLoaderClassName);
	}
	
	/**
	 * Retrieves the one and only instance of DocumentLoaderFactory 
	 * (Singleton Design Pattern).
	 * @return The one and only instance of DocumentLoaderFactory.
	 */
	public static DocumentLoaderFactory instance(){
		return instance;
	}
	
	/**
	 * Retrieves the DocumentLoader created by this DocumentLoaderFactory.
	 * @return The DocumentLoader created by this DocumentLoaderFactory.
	 */
	public DocumentLoader getDocumentLoader(){
		return this.documentLoader;
	}
	
	/**
	 * Sets the class name of the DocumentLoader to create by the DocumentLoaderFactory.
	 * @param The class name of the DocumentLoader to create by the DocumentLoaderFactory.
	 */
	public void setDocumentLoaderClassName(String className){
		try {
			Class c = Class.forName(className);
			DocumentLoader loader = (DocumentLoader)c.newInstance();
			this.documentLoaderClassName = className;
			this.documentLoader = loader;
		} catch (ClassNotFoundException e){
			throw new RuntimeException("Class not found: <"+className+">");
		} catch (IllegalAccessException e){
			throw new RuntimeException("illegal access: <"+className+">");
		} catch (InstantiationException e){
			throw new RuntimeException("instantiation: <"+className+">");
		}
	}
	
	private static DocumentLoaderFactory instance = new DocumentLoaderFactory();
	
	private String documentLoaderClassName = "xpe.DocumentLoaderImplPlain";
	private DocumentLoader documentLoader;
	
}
