package xpe;

import junit.framework.*;

public class StringPrintWriterTest extends TestCase {

    public StringPrintWriterTest(String s) {
	super(s);
    }

    public void testBasic() {
	StringPrintWriter out = new StringPrintWriter();
	out.print("foo");
	assertEquals("foo", out.getString());
    }

}
