package xpe;

import java.util.HashMap;

import org.w3c.dom.Document;

import junit.framework.*;
import com.purpletech.util.Utils;

abstract public class BaseTest extends TestCase
{
    public BaseTest() {  }

    public BaseTest(String s) { super(s); }

    protected String stripWhitespace(String s) {
        s = Utils.replace(s, "\r", "");
        s = Utils.replace(s, "\n", "");
        s = Utils.replace(s, " ", "");
        return s;
    }

    public static final String newline = System.getProperty("line.separator");

    public static final String linebreak = "<br>" + newline;

    public static final String tidy = "&lt;meta content='HTML Tidy, see www.w3.org' name='generator'/&gt;";

    public static String htmlIndent(int indent) {
        String out = "";
        for (int i=0; i<indent; ++i) {
            out += "&nbsp;&nbsp;";
        }
        return out;
    }

    public static final String textBasicCheese =
        "<foo>" + newline +
        "  <bar>" + newline +
        "    <baz/>" + newline +
        "    <*cheese*/>" + newline +
        "    <baz/>" + newline +
        "    <*cheese*/>" + newline +
        "    <baz/>" + newline +
        "  </bar>" + newline +
        "</foo>" + newline;

    public static final String textBasicBaz =
        "<foo>" + newline +
        "  <bar>" + newline +
        "    <*baz*/>" + newline +
        "    <cheese/>" + newline +
        "    <*baz*/>" + newline +
        "    <cheese/>" + newline +
        "    <*baz*/>" + newline +
        "  </bar>" + newline +
        "</foo>" + newline;

    protected static final String textIdCheese=
        "<foo id='foobar'>" + newline +
        "  <bar id='fb1'>" + newline +
        "    baz" + newline +
        "    <*cheese* kind='edam'>" + newline +
        "      gouda" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "    <*cheese* kind='gouda'>" + newline +
        "      cheddar" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "  </bar>" + newline +
        "</foo>" + newline;

    protected final static String textIdKind =
        "<foo id='foobar'>" + newline +
        "  <bar id='fb1'>" + newline +
        "    baz" + newline +
        "    <cheese *kind*='edam'>" + newline +
        "      gouda" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "    <cheese *kind*='gouda'>" + newline +
        "      cheddar" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "  </bar>" + newline +
        "</foo>" + newline;


    protected final static String htmlEmpty =
        "<html>" + newline +
        "  <head>" + newline +
        "" + newline +
        "  </head>" + newline +
        "  <body>" + newline +
        "    <p>" + newline +
        "      " + newline +
        "    </p>" + newline +
        "  </body>" + newline +
        "</html>" + newline;

    protected final static String swingTop =
        "<html>" + newline +
        "  <head>" + newline +
        "    " + newline +
        "  </head>" + newline +
        "  <body>" + newline +
        "    <font face=\"sans\">";

    protected final static String swingBottom =
        "    </font>" +
        "  </body>" + newline +
        "</html>" + newline;

    protected final static String resultsIdCheese =
        "<*cheese* kind='edam'>" + newline +
        "<*cheese* kind='gouda'>" + newline;

    protected final static String br = "<br>" + newline;

    protected final static String htmlMatchStart = "<font color='blue'><b class='match'>";
    protected final static String htmlMatchEnd = "</b></font>";

    protected final static String resultsBasicCheeseHTML =
        "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + "/&gt;" + br +
        "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + "/&gt;" + br +
        "";

    protected final static String resultsIdCheeseHTML =
        "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + " kind='edam'&gt;<br>" +
        "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + " kind='gouda'&gt;<br>";

    protected final static String htmlBasicCheese =
        "&lt;foo&gt;" + br +
        htmlIndent(1) + "&lt;bar&gt;" + br +
        htmlIndent(2) + "&lt;baz/&gt;" + br +
        htmlIndent(2) + "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + "/&gt;" + br +
        htmlIndent(2) + "&lt;baz/&gt;" + br +
        htmlIndent(2) + "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + "/&gt;" + br +
        htmlIndent(2) + "&lt;baz/&gt;" + br +
        htmlIndent(1) + "&lt;/bar&gt;" + br +
        "&lt;/foo&gt;" + br;

    protected final static String textIdCheeseKind =
        "<foo id='foobar'>" + newline +
        "  <bar id='fb1'>" + newline +
        "    baz" + newline +
        "    <cheese *kind*='edam'>" + newline +
        "      gouda" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "    <cheese *kind*='gouda'>" + newline +
        "      cheddar" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "  </bar>" + newline +
        "</foo>" + newline;

    protected static final String htmlIdCheese=
        "&lt;foo id='foobar'&gt;" + br +
        htmlIndent(1) + "&lt;bar id='fb1'&gt;" + br +
        htmlIndent(2) + "baz" + br +
        htmlIndent(2) + "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + " kind='edam'&gt;" + br +
        htmlIndent(3) + "gouda" + br +
        htmlIndent(2) + "&lt;/cheese&gt;" + br +
        htmlIndent(2) + "baz" + br +
        htmlIndent(2) + "&lt;" + htmlMatchStart + "cheese" + htmlMatchEnd + " kind='gouda'&gt;" + br +
        htmlIndent(3) + "cheddar" + br +
        htmlIndent(2) + "&lt;/cheese&gt;" + br +
        htmlIndent(2) + "baz" + br +
        htmlIndent(1) + "&lt;/bar&gt;" + br +
        "&lt;/foo&gt;" + br;

    protected static final String htmlIdFoo=
        "&lt;" + htmlMatchStart + "foo" + htmlMatchEnd + " id='foobar'&gt;" + br +
        htmlIndent(1) + "&lt;bar id='fb1'&gt;" + br +
        htmlIndent(2) + "baz" + br +
        htmlIndent(2) + "&lt;cheese kind='edam'&gt;" + br +
        htmlIndent(3) + "gouda" + br +
        htmlIndent(2) + "&lt;/cheese&gt;" + br +
        htmlIndent(2) + "baz" + br +
        htmlIndent(2) + "&lt;cheese kind='gouda'&gt;" + br +
        htmlIndent(3) + "cheddar" + br +
        htmlIndent(2) + "&lt;/cheese&gt;" + br +
        htmlIndent(2) + "baz" + br +
        htmlIndent(1) + "&lt;/bar&gt;" + br +
        "&lt;/foo&gt;" + br;

    protected static final String xmlContents =
        "<JavaXML:Book ora:category='Java' xmlns:JavaXML='http://www.oreilly.com/catalog/javaxml/' xmlns:ora='http://www.oreilly.com' xmlns:unused='http://www.unused.com'>" + newline +
        "  <#comment/>" + newline +
        "  <#comment/>" + newline +
        "  <JavaXML:Title>" + newline +
        "    Java and XML" + newline +
        "  </JavaXML:Title>" + newline +
        "  <JavaXML:Contents xmlns:topic='http://www.oreilly.com/topics'>" + newline +
        "    <JavaXML:Chapter topic:focus='XML'>" + newline +
        "      <JavaXML:Heading>" + newline +
        "        Introduction" + newline +
        "      </JavaXML:Heading>" + newline +
        "      <JavaXML:Topic subSections='7'>" + newline +
        "        What Is It?" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='3'>" + newline +
        "        How Do I Use It?" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='4'>" + newline +
        "        Why Should I Use It?" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='0'>" + newline +
        "        What's Next?" + newline +
        "      </JavaXML:Topic>" + newline +
        "    </JavaXML:Chapter>" + newline +
        "    <JavaXML:Chapter topic:focus='XML'>" + newline +
        "      <JavaXML:Heading>" + newline +
        "        Creating XML" + newline +
        "      </JavaXML:Heading>" + newline +
        "      <JavaXML:Topic subSections='0'>" + newline +
        "        An XML Document" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='2'>" + newline +
        "        The Header" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='6'>" + newline +
        "        The Content" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='1'>" + newline +
        "        What's Next?" + newline +
        "      </JavaXML:Topic>" + newline +
        "    </JavaXML:Chapter>" + newline +
        "    <JavaXML:Chapter topic:focus='Java'>" + newline +
        "      <JavaXML:Heading>" + newline +
        "        Parsing XML" + newline +
        "      </JavaXML:Heading>" + newline +
        "      <JavaXML:Topic subSections='3'>" + newline +
        "        Getting Prepared" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='3'>" + newline +
        "        SAX Readers" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='9'>" + newline +
        "        Content Handlers" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='4'>" + newline +
        "        Error Handlers" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='0'>" + newline +
        "        A Better Way to Load a Parser" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='4'>" + newline +
        "        \"Gotcha!\"" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='0'>" + newline +
        "        What's Next?" + newline +
        "      </JavaXML:Topic>" + newline +
        "    </JavaXML:Chapter>" + newline +
        "    <JavaXML:SectionBreak/>" + newline +
        "    <JavaXML:Chapter topic:focus='Java'>" + newline +
        "      <JavaXML:Heading>" + newline +
        "        Web Publishing Frameworks" + newline +
        "      </JavaXML:Heading>" + newline +
        "      <JavaXML:Topic subSections='4'>" + newline +
        "        Selecting a Framework" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='4'>" + newline +
        "        Installation" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='3'>" + newline +
        "        Using a Publishing Framework" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='2'>" + newline +
        "        XSP" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='3'>" + newline +
        "        Cocoon 2.0 and Beyond" + newline +
        "      </JavaXML:Topic>" + newline +
        "      <JavaXML:Topic subSections='0'>" + newline +
        "        What's Next?" + newline +
        "      </JavaXML:Topic>" + newline +
        "    </JavaXML:Chapter>" + newline +
        "  </JavaXML:Contents>" + newline +
        "</JavaXML:Book>" + newline;

    protected static String textId =
        "<foo id='foobar'>" + newline +
        "  <bar id='fb1'>" + newline +
        "    baz" + newline +
        "    <cheese kind='edam'>" + newline +
        "      gouda" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "    <cheese kind='gouda'>" + newline +
        "      cheddar" + newline +
        "    </cheese>" + newline +
        "    baz" + newline +
        "  </bar>" + newline +
        "</foo>" + newline;

    protected static String htmlId = "&lt;foo id='foobar'&gt;" + linebreak +
        "&nbsp;&nbsp;&lt;bar id='fb1'&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;baz" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;cheese kind='edam'&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gouda" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;/cheese&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;baz" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;cheese kind='gouda'&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cheddar" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;/cheese&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;baz" + linebreak +
        "&nbsp;&nbsp;&lt;/bar&gt;" + linebreak +
        "&lt;/foo&gt;" + linebreak;

    protected static final String htmlIdMatching = "&lt;" + htmlMatchStart + "foo" + htmlMatchEnd + " id='foobar'&gt;" + linebreak +
        "&nbsp;&nbsp;&lt;bar id='fb1'&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;baz" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;cheese kind='edam'&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;gouda" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;/cheese&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;baz" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;cheese kind='gouda'&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;cheddar" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;&lt;/cheese&gt;" + linebreak +
        "&nbsp;&nbsp;&nbsp;&nbsp;baz" + linebreak +
        "&nbsp;&nbsp;&lt;/bar&gt;" + linebreak +
        "&lt;/foo&gt;" + linebreak;

    protected static String htmlFunky = "&lt;html&gt;" + linebreak +
        htmlIndent(1) + "&lt;head&gt;" + linebreak +
        htmlIndent(2) + tidy + linebreak +
        htmlIndent(2) + "&lt;title&gt;" + linebreak +
        htmlIndent(3) + "Funky" + linebreak +
        htmlIndent(2) + "&lt;/title&gt;" + linebreak +
        htmlIndent(1) + "&lt;/head&gt;" + linebreak +
        htmlIndent(1) + "&lt;body&gt;" + linebreak +
        htmlIndent(2) + "Won't you take me &amp; you to fu&ntilde;ky" + linebreak +
        htmlIndent(2) + "&lt;a href='fr&egrave;&aacute;k &quot;out&quot; city'&gt;" + linebreak +
        htmlIndent(3) + "town" + linebreak +
        htmlIndent(2) + "&lt;/a&gt;" + linebreak +
        htmlIndent(2) + "?" + linebreak +
        htmlIndent(1) + "&lt;/body&gt;" + linebreak +
        "&lt;/html&gt;" + linebreak;


    protected static HashMap documents = new HashMap();

    protected Document getDocument(String url) throws Exception
    {
        Document document = (Document)documents.get(url);
        if (document == null) {
            DocumentLoader loader = DocumentLoaderFactory.instance().getDocumentLoader();
            document = loader.loadDocument(url).getDocument();
            documents.put(url, document);
        }
        return document;
    }

  public static String urlForXmlFile(String relativePath) {
    return "file:" + filePathForXmlFile(relativePath);
  }

  public static String filePathForXmlFile(String relativePath) {
    return "resources/xml/" + relativePath;
  }

}


