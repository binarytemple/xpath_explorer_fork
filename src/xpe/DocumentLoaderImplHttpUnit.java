
package xpe;

import com.meterware.httpunit.*;

/**
 *
 *  
 * @author Bernhard Wagner <xpe.bw@xmlizer.biz>
 * The Implementation of DocumentLoader using httpunit.
 * 
 */
public class DocumentLoaderImplHttpUnit implements DocumentLoader {
	
	/**
	 * Default Ctor.
	 */
	public DocumentLoaderImplHttpUnit(){
		com.meterware.httpunit.parsing.HTMLParserFactory.useNekoHTMLParser();
		com.meterware.httpunit.parsing.HTMLParserFactory.setPreserveTagCase(true);
	}

	/**
	 * @see xpe.DocumentLoader#loadDocument(String)
	 */
	public LoadedDocument loadDocument (String url) throws Exception {
		WebConversation conversation = new WebConversation();
		WebResponse response = conversation.getResponse(
			new GetMethodWebRequest(url)
//				  new PostMethodWebRequest(url)
		);
		return new LoadedDocument(url, response.getDOM(), response.getText());
	}
    
	/**
	 * @see xpe.DocumentLoader#supportsNamespaces()
	 */
	public boolean supportsNamespaces(){
		return false;
	}

}
