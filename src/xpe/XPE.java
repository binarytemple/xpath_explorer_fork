package xpe;

import org.jaxen.dom.*;
import org.w3c.dom.*;
import xpe.gui.NodePrinter;

import java.util.*;

/**
 * XPath Explorer
 *
 * @author Alex Chaffee (alex@purpletech.com)
 * @author Ian McFarland (ian@neo.com) helped with "generate XPath" function
 **/
public class XPE
{

    private String xpathExpression;
    private LoadedDocument loadedDocument;
    private DOMXPath xpath;
    private List matches;
    private String stringValue;
    private Number numberValue;
    private boolean booleanValue;

    public XPE(String documentURL, String xpathExpression) throws Exception
    {
        load(documentURL);
        createXPathObject(xpathExpression);
        evaluate();
    }

    private void load(String documentURL) throws Exception
    {
        this.loadedDocument = DocumentLoaderFactory.instance().getDocumentLoader().loadDocument(documentURL);
    }

    public String getXPath() {
        return xpathExpression;
    }

    public String getURL() {
        return loadedDocument.getUrl();
    }

    public Document getDocument() throws Exception
    {
        return loadedDocument.getDocument();
    }

    public String getSource() throws Exception
    {
        return loadedDocument.getSource();
    }

    public String getStringValue() throws Exception
    {
        return stringValue;
    }

    public Number getNumberValue() throws Exception
    {
        return numberValue;
    }

    public boolean getBooleanValue() throws Exception
    {
        return booleanValue;
    }

    public void setDocument(String url) throws Exception
    {
        load(url);
        evaluate();
    }

    public void setDocumentAndXpath(String url, String xpath) throws Exception
    {
        load(url);
        setXPath(xpath);
        evaluate();
    }

    public void setXPath(String xpathExpression) throws Exception
    {
        createXPathObject(xpathExpression);
        evaluate();
    }

    public int hashCode()
    {
        return xpathExpression.hashCode() ^ loadedDocument.getUrl().hashCode();
    }

    public boolean equals(Object o)
    {
        if (!(o instanceof XPE))
            return false;
        XPE other = (XPE) o;
        return xpathExpression.equals(other.xpathExpression)
            && loadedDocument.getUrl().equals(other.loadedDocument.getUrl());
    }

    public String toString()
    {
        return "XPE[url=" + loadedDocument.getUrl() + ",xpath=" + xpathExpression + "]";
    }

    public Set getNamespaces() throws Exception
    {
        return extractNamespaces(getDocument());
    }

    private Set extractNamespaces(Document doc) throws Exception
    {
        Set namespaces = new TreeSet();
        extractNamespaces(doc.getDocumentElement(), namespaces);
        return namespaces;
    }

    private void extractNamespaces(Node node, Set namespaces) throws Exception
    {
        /** turns out this won't work unless we tell the parser to understand
         * namespaces, and I don't know how to do that yet

        System.out.println("node: " + node + " prefix: " + node.getPrefix());
        String prefix = node.getPrefix();
        String uri = node.getNamespaceURI();
        if (prefix != null)
        {
            namespaces.add(new XPE.Namespace(prefix, uri));
        }
        */

        NamedNodeMap attributes = node.getAttributes();
        if (attributes != null)
        {
            for (int i = 0; i < attributes.getLength(); ++i)
            {
                String name = attributes.item(i).getNodeName();
                if (name.startsWith("xmlns:"))
                {
                    String prefix = name.substring(6);
                    String uri = attributes.item(i).getNodeValue();
                    namespaces.add(new XPE.Namespace(prefix, uri));
                }
            }
        }

        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i)
        {
            extractNamespaces(children.item(i), namespaces);
        }
    }

    public static class Namespace implements Comparable
    {
        public String prefix;
        public String uri;
        public Namespace(String prefix, String uri)
        {
            this.prefix = prefix;
            this.uri = uri;
        }
        public int hashCode()
        {
            return prefix.hashCode() ^ uri.hashCode();
        }
        public boolean equals(Object o)
        {
            return ((Namespace) o).prefix.equals(prefix)
                && ((Namespace) o).uri.equals(uri);
        }
        public int compareTo(Object o)
        {
            Namespace other = (Namespace) o;
            int x = prefix.compareTo(other.prefix);
            if (x == 0)
            {
                x = uri.compareTo(other.uri);
            }
            return x;
        }
        public String toString()
        {
            return "XPE.Namespace[prefix=" + prefix + ",uri=" + uri + "]";
        }
    }

    private void createXPathObject(String xpathExpression) throws Exception
    {
        this.xpathExpression = xpathExpression;
        xpath = new DOMXPath(xpathExpression);
        for (Iterator i = getNamespaces().iterator(); i.hasNext();)
        {
            XPE.Namespace namespace = (XPE.Namespace) i.next();
            xpath.addNamespace(namespace.prefix, namespace.uri);
        }
    }

    void evaluate() throws Exception
    {
        matches = xpath.selectNodes(getDocument());
        stringValue = xpath.stringValueOf(getDocument());
        numberValue = xpath.numberValueOf(getDocument());
        booleanValue = xpath.booleanValueOf(getDocument());
    }

    // test only
    String getFullText() throws Exception
    {
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter nodePrinter = new NodePrinter(out, matches);
        nodePrinter.printFullNode(getDocument().getDocumentElement());
        return out.getString();
    }

    // test only
    String getFullHTML() throws Exception
    {
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter nodePrinter = new NodePrinter(out, matches, true, false);
        nodePrinter.printFullNode(getDocument().getDocumentElement());
        return out.getString();
    }

    public String getExpandedXPath()
    {
        return xpath.toString();
    }

    public List getMatches()
    {
        return matches;
    }

    private String newline()
    {
        return System.getProperty("line.separator");
    }

    public String getMatchesText() throws Exception
    {
        return getMatchesString(newline(), false);
    }

    public String getMatchesHTML() throws Exception
    {
        return getMatchesString("<br>" + newline(), true);
    }

    private String getMatchesString(String eol, boolean isHTML) throws Exception
    {
        StringPrintWriter out = new StringPrintWriter();
        NodePrinter np = new NodePrinter(out, matches, isHTML, false);
        for (Iterator i = matches.iterator(); i.hasNext();)
        {
            Object result = i.next();
            if (result instanceof Node)
            {
                Node node = (Node) result;
                np.printSingleNode(node, 0);
            }
            else
            {
                out.print(result.toString());
            }
            out.print(eol);
        }
        return out.getString();
    }

    static private String getID(Node node)
    {
        NamedNodeMap attributes = node.getAttributes();
        if (attributes == null)
            return null;
        Node id = attributes.getNamedItem("id");
        if (id == null)
            return null;
        return id.getNodeValue();
    }

    static private List getChildrenWithName(Node parent, String name)
    {
        NodeList children = parent.getChildNodes();
        List results = new ArrayList();
        for (int i = 0; i < children.getLength(); ++i)
        {
            Node child = children.item(i);
            if (child == null)
                continue;
            String nodeName = child.getNodeName();
            if (nodeName == null)
                continue;
            if (nodeName.equals(name))
            {
                results.add(child);
            }
        }
        return results;

    }

    //todo: move into XpathGenerator object
    static public String generateXPath(Node node)
    {
        Node parent = node.getParentNode();

        switch (node.getNodeType())
        {
            case Node.ATTRIBUTE_NODE :
                parent = ((Attr) node).getOwnerElement();
                return generateXPath(parent) + "/@" + node.getNodeName();

            case Node.TEXT_NODE :
                return "string(" + generateXPath(parent) + ")";

            case Node.ELEMENT_NODE :
            default :
                if (parent == null
                    || parent.getNodeType() == Node.DOCUMENT_NODE)
                {
                    return "/" + node.getNodeName();
                }

                if (getID(node) != null)
                {
                    return "//"
                        + node.getNodeName()
                        + "[@id='"
                        + getID(node)
                        + "']";
                }

                String path = generateXPath(parent) + '/' + node.getNodeName();
                List siblings = getChildrenWithName(parent, node.getNodeName());
                if (siblings.size() > 1)
                {
                    path += "[" + (siblings.indexOf(node) + 1) + "]";
                }
                return path;
        }
    }

}
