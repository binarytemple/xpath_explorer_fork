package org.xpathexplorer.xslt;

import com.purpletech.util.IOUtils;
import junit.framework.TestCase;

import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.net.URL;

import org.apache.commons.lang.StringUtils;

public class LookupUriResolverTest extends TestCase {

    static Random random = new Random();
    LookupUriResolver resolver = new LookupUriResolver();
    String content = "CONTENT-" + random.nextInt();
    String someUri = "SOME-URI";

    public void testNotFound() throws Exception {
        try {
            resolver.resolve(someUri, "");
            fail("should have thrown an exception when unable to find uri");
        } catch (TransformerException e) {
            //ok
        }
    }

    public void testRegisterString() throws Exception {
        resolver.register(someUri, content);
        assertResolveToContent();
    }

    private void assertResolveToContent() throws TransformerException, IOException {
        String s = resolveUri(someUri);
        assertEquals(content, s);
    }

    private String resolveUri(String someUri) throws TransformerException, IOException {
        StreamSource source = resolveToSource(someUri);
        return IOUtils.readReader(source.getReader());
    }

    private StreamSource resolveToSource(String someUri) throws TransformerException {
        StreamSource source = (StreamSource) resolver.resolve(someUri, "");
        return source;
    }

    public void testRegisterFile() throws Exception {
        File dir = new File("build");
        dir.mkdirs();
        File file = File.createTempFile("temp", ".txt", dir);
        IOUtils.writeString(file, content);
        resolver.register(someUri, file);
        assertResolveToContent();

        StreamSource source = resolveToSource(someUri);
        String systemId = source.getSystemId();
        assertTrue(systemId.startsWith("file:"));
        URL url = new URL(source.getSystemId());
        assertEndsWith(url.getPath(), file.getPath());
    }

    private void assertEndsWith(String fullString, String endingString) {
        fullString = StringUtils.replace(fullString, "\\", "/");
        endingString = StringUtils.replace(endingString, "\\", "/");
        assertEquals(fullString.substring(fullString.length() - endingString.length()), endingString);
    }

    public void testRegisterMap() throws Exception {
        Map map = new HashMap();
        map.put("foo", "FOO123");
        map.put("bar", "BAR456");
        resolver.register(map);
        assertEquals("FOO123", resolveUri("foo"));
        assertEquals("BAR456", resolveUri("bar"));
    }

    public void testReplaceString() throws Exception {
        resolver.addReplacement("XXX", "YYY");
        resolver.addReplacement("ZZZ", "AAA");
        resolver.register(someUri, "AAAXXXBBBZZZ");
        assertEquals("AAAYYYBBBAAA", resolveUri(someUri));
    }

    public void testReplaceForUri() throws Exception {
        resolver.addReplacementForUri("one", "xxx", "yyy");
        resolver.register("one", "aaaxxx");
        resolver.register("two", "bbbxxx");

        assertEquals("aaayyy", resolveUri("one"));
        assertEquals("bbbxxx", resolveUri("two"));
    }

    public void testBaseDir() throws Exception {
        File dir = new File("build/test");
        String filename = randomFilename();
        dir.mkdirs();
        IOUtils.writeString(new File(dir, filename), content);
        resolver.addBaseDir(dir);

        assertEquals(content, resolveUri(filename));
        assertEquals(content, resolveUri("/" + filename));
        assertEquals(content, resolveUri("\\" + filename));
    }

    public void testMultipleBaseDirs() throws Exception {
        File dir = new File("build/test");
        File one = new File(dir, "one");
        File two = new File(dir, "two");
        dir.mkdirs();
        one.mkdirs();
        two.mkdirs();
        String filename = randomFilename();
        File target = new File(two, filename);
        IOUtils.writeString(target, content);

        resolver.addBaseDir(one);
        resolver.addBaseDir(two);
        assertEquals(content, resolveUri(filename));
    }

    private String randomFilename() {
        String filename = "foo-" + random.nextInt() + ".txt";
        return filename;
    }

    public void testAbsoluteHref() throws Exception {
        File dir = new File("build/test");
        dir.mkdirs();
        String filename = randomFilename();
        File target = new File(dir, filename);
        IOUtils.writeString(target, content);
		//TODO: finish this test?
    }
    
    public void testAbsolutePath() {
		assertTrue(LookupUriResolver.isAbsolutePath("c:/foo/bar"));
		assertTrue(LookupUriResolver.isAbsolutePath("C:/foo/bar"));
		assertTrue(LookupUriResolver.isAbsolutePath("D:\\foo\\bar"));
		
		assertFalse(LookupUriResolver.isAbsolutePath("c"));
		assertFalse(LookupUriResolver.isAbsolutePath("cow"));
		assertFalse(LookupUriResolver.isAbsolutePath("D:E"));

    }

}
