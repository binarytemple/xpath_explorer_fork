package org.xpathexplorer.xslt;

import junit.framework.TestCase;

import java.io.File;
import java.io.IOException;

import com.purpletech.util.IOUtils;

public class XsltFixtureTest extends TestCase
{
    protected final String xmlOokData = "<data>ook</data>";

    XsltFixture fixture;
    ResolverConfigFixture config;

    //todo: test all functions

    public void testUriResolution() throws Exception
    {
        config = new ResolverConfigFixture();
        config.uri = "foo";
        config.xslt = "<xsl:template match='data'><foo><xsl:apply-templates/></foo></xsl:template>";
        config.set();

        fixture = new XsltFixture();
        fixture.xml = xmlOokData;
        fixture.xslt = "<xsl:include href='foo'/>";
        assertEquals("<foo>ook</foo>", fixture.transform());
    }

    public void testUriResolutionToFile() throws Exception
    {
        config = new ResolverConfigFixture();
        config.uri = "foo";
        config.file = "testdata/matchdata.xsl";
        config.set();

        fixture = new XsltFixture();
        fixture.xml = xmlOokData;
        fixture.xslt = "<xsl:include href='foo'/>";
        assertEquals("<foo>ook</foo>", fixture.transform());
    }

    public void testStringReplacement() throws Exception
    {
        config = new ResolverConfigFixture();
        config.replace = "xxx";
        config.content = "yyy";
        config.set();

        //note that it should *not* replace the XML at this point, since the XML is not an entity
        config.reset();
        config.replace = "ook";
        config.content = "eek";
        config.set();

        fixture = new XsltFixture();
        fixture.xml = xmlOokData;
        fixture.xslt = "<xsl:template match='data'><xxx><xsl:apply-templates/></xxx></xsl:template>";
        assertEquals("<yyy>ook</yyy>", fixture.transform());
    }

    public void testDoubleInclude() throws Exception
    {
        String one = "<xsl:include href='two'/>";
        String two = "<xsl:include href='three'/>";
        String three = "<xsl:template match='data'><result foo='xxx'/></xsl:template>";

        config = new ResolverConfigFixture();
        config.replace = "xxx";
        config.content = "yyy";
        config.set();
        config.reset();
        config.uri = "two";
        config.xslt = two;
        config.set();
        config.reset();
        config.uri = "three";
        config.xslt = three;
        config.set();
        config.reset();

        fixture = new XsltFixture();
        fixture.xml = "<data/>";
        fixture.xslt = one;
        String result = fixture.transform();
        assertEquals("<result foo=\"yyy\" />", result);

    }

    public void testDoubleIncludeFiles() throws Exception
    {
        String one = "<xsl:include href='two'/>";
        String two = "<xsl:include href='three'/>";
        String three = "<xsl:template match='data'><result foo='xxx'/></xsl:template>";

        File data = new File("build/test");
        write(data, "one.xsl", one);
        write(data, "two.xsl", two);
        write(data, "three.xsl", three);

        config = new ResolverConfigFixture();
        config.replace = "xxx";
        config.content = "yyy";
        config.set();
        config.reset();
        config.uri = "two";
        config.file = "build/test/two.xsl";
        config.set();
        config.reset();
        config.uri = "three";
        config.file = "build/test/three.xsl";
        config.set();
        config.reset();

        fixture = new XsltFixture();
        fixture.xml = "<data/>";
        fixture.xslt = one;
        String result = fixture.transform();
        assertEquals("<result foo=\"yyy\" />", result);
    }
    
    public void test() throws Exception
    {
        
    }

    private void write(File dir, String filename, String content) throws IOException {
        IOUtils.writeString(new File(dir, filename), XsltTester.createStylesheet(content));
    }
}
