package org.xpathexplorer.xslt;

import java.io.IOException;
import java.io.StringReader;
import java.io.File;
import java.util.*;

import javax.xml.transform.*;
import javax.xml.transform.sax.SAXSource;

import org.apache.commons.lang.StringUtils;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.Text;
import org.jdom.output.XMLOutputter;
import org.jdom.transform.JDOMResult;
import org.jdom.transform.JDOMSource;
import org.xml.sax.InputSource;
import net.sourceforge.xpe.xmlassert.XmlDiff;
import com.purpletech.util.IOUtils;
import xpe.StringPrintWriter;
import org.apache.commons.lang.StringEscapeUtils;
import net.sourceforge.xpe.xmlassert.XmlAssert;

public class XsltTester
{
    public String xmlHeader = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    public static String xslStylesheetElement = "<xsl:stylesheet xmlns:xsl='http://www.w3.org/1999/XSL/Transform' version='1.0'>";

    private String testedXslHref = "XSLT_TESTER_TEMPLATE_HREF";
    private String extraTemplates = "";
    private String testedXsl;
    private String inputXml = "";
    private List result;
    private String resultString;
    protected LookupUriResolver myUriResolver;

    private String driverTemplates = "";
    private Map variables = new HashMap();
    private String mode;

    public XsltTester()
    {
    }

    public XsltTester(String testedXsl) throws JDOMException, IOException
    {
        setTestedXsl(testedXsl);
    }

    public XsltTester(String testedXsl, String extraTemplates) throws JDOMException, IOException
    {
        setTestedXsl(testedXsl);
        setExtraTemplates(extraTemplates);
    }

    public void setTestedXsl(File testedXsl) throws IOException, JDOMException {
        setTestedXsl(IOUtils.readFile(testedXsl));
    }

    public void setTestedXsl(String testedXsl) throws JDOMException, IOException
    {
        if (needsXslStylesheetTag(testedXsl))
        {
            testedXsl = createStylesheet(testedXsl);
        }
        this.testedXsl = testedXsl;
    }

    public void setExtraTemplates(File extraTemplatesFile) throws IOException {
        setExtraTemplates(IOUtils.readFile(extraTemplatesFile));
    }

    public void setExtraTemplates(String extraTemplates) {
        this.extraTemplates = extraTemplates;
    }

    public void setInputXml(File xmlFile) throws IOException {
        setInputXml(IOUtils.readFile(xmlFile));
    }

    public void setInputXml(String xml) {
        this.inputXml = xml;
    }

    private boolean needsXslStylesheetTag(String xsl)
    {
        int iStylesheet = xsl.indexOf("<xsl:stylesheet");
        return (iStylesheet == -1);
    }

    /**
     * @deprecated
     */
    public String run(String xml) throws Exception
    {
        setInputXml(xml);
        return run();
    }

    public String run() throws Exception
    {
        String driverXsl = createDriverStylesheet();

        Document xslDocument = XmlDiff.parse(driverXsl, false);

        Document xmlDocument;
        if (inputXml == null || inputXml.trim().equals("")) {
            xmlDocument = new Document();
            xmlDocument.setRootElement(new Element("empty"));
        }
        else {
            xmlDocument = XmlAssert.asJdomDocument(inputXml, false);
        }

        result = transform(xmlDocument, xslDocument);
        resultString = stripped(result);
        return resultString;
    }

    /*
     * @deprecated use setInputXml() instead
     **/
    public String runMatch(String xml, String xpath) throws Exception
    {
        setInputXml(xml);
        return runMatch(xpath);
    }

    public String runMatch(String xpath) throws Exception
    {
        driverTemplates = "<xsl:template match='/'>" +
                "<xsl:apply-templates select='" + xpath + "'" + mode() + "/>" +
                "</xsl:template>";
        return run();
    }

    private String mode() {
        if (mode == null)
            return "";
        return " mode='" + mode + "'";
    }

    public String runMatch(String xpath, String[][] params) throws Exception
    {
        driverTemplates = "<xsl:template match='/'>" +
                "<xsl:apply-templates select='" + xpath + "'" + mode() + ">" +
                paramElementsString(params) +
                "</xsl:apply-templates>" +
                "</xsl:template>";
        return run();
    }


    private String stripped(List list) throws IOException
    {
        StringPrintWriter out = new StringPrintWriter();
        XMLOutputter outputter = outputter();

        for (Iterator iterator = list.iterator(); iterator.hasNext();)
        {
            Object o = iterator.next();
            if (o instanceof Element)
            {
                outputter.output((Element) o, out);
            }
            else if (o instanceof Text)
            {
                outputter.output((Text) o, out);
            }
            else
            {
                System.err.println("Unknown object type " + o.getClass().getName());
                out.print(o);
            }

        }
        return out.getString();
    }

    //TODO: make this configurable
    private XMLOutputter outputter()
    {
        XMLOutputter outputter = new XMLOutputter();
        outputter.setNewlines(false);
        outputter.setTrimAllWhite(true);
        return outputter;
    }

    private String createDriverStylesheet()
    {
        return createStylesheet(
                "<xsl:import href='" + testedXslHref + "'/>" +
                driverTemplates +
                extraTemplates +
                variables());
    }

    private String variables() {
        StringBuffer buf = new StringBuffer();
        Iterator names = variables.keySet().iterator();
        while (names.hasNext()) {
            String name = (String) names.next();
            String value = (String) variables.get(name);
            buf.append("<xsl:variable name=\"" + name + "\" select=\"" + value + "\"/>");
        }
        return buf.toString();
    }

    public static String createStylesheet(String templates)
    {
        return xslStylesheetElement + templates + "</xsl:stylesheet>";
    }

    public String runCallTemplate(String templateName) throws Exception
    {
        return runCallTemplate(templateName, new String[][]{});
    }

    public String runCallTemplate(String templateName, String params)
            throws Exception
    {
        return runCallTemplate(templateName, parseParams(params));
    }

    public String runCallTemplate(String templateName, String[][] params)
            throws Exception
    {
        driverTemplates =
                "<xsl:template match='/'>" +
                "<xsl:call-template name='" + templateName + "'" + ">" +
                paramElementsString(params) +
                "</xsl:call-template>" +
                "</xsl:template>";

        return run();
    }




    private String paramElementsString(String[][] params)
    {
        String result = "";
        for (int i = 0; i < params.length; i++)
        {
            result = result + "<xsl:with-param name='" + params[i][0] + "' select=\"" + params[i][1] + "\"/>";
        }
        return result;
    }

    public String getResultAsString()
    {
        return resultString;
    }

    public List getResult()
    {
        return result;
    }

    public Element getResultAsElement()
    {
        return getResultAsElement(new Element("result"));
    }

    //todo: test me
    public Element getResultAsElement(Element parentElement)
    {
//        if (parentElement.getChildren() == null) {
//            parentElement.addContent(getResult());
//        }
        parentElement.getChildren().addAll(getResult());
        return parentElement;
    }

    public String stringEquals(String expectedString)
    {
        if (expectedString.equals(getResultAsString()))
        {
            return "true";
        }
        else
        {
            return strdiff(expectedString, getResultAsString());
        }
    }

    public static String strdiff(String expectedString, String resultString)
    {
        return strdiff(expectedString, resultString, 10, 40);
    }

    private static String strdiff(String expectedString, String resultString, int leftMargin, int rightMargin)
    {
        int at = StringUtils.indexOfDifference(expectedString, resultString);
        if (at == -1) return "true";
        String expectedWindow = window(expectedString, at, leftMargin, rightMargin);
        String actualWindow = window(resultString, at, leftMargin, rightMargin);
        return "Expected: " + StringEscapeUtils.escapeJava(expectedWindow) + " " +
                "Actual: " + StringEscapeUtils.escapeJava(actualWindow);
    }

    private static String window(String s, int at, int leftMargin, int rightMargin)
    {
        return StringUtils.abbreviate(s, at - leftMargin, rightMargin);
    }

    public String xmlEquals(String xmlExpected) throws JDOMException, IOException
    {
        XmlDiff diff = new XmlDiff(xmlExpected, resultString);
        if (diff.same())
            return "true";
        else
        {
            return diff.print();
        }
    }

    private List transform(org.jdom.Document xml, org.jdom.Document xsl) throws TransformerException
    {
        TransformerFactory factory = TransformerFactory.newInstance();

		StringReader xmlReader = new StringReader(new XMLOutputter().outputString(xml));
        Source xslSource = new JDOMSource(xsl);
		Source xmlSource = new SAXSource(new InputSource(xmlReader));

        factory.setURIResolver(uriResolver());
        Transformer transformer = factory.newTransformer(xslSource);
        transformer.setURIResolver(uriResolver());

        JDOMResult result = new JDOMResult();
        transformer.transform(xmlSource, result);
        return result.getResult();
    }

    public LookupUriResolver uriResolver()
    {
        if (myUriResolver == null)
            myUriResolver = new LookupUriResolver();
        myUriResolver.register(testedXslHref, testedXsl);
        return myUriResolver;
    }

    public void register(String uri, String content)
    {
        uriResolver().register(uri, content);
    }

    public static String[][] parseParams(String params)
    {
        ArrayList list = new ArrayList();
        StringTokenizer tok = new StringTokenizer(params, ",");
        while (tok.hasMoreTokens())
        {
            String pair = tok.nextToken();
            int equalsign = pair.indexOf("=");
            String name = pair.substring(0, equalsign);
            String value = pair.substring(equalsign + 1);
            list.add(new String[]{name, value});
        }
        return (String[][]) list.toArray(new String[][]{});
    }

    public void setVariableValue(String name, String value) {
        variables.put(name, "'" + value + "'");
    }

    public void setVariableXpath(String name, String xpath) {
        variables.put(name, xpath);
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

}
