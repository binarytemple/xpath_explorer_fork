package org.xpathexplorer.xslt;

public class CalcFixture extends SimpleFixture
{
    public int x;
    public int y;

    public int getSum()
    {
        return x + y;
    }

    public int difference()
    {
        return x - y;
    }
}
