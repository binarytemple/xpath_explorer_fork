package org.xpathexplorer.xslt;

import org.jdom.JDOMException;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.StringTokenizer;

//todo: test me

public class XsltFixture extends SimpleFixture
{
    public String xslt;
    public String xsltFile;
    public String xsltExtra = "";

    public String xml;
    public String xmlFile;

    public String xpath;
    public String template;
    public String params;

    public String expected;
    public String expectedFile;

    protected XsltTester tester;

    public void reset() throws Exception
    {
        xslt = null;
        xsltFile = null;
        xsltExtra = "";

        xml = null;
        xmlFile = null;

        xpath = null;
        template = null;

        tester = null;
    }

    public void execute() throws Exception
    {
        //todo: test failure if more than one of (xslt, xsltFile) defined
        if (xslt == null)
        {
            if (xsltFile != null)
            {
                xslt = loadFile(new File(xsltFile));
            }
        }

        //todo: test failure if more than one of (xml, xmlFile) defined
        if (xml == null)
        {
            if (xmlFile != null)
            {
                xml = loadFile(new File(xmlFile));
            }
        }
    }

    public static String loadFile(File file) throws IOException
    {
        FileReader filein = new FileReader(file);
        BufferedReader in = new BufferedReader(filein);
        long length = file.length();
        StringBuffer buf = new StringBuffer((int) length);
        int b;
        while ((b = in.read()) != -1)
        {
            buf.append((char) b);
        }
        in.close();
        return buf.toString();
    }

    public String transform() throws Exception
    {
        XsltTester tester = tester();
        tester.setInputXml(xml);
        return tester.run();
    }

    XsltTester tester() throws JDOMException, IOException
    {
        if (tester == null)
            tester = new XsltTester(xslt, xsltExtra);
        tester.uriResolver().register(ResolverConfigFixture.uriMap);
        tester.uriResolver().addReplacements(ResolverConfigFixture.replacements);
        if (ResolverConfigFixture.base != null) {
            tester.uriResolver().addBaseDir(new File(ResolverConfigFixture.base));
        }
        return tester;
    }

    public String match() throws Exception
    {
        return tester().runMatch(xml, xpath);
    }

    public String call() throws Exception
    {
        tester().setInputXml(xml);
        if (params == null)
        return tester().runCallTemplate(template);
        else
            return tester().runCallTemplate(template, params);
    }

    public String stringEquals() throws IOException, JDOMException
    {
        return tester().stringEquals(getExpected());
    }

    public String xmlEquals() throws JDOMException, IOException
    {
        String message = tester.xmlEquals(getExpected());
        message = normalizeWhitespace(message);
        return message;
    }

    String normalizeWhitespace(String original)
    {
        StringBuffer buf = new StringBuffer();
        StringTokenizer tok = new StringTokenizer(original);
        while (tok.hasMoreTokens())
        {
            buf.append(tok.nextToken());
            if (tok.hasMoreTokens())
            {
                buf.append(" ");
            }
        }
        return buf.toString();
    }

    public String getExpected() throws IOException
    {
        if (expectedFile != null)
        {
            expected = loadFile(new File(expectedFile));
        }
        return expected;
    }
}
