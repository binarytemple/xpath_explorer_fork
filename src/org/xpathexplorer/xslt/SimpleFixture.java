/*
  * ====================================================================
  * Copyright (c) 1995-2003 Purple Technology, Inc. All rights
  * reserved.
  *
  * PLAIN LANGUAGE LICENSE: Do whatever you like with this code, free
  * of charge, just give credit where credit is due. If you improve it,
  * please send your improvements to code@purpletech.com. Check
  * http://www.purpletech.com/code/ for the latest version and news.
  *
  * LEGAL LANGUAGE LICENSE: Redistribution and use in source and binary
  * forms, with or without modification, are permitted provided that
  * the following conditions are met:
  *
  * 1. Redistributions of source code must retain the above copyright
  * notice, this list of conditions and the following disclaimer.
  *
  * 2. Redistributions in binary form must reproduce the above
  * copyright notice, this list of conditions and the following
  * disclaimer in the documentation and/or other materials provided
  * with the distribution.
  *
  * 3. The names of the authors and the names "Purple Technology,"
  * "Purple Server" and "Purple Chat" must not be used to endorse or
  * promote products derived from this software without prior written
  * permission. For written permission, please contact
  * server@purpletech.com.
  *
  * THIS SOFTWARE IS PROVIDED BY THE AUTHORS AND PURPLE TECHNOLOGY ``AS
  * IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
  * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
  * AUTHORS OR PURPLE TECHNOLOGY BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
  * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
  * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
  * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
  * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
  * OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  * ====================================================================
  *
  **/

package org.xpathexplorer.xslt;

import fit.ColumnFixture;
import fit.Parse;
import fit.TypeAdapter;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class SimpleFixture extends ColumnFixture
{
    List columnNames = new ArrayList();

    public void doCell(Parse cell, int column) {
        TypeAdapter a = columnBindings[column];
        try {
            String text = cell.text();

            String columnName = (String) columnNames.get(column);
            boolean isGetter = (columnName.endsWith("?") || columnName.endsWith("()"));

            if (text.equals("")) {
                check(cell, a);
            }
            else if (a == null) {
                ignore(cell);
            }
            else if (isGetter) {
                check(cell, a);
            }
            else {
                a.set(a.parse(text));
            }
        } catch(Exception e) {
            exception(cell, e);
        }
    }

    protected void bind (Parse heads) {
        columnBindings = new TypeAdapter[heads.size()];
        for (int i=0; heads!=null; i++, heads=heads.more) {
            String name = heads.text();
            columnNames.add(name);
            boolean isGetter = false;
            try {
                TypeAdapter adapter = null;
                if (!name.equals("")) {
                    if (name.endsWith("?")) {
                        name = name.substring( 0, name.length()-1 );
                        isGetter = true;
                    }
                    else if (name.endsWith("()")) {
                        name = name.substring( 0, name.length()-2 );
                        isGetter = true;
                    }

                    // first look for a field
                    if (findField(name) != null) {
                        adapter = bindField(name);
                    }
                    // then a method
                    else if (isGetter) {
                        adapter = bindIfMethodExists(name);

                        if (adapter == null) {
                            //todo: use java.beans.Introspector to get the real JavaBeans getter
                            String getterMethodName = "get" +
                                    name.substring(0,1).toUpperCase() +
                                    name.substring(1);
                            adapter = bindIfMethodExists(getterMethodName);
                        }

                        if (adapter == null) {
                            throw new Exception("could not find getter for " + name);
                        }
                    }
                    // todo: make it work with a setter method
                    else {
                        throw new Exception("could not find field " + name + ": SimpleFixture does not yet support setter methods");
                    }
                }
                columnBindings[i] = adapter;
            }
            catch (Exception e) {
                exception (heads, e);
            }
        }
    }

    private Field findField(String name) throws NoSuchFieldException
    {
        try {
            return getTargetClass().getField(name);
        }
        catch (NoSuchFieldException e) {
            return null;
        }
    }

    private TypeAdapter bindIfMethodExists(String name) throws Exception
    {
        TypeAdapter adapter = null;
        if (findMethod(name) != null) {
            adapter = bindMethod(name);
        }
        return adapter;
    }

    private Method findMethod(String name)
    {
        try {
            return getTargetClass().getMethod(name, new Class[] {});
        }
        catch (NoSuchMethodException e) {
            return null;
        }
    }

}
