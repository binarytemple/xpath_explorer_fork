package org.xpathexplorer.xslt;

import junit.framework.TestCase;
import org.jdom.JDOMException;
import org.jdom.Element;
import net.sourceforge.xpe.xmlassert.XmlDiff;

import java.io.IOException;
import java.io.File;
import java.util.List;

import com.purpletech.util.IOUtils;

public class XsltTesterTest extends TestCase
{
    protected final String xmlOokData = "<data>ook</data>";
    protected final String xslOokTransform = "<xsl:template match='data'><trans><xsl:apply-templates/></trans></xsl:template>";
    protected final String xmlOokResult = "<trans>ook</trans>";

    public void testTransform() throws Exception
    {
        String expectedXml = "<root><sub>ook</sub></root>";
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='/'><root><xsl:apply-templates/></root></xsl:template>" +
                                                       "<xsl:template match='data'><sub><xsl:apply-templates/></sub></xsl:template>");
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        String resultString = tester.run();
        assertXmlEquals(expectedXml, resultString);
    }

    public void testResultString() throws Exception
    {
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='/'><root><xsl:apply-templates/></root></xsl:template>" +
                                                       "<xsl:template match='data'><sub><xsl:apply-templates/></sub></xsl:template>");
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        String resultString = tester.run();
        assertXmlEquals(resultString, tester.getResultAsString());
    }

    public void testResultAsElement() throws Exception
    {
        XsltTester tester = new XsltTester(xslOokTransform);
        tester.setInputXml(xmlOokData);
        tester.run();
        assertEquals(xmlOokResult, tester.getResultAsString());
        Element resultElement = tester.getResultAsElement();
        assertEquals("result", resultElement.getName());
        assertEquals("trans", ((Element)resultElement.getChildren().get(0)).getName());
    }

    public void TODOtestTextOutput() throws Exception
    {
        XsltTester tester = new XsltTester();
        tester.setTestedXsl("<xsl:template name='foo'>text</xsl:template>");
        tester.runCallTemplate("foo");
        assertEquals("text", tester.getResultAsString());
        List list = tester.getResult();
        assertEquals(1, list.size());
        assertEquals("text", (String)list.get(0));
    }

    public void testStringEquals() throws Exception
    {
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='data'><sub><xsl:apply-templates/></sub></xsl:template>");
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        String resultString = tester.run();
        assertEquals("true", tester.stringEquals(resultString));

        String expectedString = "<sub>geek</sub>";
        assertEquals(XsltTester.strdiff(expectedString, resultString),
                tester.stringEquals(expectedString));
    }

    public void testXmlEquals() throws Exception
    {
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='/'><foo aaa='xxx' bbb='zzz'/></xsl:template>");
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        tester.run();
        assertEquals("should xml-equal equivalent DOM", "true", tester.xmlEquals("<foo bbb='zzz' aaa='xxx'></foo>"));
        String xmlEqualsMessage = tester.xmlEquals("<foo bbb='zzz'></foo>");
        assertTrue("should start with Additional but is " + xmlEqualsMessage, xmlEqualsMessage.startsWith("Additional"));
    }

    public void testStrdiff() throws Exception
    {
        assertEquals("Expected: foo " +
                "Actual: bar", XsltTester.strdiff("foo", "bar"));
    }

    private void assertXmlEquals(String expectedXml, String resultString) throws IOException, JDOMException
    {
        XmlDiff diff = new XmlDiff(expectedXml, resultString);
        assertTrue(diff.print(), diff.same());
    }

    public void testMatch() throws Exception
    {
        String xml = "<stuff><ignore>foo</ignore><data>ook</data><ignore>bar</ignore></stuff>";
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='/'><slash/></xsl:template>" +
                                                       "<xsl:template match='data'><result><xsl:apply-templates/></result></xsl:template>" );
        String expectedXml = "<result>ook</result>";
        String resultString = new XsltTester(testedXsl).runMatch(xml, "//data");
        assertXmlEquals(expectedXml, resultString);
    }

    public void testCallTemplate() throws Exception
    {
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='/'><slash/></xsl:template>" +
                                                       "<xsl:template name='process'><result><xsl:apply-templates/></result></xsl:template>");
        String expectedXml = "<result>ook</result>";
        XsltTester tester = new XsltTester();
        tester.setTestedXsl(testedXsl);
        tester.setInputXml(xmlOokData);
        String resultString = tester.runCallTemplate("process");
        assertXmlEquals(expectedXml, resultString);
    }

    public void testCallTemplateWithParams() throws Exception
    {
        String xml = xmlOokData;
        String testedXsl = XsltTester.createStylesheet(
                "<xsl:template match='/'><slash/></xsl:template>" +
                "<xsl:template name='process'>" +
                    "<xsl:param name='name'/>" +
                    "<xsl:param name='value'/>" +
                    "<xsl:element name='{$name}'>" +
                        "<xsl:value-of select='$value'/>" +
                    "</xsl:element>" +
                "</xsl:template>");

        String expectedXml = "<result>koo</result>";
        XsltTester xsltTester = new XsltTester();
        xsltTester.setTestedXsl(testedXsl);
        xsltTester.setInputXml(xml);
        String resultString = xsltTester.runCallTemplate(
                "process", new String[][] { { "name", "'result'" }, { "value", "'koo'" } });
        assertXmlEquals(expectedXml, resultString);
    }

    //todo: maybe unify with above
    public void testMatchWithParams() throws Exception
    {
        String xml = xmlOokData;
        String testedXsl = XsltTester.createStylesheet(
                "<xsl:template match='data'>" +
                    "<xsl:param name='name'/>" +
                    "<xsl:param name='value'/>" +
                "<xsl:element name='{$name}'>" +
                        "<xsl:value-of select='$value'/>" +
                    "</xsl:element>" +
                "</xsl:template>");

        String expectedXml = "<result>koo</result>";
        XsltTester xsltTester = new XsltTester();
        xsltTester.setTestedXsl(testedXsl);
        xsltTester.setInputXml(xml);
        String resultString = xsltTester.runMatch("/data", new String[][] { { "name", "'result'" }, { "value", "'koo'" } });
        assertXmlEquals(expectedXml, resultString);
    }

    public void testMatchWithMode() throws Exception
    {
        XsltTester xsltTester = new XsltTester();
        xsltTester.setTestedXsl("<xsl:template match='data' mode='blue'><BLUE/></xsl:template>" +
                "<xsl:template match='data' mode='white'><WHITE/></xsl:template>");
        xsltTester.setMode("blue");
        xsltTester.setInputXml(xmlOokData);
        xsltTester.runMatch("/data");
        assertEquals("<BLUE />", xsltTester.getResultAsString());

        xsltTester.runMatch("/data", new String[][] {});
        assertEquals("<BLUE />", xsltTester.getResultAsString());

    }

    public void testParseParams() throws Exception
    {
        String params="foo=bar,baz=baf bam";
        String[][] a = XsltTester.parseParams(params);
        assertEquals("foo", a[0][0]);
        assertEquals("bar", a[0][1]);
        assertEquals("baz", a[1][0]);
        assertEquals("baf bam", a[1][1]);
    }

    public void testStripXmlHeader() throws Exception
    {
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='data'><result><xsl:apply-templates/></result></xsl:template>");
        String expectedXml = "<result>ook</result>";
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        String resultString = tester.run();
        assertEquals(expectedXml, resultString);
    }

    public void testExtraTemplates() throws Exception
    {
        String testedXsl = XsltTester.createStylesheet("<xsl:template match='data'><result>BAD</result></xsl:template>");
        String extraTemplates = "<xsl:template match='data'><result>GOOD</result></xsl:template>";
        XsltTester tester = new XsltTester(testedXsl, extraTemplates);
        tester.setInputXml(xmlOokData);
        String result = tester.run();
        assertEquals("<result>GOOD</result>", result);
    }

    public void testTurnsFragmentsIntoStylesheet() throws Exception
    {
        String testedXsl = "<xsl:template match='a'><x><xsl:apply-templates/></x></xsl:template>"
        +
        "<xsl:template match='b'><y><xsl:apply-templates/></y></xsl:template>";
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml("<a><b>foo</b></a>");
        assertEquals("<x><y>foo</y></x>", tester.run());
    }

    public void testTurnsSingleFragmentIntoStylesheet() throws Exception
    {
        String testedXsl = "<xsl:template match='data'><result><xsl:apply-templates/></result></xsl:template>";
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        assertEquals("<result>ook</result>", tester.run());
    }

    public void testRegisterUri() throws Exception
    {
        String testedXsl = "<xsl:include href='foo'/>";
        String includedString =
                XsltTester.createStylesheet("<xsl:template match='data'><included><xsl:apply-templates/></included></xsl:template>");
        XsltTester tester = new XsltTester(testedXsl);
        tester.register("foo", includedString);
        tester.setInputXml(xmlOokData);
        assertEquals("<included>ook</included>", tester.run());
    }

    public void testTransformResultIsNodesNotElement() throws Exception
    {
        String testedXsl = "<xsl:template match='data'><element1><xsl:apply-templates/></element1>some text<element2><xsl:apply-templates/></element2></xsl:template>";
        XsltTester tester = new XsltTester(testedXsl);
        tester.setInputXml(xmlOokData);
        assertEquals("<element1>ook</element1>some text<element2>ook</element2>", tester.run());
    }

    public void testDoubleInclude() throws Exception
    {
        String one = "<xsl:include href='two'/>";
        String two = XsltTester.createStylesheet("<xsl:include href='three'/>");
        String three = XsltTester.createStylesheet("<xsl:template match='data'><three foo='bar'/></xsl:template>");
        XsltTester tester = new XsltTester(one);
        tester.register("two", two);
        tester.register("three", three);
        tester.uriResolver().addReplacement("bar", "zog");
        tester.setInputXml("<data />");
        assertEquals("<three foo=\"zog\" />", tester.run());
    }

    public void testIncludeFromRelativeDirectory() throws Exception
    {
        File baseDir = new File("build/test/base");
        File oneDir = new File(baseDir, "one");
        File twoDir = new File(baseDir, "two");
        baseDir.mkdirs();
        oneDir.mkdirs();
        twoDir.mkdirs();

        String baseXsl = XsltTester.createStylesheet("<xsl:include href='one/one.xsl'/>");
        IOUtils.writeString(new File(baseDir, "base.xsl"), baseXsl);

        String oneXsl = XsltTester.createStylesheet("<xsl:include href='../two/two.xsl'/>");
        IOUtils.writeString(new File(oneDir, "one.xsl"), oneXsl);

        String twoXsl = XsltTester.createStylesheet("<xsl:template name='foo'><foo/></xsl:template>");
        IOUtils.writeString(new File(twoDir, "two.xsl"), twoXsl);

        XsltTester tester = new XsltTester();
        tester.uriResolver().addBaseDir(baseDir);
        tester.setTestedXsl("<xsl:include href='base.xsl'/>");
        tester.setInputXml("<bar/>");
        String result = tester.runCallTemplate("foo");
        assertEquals("<foo />", result);

    }

    public void testIncludeFromRelativeDirectoryRecursive() throws Exception
    {

    }

    public void testNoInputXml() throws Exception
    {
        XsltTester tester = new XsltTester();
        tester.setTestedXsl("<xsl:template name='foo'><foo/></xsl:template>");
        tester.runCallTemplate("foo");
        assertXmlEquals("<foo/>", tester.getResultAsString());
    }

    public void testSetVariableValue() throws Exception
    {
        XsltTester tester = new XsltTester();
        tester.setTestedXsl("<xsl:template name='foo'><foo><xsl:value-of select='$one'/></foo></xsl:template>");
        tester.setVariableValue("one", "1");
        tester.runCallTemplate("foo");
        assertXmlEquals("<foo>1</foo>", tester.getResultAsString());
    }

    public void testSetVariableXpath() throws Exception
    {
        XsltTester tester = new XsltTester();
        tester.setTestedXsl("<xsl:template name='foo'><foo><xsl:value-of select='$one'/></foo></xsl:template>");
        tester.setInputXml("<data>2</data>");
        tester.setVariableXpath("one", "/data");
        tester.runCallTemplate("foo");
        assertXmlEquals("<foo>2</foo>", tester.getResultAsString());
    }

}
