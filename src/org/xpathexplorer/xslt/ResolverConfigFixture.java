package org.xpathexplorer.xslt;

import com.purpletech.util.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

//todo: test me

/**
 * Static configurator for URI Resolver for XsltFixture
 */
public class ResolverConfigFixture extends SimpleFixture {
    public static Map uriMap = new TreeMap();
    public static Map replacements = new TreeMap();
    public static String base = null;

    public String uri;
    public String replace;
    public String content;
    public String xslt;
    public String file;
    public String baseDir;

    public void reset() throws Exception {
        uri = null;
        replace = null;
        content = null;
        file = null;
        xslt = null;
        baseDir = null;
    }

    public String set() throws Exception {
        if (count(new String[]{uri, replace, baseDir}) != 1) {
            throw new RuntimeException("One and only one of 'uri' or 'replace' or 'baseDir' must be set");
        }

        int c = count(new String[]{content, file, xslt});

        if (baseDir != null) {
            if (c > 0) {
                throw new RuntimeException("If baseDir is set, you shouldn't set any other parameters");
            }
            base = baseDir;
            return "baseDir => " + base;
        }

        if (c == 0) {
            throw new RuntimeException("Must set one of " + "(content, file, xslt)");
        }
        if (c > 1) {
            throw new RuntimeException("Shouldn't set more than one of " + "(content, file, xslt)");
        }

        if (file != null) {
            File f = new File(file);
            content = IOUtils.readFile(f);
        } else if (xslt != null) {
            content = XsltTester.createStylesheet(xslt);
        }

        if (content == null) {
            throw new RuntimeException("ERROR: content not set");
        }

        String key;
        if (uri != null) {
            uriMap.put(uri, content);
            key = uri;
        } else {
            replacements.put(replace, content);
            key = replace;
        }
        String message = key + " => " + StringEscapeUtils.escapeHtml(StringUtils.abbreviate(content, 200));
        return message;
    }

    private int count(String[] strings) {
        int c = 0;
        for (int i = 0; i < strings.length; i++) {
            if (strings[i] != null)
                c++;

        }
        return c;
    }
}
