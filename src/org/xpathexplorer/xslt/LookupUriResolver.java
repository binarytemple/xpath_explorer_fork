package org.xpathexplorer.xslt;

import com.purpletech.util.IOUtils;
import org.apache.commons.lang.StringUtils;

import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.URIResolver;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.*;
import org.apache.commons.lang.StringEscapeUtils;

public class LookupUriResolver implements URIResolver {
    Map hrefMap = new HashMap();
    Map replacements = new HashMap();
    List baseDirs = new LinkedList();
    public static boolean debug = false;
    private Map uriReplacements = new HashMap();

    private static void log(String message) {
        if (debug)
            System.err.println(new Date() + " - " + message);
    }

    public LookupUriResolver() {
    }

    public LookupUriResolver(String targetHref, String targetXml) {
        hrefMap.put(targetHref, targetXml);
    }

    public Source resolve(String href, String base) throws TransformerException {
        log("resolve(href=" + href + ",base=" + base);
        File file = null;
        String content = null;
        Object value = hrefMap.get(href);
        if (value == null) {
            List currentBaseDirs = baseDirs(base);
            if (isAbsolutePath(href)) {
            	file = new File(href);
				if (!file.exists())
					throw new TransformerException("Could not find file '" + href);
            }
            else {
            file = findFile(currentBaseDirs, href);
            if (file == null)
                throw new TransformerException("Could not find file '" + href + "' relative to currentBaseDirs " + StringUtils.join(currentBaseDirs.iterator(), ","));
            }
            log("Found " + file);

            content = loadContent(file);
        } else if (value instanceof File) {
            file = (File) value;
            content = loadContent(file);
        } else {
            content = (String) value;
        }

        if (content == null)
            return null;
        content = applyReplacements(content);
        content = applyReplacementsForUri(href, content);
//        log(StringUtils.abbreviate(StringUtils.escape(content), 200));
        log(StringEscapeUtils.escapeJava(content));
        return source(content, file);
    }

    static boolean isAbsolutePath(String href) {
    	return
    		href.length() > 3 &&
    		Character.isLetter(href.charAt(0)) &&
    		href.charAt(1) == ':' &&
    		(href.charAt(2) == '/' || href.charAt(2) == '\\');
	}

	private String loadContent(File file) throws TransformerException {
        log("loading from " + file);
        try {
            return IOUtils.readFile(file);
        } catch (IOException e) {
            throw new TransformerException("Exception loading file " + file + ": " + e.getMessage());
        }
    }

    private File findFile(List baseDirs, String href) {
        File file = null;
        Iterator i = baseDirs.iterator();
        while (file == null && i.hasNext()) {
            File currentBaseDir = (File) i.next();
            File currentFile = new File(currentBaseDir, href);
            log("Looking for " + currentFile);
            if (currentFile.exists()) {
                file = currentFile;
            }
        }
        return file;
    }

    private List baseDirs(String base) {
        List currentBaseDirs = new ArrayList();
        if (base != null && base.startsWith("file:///")) {
            currentBaseDirs.add(turnBaseUrlIntoDir(base));
        }
        currentBaseDirs.addAll(baseDirs);
        if (currentBaseDirs.isEmpty())
            currentBaseDirs.add(new File("."));
        return currentBaseDirs;
    }

    private StreamSource source(String content, File file) {
        StreamSource source = new StreamSource(new StringReader(content));
        if (file != null)
            source.setSystemId(file);
        return source;
    }

    private File turnBaseUrlIntoDir(String base) {
        File currentBaseDir = new File(base.substring(8));
        if (currentBaseDir.isFile()) {
            currentBaseDir = currentBaseDir.getParentFile();
        }
        return currentBaseDir;
    }

    private String applyReplacements(String content) {
        return applyReplacements(replacements, content);
    }

    private String applyReplacementsForUri(String uri, String content) {
        return applyReplacements(replacements(uri), content);
    }

    private String applyReplacements(Map replacements, String content) {
        //todo: support regular expressions
        Iterator patterns = replacements.keySet().iterator();
        while (patterns.hasNext()) {
            String pattern = (String) patterns.next();
            String replacement = (String) replacements.get(pattern);
            content = StringUtils.replace(content, pattern, replacement);
        }
        return content;
    }

    public void register(String uri, String content) {
        hrefMap.put(uri, content);
    }

    public void register(String uri, File contentFile) throws IOException {
        hrefMap.put(uri, contentFile);
    }

    public void register(Map uriMap) {
        hrefMap.putAll(uriMap);
    }

    public void addReplacement(String pattern, String replacement) {
        replacements.put(pattern, replacement);
    }

    public void addReplacements(Map replacements) {
        this.replacements.putAll(replacements);
    }

    public void addReplacementForUri(String uri, String pattern, String replacement) {
        replacements(uri).put(pattern, replacement);
    }

    private Map replacements(String uri) {
        if (uriReplacements.get(uri) == null) {
            uriReplacements.put(uri, new HashMap());
        }
        return (Map) uriReplacements.get(uri);
    }


    /**
     * @deprecated
     */
    public void setBaseDir(File dir) {
        baseDirs = new LinkedList();
        addBaseDir(dir);
    }

    public void addBaseDir(File dir) {
        baseDirs.add(dir);
    }
}
