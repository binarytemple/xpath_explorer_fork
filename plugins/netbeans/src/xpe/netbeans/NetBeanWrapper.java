package xpe.netbeans;

import java.io.*;

import org.openide.*;
import org.openide.util.*;
import org.openide.util.actions.CallableSystemAction;

/**
 *
 * @author Simon Ru (UC Berkeley) simonru@yahoo.com
 */
public class NetBeanWrapper extends CallableSystemAction {

    public void performAction() {

        try {
            xpe.gui.XPEFrame.main(new String[0]);
        }
        catch (Exception e) {
            StringWriter writer = new StringWriter();
            e.printStackTrace(new PrintWriter(writer));
            NotifyDescriptor desc = new NotifyDescriptor.Message(writer.toString());
            TopManager.getDefault().notify(desc);
        }
    }

    public String getName() {
        return NbBundle.getMessage(NetBeanWrapper.class, "LBL_Action");
    }

    protected String iconResource() {
        return "xpe/netbeans/MyActionIcon.gif";
    }

    public HelpCtx getHelpCtx() {
        return HelpCtx.DEFAULT_HELP;
    }

}
