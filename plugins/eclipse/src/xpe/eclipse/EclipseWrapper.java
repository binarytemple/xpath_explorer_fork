package xpe.eclipse;

import org.eclipse.ui.plugin.*;
import org.eclipse.core.runtime.*;

/**
 * The main plugin class to be used in the desktop.
 */
public class EclipseWrapper extends AbstractUIPlugin {
	
	/**
	 * The constructor.
	 */
	public EclipseWrapper(IPluginDescriptor descriptor) {
		super(descriptor);
	}
}
