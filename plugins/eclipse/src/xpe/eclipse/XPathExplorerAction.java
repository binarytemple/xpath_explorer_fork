package xpe.eclipse;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.*;
import xpe.gui.XPEFrame;

/**
 * Our sample action implements workbench action delegate.
 * The action proxy will be created by the workbench and
 * shown in the UI. When the user tries to use the action,
 * this delegate will be created and execution will be 
 * delegated to it.
 * @see IWorkbenchWindowActionDelegate
 */
public class XPathExplorerAction implements IWorkbenchWindowActionDelegate {
    private IWorkbenchWindow window;
    
    XPEFrame xpeframe;
    
    /**
     * The constructor.
     */
    public XPathExplorerAction() {
    }

    /**
     * The action has been activated. The argument of the
     * method represents the 'real' action sitting
     * in the workbench UI.
     * @see IWorkbenchWindowActionDelegate#run
     */
    public void run(IAction action) {

        try {
//            System.out.println(action);
            String url = XPEFrame.getDefaultUrlString();
            String xpath = "//";
            xpeframe = XPEFrame.displayFrame(url, xpath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Selection in the workbench has been changed. We 
     * can change the state of the 'real' action here
     * if we want, but this can only happen after 
     * the delegate has been created.
     * @see IWorkbenchWindowActionDelegate#selectionChanged
     */
    public void selectionChanged(IAction action, ISelection selection) {
//        System.out.println("Action=" +action);
//        System.out.println("Selection="+selection);
    }

    /**
     * We can use this method to dispose of any system
     * resources we previously allocated.
     * @see IWorkbenchWindowActionDelegate#dispose
     */
    public void dispose() {
        xpeframe.dispose();
    }

    /**
     * We will cache window object in order to
     * be able to provide parent shell for the message dialog.
     * @see IWorkbenchWindowActionDelegate#init
     */
    public void init(IWorkbenchWindow window) {
        this.window = window;
    }
}