require 'rubybuilder/Release'
require 'rubybuilder/RubyAnt'
require 'rubybuilder/BuildFile'

info = '1-0beta2'
cvs_client = CvsClient.new(CvsInfo.load('CVS'), 'releasetmp') 
builder = RubyAnt.new('releasetmp/build.xml')
build_file = BuildFile.new('xml-fixture') 

cvs_client.checkout
builder.launch('release-fixture')
build_number = build_file.increase('build.number')
build_file.save
cvs_client.commit("Updating build number for xmlfixture #{info}")
cvs_client.tag("xml-fixture-#{info}")
cvs_client.commit("Releasing xmlfixture #{info}")
